/*******************************************************************************
NiltfController
*******************************************************************************/
class NiltfController  {
  constructor(noteData) {
    // alert("in constructor Note data is " + noteData)
    this.noteData = noteData;
    this.activeNoteId = null;
    // this.noteSeq = 10;
    this.activeNoteBody = ""
    this.activeNoteTitle = ""
    this.activeNoteListeners = [];
  }

  registerActiveNoteListener(listener) {
    this.activeNoteListeners.push(listener);
  }

  addNewNote(noteTitle) {
    // alert("adding new note " + noteTitle);
    var noteId = this.noteData.length
    this.noteData.push({id : noteId , title : noteTitle, body : ""});
    this.updateActiveNoteId(noteId);
    // this.noteSeq = this.noteSeq + 1;
  }

  saveActiveNote(noteBody, noteTitle) {
    // alert("adding new note " + noteTitle);
    var note = this.noteData[this.activeNoteId];
    // alert(note);
    note.body = noteBody;
    note.title = noteTitle;
    this.activeNoteBody = noteBody;
    this.activeNoteTitle = noteTitle;
    this.updateActiveNoteListeners();
    // this.noteData.push({id : noteId , title : noteTitle, body : "new Body"});
    // this.noteSeq = this.noteSeq + 1;
  }

  getNoteData() {
    // alert("in getNoteData, note data is " + this.noteData)
    return this.noteData;
  }

  alertState() {
    var state = "I have " + this.noteData.length + " entries \n\n";
    state += this.noteData.map(function(note) {
      return note.title + "\n";
    });
    state += "\n";
    state += "activeNoteId=" + this.activeNoteId;
    state += "\n";
    state += "length=" + this.noteData.length;
    state += "\n";
    state += "activeNoteBody=" + this.activeNoteBody;
    alert(state);
  }

  updateActiveNoteId(newActiveNoteId) {
    this.activeNoteId = newActiveNoteId;
    this.activeNoteBody = this.noteData[this.activeNoteId].body;
    this.activeNoteTitle = this.noteData[this.activeNoteId].title;
    this.updateActiveNoteListeners();
    // this.activeNoteBody = this.noteData.
    // alert("activeNoteId is now " + newActiveNoteId);
  }

  updateActiveNoteListeners() {
    // alert("updating note listeners");
    this.activeNoteListeners.forEach(function(listener) {
      // alert("updating listener");
      listener.activeNoteUpdated();
    })
  }

}

/*******************************************************************************
NoteTreeComponent
*******************************************************************************/
var NoteTreeComponent = React.createClass({
  getInitialState: function() {
    // alert(this.props.niltfController);
    // alert(this.props.niltfController.getNoteData());
    niltfController.registerActiveNoteListener(this);
    return {
      // noteData: this.props.niltfController.noteData,
      noteData : niltfController.noteData
    };
  },
  render: function() {
    // alert("note tree controller is in NoteTreeComponent is " + this.props.niltfController);
    return (
      <div className="noteTreeComponent">
         <NoteTreeList noteData={this.state.noteData}/>
         <NoteTreeAddNoteForm onNewNoteSubmit={this.handleNewNoteSubmit} />
         <button className="btn btn-default" onClick={this.showControllerState}>
            Show ControllerState
          </button>
      </div>
    );
  },
  handleNewNoteSubmit: function(newNoteData) {
    niltfController.addNewNote(newNoteData.noteTitle)
    this.setState({noteData: niltfController.noteData});
  },
  showControllerState: function() {
    niltfController.alertState();
  },
  activeNoteUpdated: function() {
    // alert("NoteTreeComponent is updating ");
    this.setState({noteData: niltfController.noteData});
  }
});

/******************************************************************************
NoteTreeAddNoteForm : allows us to add a new note by entering
a title and clicking submit
*******************************************************************************/
var NoteTreeAddNoteForm = React.createClass({
  getInitialState: function() {
    return {noteTitle: ''};
  },
  handleNoteTitleChange : function(e) {
    // alert("note title change note title is now " + e.target.value)
    this.setState({noteTitle : e.target.value})
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var noteTitle = this.state.noteTitle.trim();
    if (!noteTitle) {
      return;
    }
    this.props.onNewNoteSubmit({noteTitle:noteTitle})
    this.setState({noteTitle: ''})
    // alert("submited");
  },
  render: function() {
    return (
      <form className="noteTreeAddNoteForm" onSubmit={this.handleSubmit}>
        <input
          type="text"
          size="20"
          placeholder="Enter New Note Title"
          value = {this.state.noteTitle}
          onChange={this.handleNoteTitleChange}
        />
        <button className="btn btn-default" type="submit" value="Post">Add</button>
      </form>
    );
  }
});

/*******************************************************************************
  NoteTreeList : the list of notes
*******************************************************************************/
var NoteTreeList = React.createClass({
  render: function() {
    var noteTreeNoteParent = this;
    var noteTreeNotes = this.props.noteData.map(function(note) {
      return (
        <NoteTreeNode
          title={note.title}
          key={note.id}
          noteId={note.id}
        />
      );
    });
    return (
        <ul>
          {noteTreeNotes}
        </ul>
    );
  }

});

/*******************************************************************************
NoteTreeNode : A single note in the list
*******************************************************************************/
var NoteTreeNode = React.createClass({
  render: function() {
    return (
      <li className="noteTreeNote" onClick={this.handleClick}>
        {this.props.title}
      </li>
    );
  },
  handleClick: function(e) {
    niltfController.updateActiveNoteId(this.props.noteId)
  }
});

/*******************************************************************************
NoteEditorComponent
*******************************************************************************/
var NoteEditorComponent = React.createClass({
  getInitialState: function () {
    niltfController.registerActiveNoteListener(this);
    // return {noteBody : "some stuff", noteTitle : "The title"};
    return {noteBody : "", noteTitle : ""};
  },
  handleNoteBodyChange: function(ev) {
    // alert(ev.target.value);
    this.setState({noteBody: ev.target.value});
    // alert(this.state.noteBody);
  },
  handleNoteTitleChange: function(ev) {
    // alert(ev.target.value);
    this.setState({noteTitle: ev.target.value});
    // alert(this.state.noteBody);
  },
  showComponentState: function(ev) {
    var stateStr = "componentState is";
    stateStr += "\n";
    stateStr += "noteBody=" + this.state.noteBody;
    alert(stateStr);
  },
  saveNote: function(ev) {
    console.log("saving note");
    niltfController.saveActiveNote(this.state.noteBody,
      this.state.noteTitle);
  },
  render: function() {
    return (
      <div className="noteEditorComponent">
        <NoteEditorMenuBar
          handleSave = {this.saveNote}
          showComponentState={this.showComponentState}
        />
        <NoteEditorTitle
          noteTitle={this.state.noteTitle}
          handleNoteTitleChange={this.handleNoteTitleChange}
        />
        <RawNoteEditor
          noteBody={this.state.noteBody}
          handleNoteBodyChange={this.handleNoteBodyChange}
          />
      </div>
    );
  },
  activeNoteUpdated: function() {
    // alert("I will update then also");
    this.setState({noteBody: niltfController.activeNoteBody})
    this.setState({noteTitle: niltfController.activeNoteTitle})
  }
});

/*******************************************************************************
NoteEditorMenuBar
*******************************************************************************/
var NoteEditorMenuBar = React.createClass({
  render: function() {
    return (
      <div className="noteEditorMenuBar">
        <button className="btn btn-default"  onClick={this.props.handleSave}>
          Save
        </button>
        <button className="btn btn-default" onClick={this.props.showComponentState}>
          Show State
        </button>
      </div>
    );
  }
});

/******************************************************************************
  RawNoteEditor
*******************************************************************************/
var RawNoteEditor = React.createClass({
  // getInitialState: function () {
  //   return {noteBody : "some stuff"};
  // },
  // handleNoteBodyChange: function(ev) {
  //   this.setState({noteBody: ev.target.value});
  //   alert(this.state.noteBody);
  // },
  render: function() {
    return (
      <textarea
        value={this.props.noteBody}
        cols="100"
        rows="25"
        onChange={this.props.handleNoteBodyChange} />
    );
  }
});


var NoteEditorTitle = React.createClass({
  // getIntialState : function () {
  //   return {noteTitle : ""}
  // },
  // render: function() {
  //   return (
  //     <h1 contentEditable = "true" onChange={this.props.handleNoteTitleChange}
  //        >
  //       {this.props.noteTitle}
  //     </h1>
  //   )
  // }
  render: function() {
    return (
      <div>
      <input type="text"
        value={this.props.noteTitle}
        size="40"
        className="noteTitle"
        onChange={this.props.handleNoteTitleChange} />
      </div>
    );
  }
});




/******************************************************************************
Main : The main setup for our app
*******************************************************************************/

// TODO: constructor not working here, don't know why
// have to set by hand instead
var noteData = [
  {id: 0, title: "Note 1", body: "This is the first note"},
  {id: 1, title: "Note 2", body: "This is the second note"},
  {id: 2, title: "Note 3", body: "This is the third note"},
  {id: 3, title: "Note 4", body: "This is the fourth note"}
];
var niltfController = new NiltfController(noteData);
// niltfController.noteData =

ReactDOM.render(
  <NoteTreeComponent />,
  document.getElementById('noteTreeComponent')
);

ReactDOM.render(
  <NoteEditorComponent />,
  document.getElementById('noteEditorComponent')
);

niltfController.updateActiveNoteId(0);
