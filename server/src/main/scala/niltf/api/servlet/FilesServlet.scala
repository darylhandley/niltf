package niltf.api.servlet

import java.io.{BufferedOutputStream, File, FileOutputStream, FileReader}
import java.nio.file.{Files, Paths}
import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.BindingModule
import niltf.api.model.FileDefinition
import niltf.api.restexception.E400BadRequest
import niltf.api.util.FileExtensionUtil
import org.scalatra._
import org.scalatra.servlet.{FileItem, FileUploadSupport, MultipartConfig, SizeConstraintExceededException}

import xml.Node

class FilesServlet (implicit bindingModule: BindingModule)
    extends BaseJsonServlet() (bindingModule)
    with FileUploadSupport {

  configureMultipartHandling(MultipartConfig(maxFileSize = Some(3*1024*1024)))

  val baseDir = "/Users/darylhandley/dev/niltfData"

  options("/*") {
    response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"))
    response.setHeader("Access-Control-Allow-Methods", "DELETE,PATCH,POST")
  }

  // display a simple form for uploading a file, this is for testing only
  get("/") {
    val html =
      <html>
        <head></head>
        <body>
          <h1>Upload file</h1>
          <form action={url("/upload")} method="post" enctype="multipart/form-data">
            <p>File to upload: <input type="file" name="file" /></p>
            <p><input type="submit" value="Upload" /></p>
          </form>
          <p>
            Upload a file using the above form. After you hit "Upload"
            the file will be uploaded and your browser will start
            downloading it.
          </p>

          <p>
            The maximum file size accepted is 3 MB.
          </p>
        </body>
      </html>


    Ok(html,
      Map("Content-Type" -> "text/html")
    )

  }

  error {
    case e: SizeConstraintExceededException =>
      RequestEntityTooLarge(
        <p>The file you uploaded exceeded the 3 MB limit.</p>
      )
  }


  // process a file upload
  post("/upload") {
    fileParams.get("file") match {
      case Some(file) => processFile(file)
      case None =>  handleNoFile
    }
  }

  // returns the file if found
  get("/:filename")  {

    // load the file from the disk and get the content type
    val filename = params("filename")
    val fullFilePath = baseDir + "/" + filename
    val fileBytes = Files.readAllBytes(Paths.get(fullFilePath))
    val contentType = FileExtensionUtil.filepathToContentType(filename)

    // alternate way to get content type but doesn't seem to work might need it later if we decide to use
    // the content of the file to determine content type instead of the extension
    // val contentType = Files.probeContentType(Paths.get(fullFilePath))

    // TODO: what should we do if we can't determine the content type ?

    // return the file
    Ok(fileBytes, Map(
      "Content-Type" -> contentType.getOrElse("application/octet-stream")
    ))

  }

  // process the file if one was found
  def processFile(file: FileItem) = {
    val user = getUser

    if (file.size ==0) {
      handleNoFile
    }

    // mimeType is something like Some("image/png")
    // can check this later for a known mime type
    val mimeType = file.contentType

    // save file to disk
    val fileBytes = file.get()
    val name = file.name

    val filename = baseDir + "/" + name
    Files.write(Paths.get(filename), fileBytes)

    // save the file definition to the db
    val id = UUID.randomUUID()
    val path = s"/api/niltf/1/files/$name"
    val fileDef = FileDefinition(id, name, user.id, Instant.now, path)

    // success
    Ok(fileDef)
  }

  def handleNoFile = {
    throw new E400BadRequest("No file content in request")
  }

}
