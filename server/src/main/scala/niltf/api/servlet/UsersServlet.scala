package niltf.api.servlet

import java.util.UUID

import com.escalatesoft.subcut.inject.BindingModule
import niltf.api.requestmodel.{NoteRequest, UserRequest}
import niltf.api.restexception.E404NotFound
import niltf.api.security.AuthenticationSupport
import niltf.api.service.{NoteService, UserService}

class UsersServlet(implicit bindingModule: BindingModule)
  extends BaseJsonServlet() (bindingModule) with AuthenticationSupport {

  private val userService = inject[UserService]

  get("/me") {
    val user = getUser
    logger.info("user is " + user)
    user
  }

  post("/") {
    var userRequest = parsedBody.extract[UserRequest]
    userService.create(userRequest)
  }

  patch("/me") {
    val user = getUser
    var userRequest = parsedBody.extract[UserRequest]
    userService.update(user, userRequest)
  }

}
