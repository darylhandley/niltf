package niltf.api.servlet

import niltf.api.restexception.{E400BadRequest, E404NotFound, E409Conflict}

/*
 * Case class used for serializing errors to JSON. Errors in JSON look like this
 * {
 *    "status" : 404,
 *    "reason" : "Not Found"
 *    "message" : "Note xxxxx was not found"
 * }
 *
 * status - the http status code that is being returned
 * reason - The standard reason for this error code. 1:1 mapping with the status. Example: OK, Not Found,
 *  Unauthorized, ... See https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 * message - a detailed message of what went wrong.
 */
case class ErrorResponse (status : Int, reason : String, message : String)

object ErrorResponse {

  def e400BadRequest (message : String) = ErrorResponse(E400BadRequest.code, E400BadRequest.reason,  message)

  def e401Unauthorized (message : String) = ErrorResponse(E400BadRequest.code, E400BadRequest.reason, message)


  def e404NotFound (message : String) = ErrorResponse(E404NotFound.code, E404NotFound.reason,  message)

  def e409Conflict (message : String) = ErrorResponse(E409Conflict.code, E409Conflict.reason,  message)
}
