package niltf.api.servlet

import java.time.Instant
import java.util.{Date, UUID}
import javax.servlet.http.HttpServletRequest

import com.datastax.driver.core._
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.constants.NiltfConstants
import niltf.api.requestmodel.NoteRequest
import niltf.api.restexception.{E401Unauthorized, RestException, E404NotFound}
import niltf.api.security.AuthenticationSupport
import niltf.api.service.{SecurityService, NoteService}
import org.json4s.{DefaultFormats, Formats}
import org.scalatra._
import org.scalatra.json._
import niltf.api.model.{User, Note}
import org.slf4j.{LoggerFactory, Logger}

import scala.collection.JavaConverters._
import scala.util.Try
import org.scalatra.CorsSupport

class NotesServlet (implicit bindingModule: BindingModule)
  extends BaseJsonServlet() (bindingModule) with AuthenticationSupport {

  private val noteService = inject[NoteService]

  get("/") {
    val user = getUser
    logger.info("user is " + user)
    noteService.getTopLevelNotes(user.id)
  }

  get("/:noteId") {
    val user = getUser
    val noteId = UUID.fromString(params("noteId"))
    noteService.get(noteId, user.id) match {
      case Some(note) => note
      case None => throw new E404NotFound("Note " + noteId + " not found")
    }
  }

  get("/root") {
    val user = getUser
    noteService.getTopLevelNote(user.id)
  }

  get("/:noteId/children") {
    val user = getUser
    val noteId = UUID.fromString(params("noteId"))
    noteService.get(noteId, user.id) match {
      case Some(note) => noteService.getChildren(note)
      case None => throw new E404NotFound("Note " + noteId + " not found")
    }
  }

  // this is just for testing the logging, it does the same as the get(:noteId)
  // remove later when logging figured out
  get("/:someId/otherStuff/:noteId") {
    val user = getUser
    val noteId = UUID.fromString(params("noteId"))
    noteService.get(noteId, user.id) match {
      case Some(note) => note
      case None => throw new E404NotFound("Note " + noteId + " not found")
    }
  }

  delete("/:noteId") {
    val user = getUser
    val noteId = UUID.fromString(params("noteId"))
    noteService.delete(noteId, user.id)
    NoContent
  }

  post("/") {
    val user = getUser
    var noteRequest = parsedBody.extract[NoteRequest]
    noteService.create(user, noteRequest)
  }

  patch("/:noteId") {
    val user = getUser

    // get the note (simple version)
    val noteId = UUID.fromString(params("noteId"))

    noteService.get(noteId, user.id) match {
      case None => throw new E404NotFound(s"Note $noteId not found")
      case Some(note) => {
        val noteRequest : NoteRequest = parsedBody.extract[NoteRequest]
        noteService.update(note, noteRequest)
      }
    }
  }

}
