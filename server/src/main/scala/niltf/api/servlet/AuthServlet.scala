package niltf.api.servlet

import java.time.Instant
import java.util.UUID
import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.BindingModule
import niltf.api.model.User
import niltf.api.requestmodel.{AuthRequest, NoteRequest}
import niltf.api.restexception.{E404NotFound, E401Unauthorized}
import niltf.api.security.AuthenticationSupport
import niltf.api.service.{SecurityService, NoteService}

import scala.Option

class AuthServlet (implicit bindingModule: BindingModule)
  extends BaseJsonServlet() (bindingModule) {

  get("/token/:tokenId") {
    val tokenId = params("tokenId")
    val userOpt = securityService.getUserByAuthToken(tokenId)
    userOpt match {
      case Some(user) => user
      case None => throw new E404NotFound(s"AuthToken $tokenId not found")
    }
  }

  delete("/token/:tokenId") {
    val tokenId = params("tokenId")
    securityService.removeAuthToken(tokenId)
  }

  post("/token") {
    var authRequest = parsedBody.extract[AuthRequest]
    val tokenAndUserOpt = securityService.authenticate(authRequest.email, authRequest.password)

    tokenAndUserOpt match {
      case Some((authToken, user)) => AuthTokenAndUser (authToken, user)
      case None => throw new E401Unauthorized("Invalid email or password")
    }

  }

  // class to serialize the AuthToken and user
  case class AuthTokenAndUser(authToken: String, user: User)

}
