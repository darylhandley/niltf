package niltf.api.servlet

import java.util.UUID
import javax.servlet.http.HttpServletRequest

import com.datastax.driver.core._
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.constants.NiltfConstants
import niltf.api.jsonserializer.InstantSerializer
import niltf.api.jsonserializer.UUIDSerializer
import niltf.api.model.{Note, User}
import niltf.api.requestmodel.NoteRequest
import niltf.api.restexception.{E401Unauthorized, RestException}
import niltf.api.service.{NoteService, SecurityService}
import org.json4s.jackson.Serialization
import org.json4s.{DefaultFormats, FieldSerializer, Formats, NoTypeHints}
import org.scalatra.{CorsSupport, _}
import org.scalatra.json._
import org.slf4j.{Logger, LoggerFactory}

import scala.util.Try
/*
 * The base class for all JsonServlets. Provides
 * - error handling
 * - consistent logging of requests
 * - convenience methods
 */
class BaseJsonServlet (implicit val bindingModule: BindingModule)
  extends ScalatraServlet
  with JacksonJsonSupport
  with CorsSupport
  with Injectable {

  protected val logger : Logger = LoggerFactory.getLogger(getClass.getName)
  protected val securityService = inject[SecurityService]

  def parseUUID(uuidString: String) : Try[UUID] = Try(UUID.fromString(uuidString))

  before() {
    contentType = formats("json")
    request.setAttribute("requestStart", System.currentTimeMillis())
  }

  after() {
    val startTime = request.getAttribute("requestStart").asInstanceOf[Long]
    val requestDuration = System.currentTimeMillis - startTime
    logger.info(
          s"method=${request.getMethod}, " +
          s"path=${request.getServletPath}, " +
          s"requestDuration=$requestDuration, " +
          s"status=${response.getStatus}, "
    )


    //    logger.info("requestDuration=" + requestDuration + "ms, requestPath=" + ScalatraServlet.requestPath(request) +
    // ",contextPath=" + contextPath + ",servletPath=" + request.getServletPath + ",pathInfo=" + request.getPathInfo)
  }

  /** add the options on all routes for COR Support */
  options("/*") {
    response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"))
    response.setHeader("Access-Control-Allow-Methods", "DELETE,PATCH")
    // response.setHeader("Access-Control-Allow-Methods", "PATCH")
  }

  // gets the user based on the auth token in the headers,
  // if the auth token is not valid it will throw a 401 Unauthorized so it is also used for authentication. Make
  // sure to call it for all secured endpoints
  // requires a trip to database so use sparingly (once per request)
  def getUser(implicit request: HttpServletRequest) : User = {
    val authTokenOpt = Option(request.getHeader(NiltfConstants.AuthTokenKey))
    authTokenOpt match  {
      case Some(authToken) => securityService.getUserByAuthToken(authToken).getOrElse(
        throw new E401Unauthorized(s"Unknown Auth Token '$authToken'"))
      case _ => throw new E401Unauthorized("Auth Token was not supplied")
    }
  }

  // don't return password when user is returned
  val userSerializer = FieldSerializer[User](
    FieldSerializer.ignore("password")
  )

  // Sets up automatic case class to JSON output serialization, required by
  // the JValueResult trait.
  protected implicit lazy val jsonFormats: Formats = DefaultFormats + new InstantSerializer + new UUIDSerializer +
    userSerializer

  // global errorHandling for json servlets, for known RestExceptions we return the info embedded
  // in the exception for any unknown exceptions we return a 500 Error.
  error {
    case restExcp: RestException => {
      halt(
        status = restExcp.status,
        body = ErrorResponse(restExcp.status, restExcp.reason, restExcp.message)
      )
    }
    case unknownError : Throwable => {
      val errorText = "An unknown error has occured, if this problem persists please contact support"
      logger.error("Unexpected error occured", unknownError)
      halt(
        status = 500,
        body = ErrorResponse(500, "Internal Server Error", errorText)
      )
    }
  }

}
