package niltf.api.restexception

abstract class RestException (
  theStatus : Int,
  theReason : String,
  theMessage : String
) extends RuntimeException(theMessage) {
  val status = theStatus
  val reason = theReason
  val message = theMessage

}
