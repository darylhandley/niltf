package niltf.api.restexception

class E409Conflict (theMessage : String) extends RestException(E409Conflict.code, E409Conflict.reason, theMessage)

object E409Conflict {
  val code = 409
  val reason = "Conflict"
}
