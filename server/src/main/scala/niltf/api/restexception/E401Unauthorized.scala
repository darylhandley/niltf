package niltf.api.restexception

class E401Unauthorized (theMessage : String) extends RestException(E400Unauthorized.code, E400Unauthorized.reason, theMessage)

object E400Unauthorized  {
  val code = 401
  val reason = "Unauthorized"
}
