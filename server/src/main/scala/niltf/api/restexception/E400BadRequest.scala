package niltf.api.restexception

class E400BadRequest(theMessage : String) extends RestException(E400BadRequest.code, E400BadRequest.reason , theMessage)

object E400BadRequest  {
  val code = 400
  val reason = "Bad Request"
}
