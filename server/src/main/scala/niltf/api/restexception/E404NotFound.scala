package niltf.api.restexception

class E404NotFound (theMessage : String) extends RestException(404, "Not Found", theMessage)

object E404NotFound  {
  val code = 404
  val reason = "Not Found"
}
