package niltf.api.repository

import java.util.UUID

import com.datastax.driver.core.{ResultSet, Row, Session}
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.model.{FileDefinition, Note}
import niltf.api.util.DateUtil

import scala.collection.JavaConverters._

class FileDefinitionRepo(implicit val bindingModule: BindingModule) extends Injectable {

  private val session = inject[Session]

  def get(id : UUID) : Option[FileDefinition] = {
    val cql = "select * from file_definition  where id = ?"
    val rs : ResultSet = session.execute(cql, id)
    val row = rs.one()
    Option(row).map(mapFileDefinition(_))
  }

  def delete(id : UUID): Unit = {
    val cql = "delete from file_definition where id = ? "
    val rs : ResultSet = session.execute(cql, id)
  }

  def save(fileDefinition : FileDefinition) : FileDefinition = {
    val updateCql = "update file_definition set note_id = ?, filename = ? , created_timestamp = ? where id = ? "
    val longDate = java.lang.Long.valueOf(fileDefinition.createdDate.toEpochMilli)
    session.execute(updateCql, fileDefinition.noteId, fileDefinition.filename,
      longDate, fileDefinition.id)
    fileDefinition
  }

  def mapFileDefinition (row : Row) =
    FileDefinition(
      row.getUUID("id"),
      row.getString("filename"),
      row.getUUID("note_id"),
      row.getTimestamp("created_timestamp").toInstant
    )

}

