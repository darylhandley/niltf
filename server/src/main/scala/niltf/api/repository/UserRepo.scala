package niltf.api.repository

import java.util.UUID

import com.datastax.driver.core.{ResultSet, Row, Session}
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.model.{User, Note}
import niltf.api.restexception.E409Conflict
import scala.collection.JavaConverters._

class UserRepo(implicit val bindingModule: BindingModule) extends Injectable {

  private val session = inject[Session]

  def get(id : UUID) : Option[User] = {
    val cql = "select * from user where id = ?"
    val rs : ResultSet = session.execute(cql, id)
    val row = rs.one()
    Option(row).map(mapUser)
  }

  def getByEmail(email : String) : Option[User] = {
    val cql = "select * from user_by_email where email = ?"
    val rs : ResultSet = session.execute(cql, email)
    val row = rs.one()
    Option(row).map(mapUser(_))
  }

  def delete(id : UUID): Unit = {
    val cql = "delete from user where id = ? "
    val rs : ResultSet = session.execute(cql, id)
  }

  def save(user: User) : User = {

    // load existing user
    val dbUserOpt = get(user.id)

    // if new user then check if email exists
    if (dbUserOpt.isEmpty && emailExists(user.email)) {
      throw new E409Conflict("User with email already exists")
    }

    // if existing user and email updated
    //  - check if new email exists
    //  - delete old user_by_email row
    dbUserOpt.filter(_.email != user.email).foreach { dbUser =>
      if (emailExists(user.email)) {
        throw new E409Conflict("User with email already exists")
      }
      val deleteFromUserByEmail = "delete from user_by_email where email = ?"
      session.execute(deleteFromUserByEmail, dbUser.email)
    }



    val longInstant = java.lang.Long.valueOf(user.createdInstant.toEpochMilli)
    val isEmailValidated = new java.lang.Boolean(user.isEmailValidated.booleanValue)

    // save  in user
    val saveInUser = "update user set email = ?, password = ?, is_email_validated = ?, created_instant = ? where id = ?"
    session.execute(saveInUser, user.email, user.password, isEmailValidated, longInstant, user.id)

    // save in user_by_email
    val saveInUserByEmail = "update user_by_email set id = ?, password = ?, is_email_validated = ?, created_instant = ? where email = ? "
    session.execute(saveInUserByEmail, user.id,  user.password, isEmailValidated, longInstant, user.email)

    user
  }

  def emailExists(email : String) = getByEmail(email).isDefined

  def mapUser (row : Row) =
    User(
      row.getUUID("id"),
      row.getString("email"),
      row.getString("password"),
      row.getBool("is_email_validated"),
      row.getTimestamp("created_instant").toInstant
    )

}

