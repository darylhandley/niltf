package niltf.api.repository

import java.util.UUID

import com.datastax.driver.core.{ResultSet, Row, Session}
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.model.{AuthToken, User}
import niltf.api.restexception.E409Conflict

class AuthTokenRepo(implicit val bindingModule: BindingModule) extends Injectable {

  private val session = inject[Session]

  def get(id : String) : Option[AuthToken] = {
    val cql = "select * from auth_token where id = ?"
    val rs : ResultSet = session.execute(cql, id)
    val row = rs.one()
    Option(row).map(mapAuthToken(_))
  }

  def delete(id : String): Unit = {
    val cql = "delete from auth_token where id = ? "
    val rs : ResultSet = session.execute(cql, id)
  }

  def save(authToken: AuthToken) : AuthToken = {
    val javaLong = java.lang.Long.valueOf(authToken.createdInstant.toEpochMilli)
    val saveInUser = "update auth_token set user_id = ?, created_instant = ? where id = ?"
    session.execute(saveInUser, authToken.userId, javaLong, authToken.id)
    authToken
  }


  def mapAuthToken (row : Row) =
    AuthToken(
      row.getString("id"),
      row.getUUID("user_id"),
      row.getTimestamp("created_instant").toInstant
    )

}

