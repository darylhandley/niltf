package niltf.api.repository

import java.time.Instant
import java.util.{Date, UUID}

import com.datastax.driver.core.{Row, ResultSet, Session}
import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import niltf.api.model.Note
import niltf.api.requestmodel.NoteRequest
import scala.collection.JavaConverters._

class NoteRepo(implicit val bindingModule: BindingModule) extends Injectable {

  private val session = inject[Session]

  def get(id : UUID) : Option[Note] = {
    val cql = "select * from note where id = ?"
    val rs : ResultSet = session.execute(cql, id)
    val row = rs.one()
    Option(row).map(mapNote(_))
  }

  def getAll() : List[Note] = {
    val cql = "select * from note"
    val rs : ResultSet = session.execute(cql)
    val notes = rs.all().asScala.map(mapNote(_))
    notes.sortBy(_.createdDate).toList
  }

  def delete(id : UUID): Unit = {
    val cql = "delete from note where id = ? "
    val rs : ResultSet = session.execute(cql, id)
  }

  def save(note : Note) : Note = {
    val updateCql = "update note set user_id = ?, parent_id = ?, title = ?, body = ?, " +
      " created_timestamp = ?, children_ids = ? where id = ? "
    val longDate = java.lang.Long.valueOf(note.createdDate.toEpochMilli)
    val javaChildrenIds = note.childrenIds.asJava
    session.execute(updateCql, note.userId, note.parentId, note.title, note.body, longDate, javaChildrenIds, note.id)
    note
  }

  def mapNote (row : Row) =
    Note(
      row.getUUID("id"),
      row.getUUID("user_id"),
      row.getUUID("parent_id"),
      row.getString("title"),
      row.getString("body"),
      row.getTimestamp("created_timestamp").toInstant,
      row.getList("children_ids", classOf[UUID]).asScala.toList
    )

}

