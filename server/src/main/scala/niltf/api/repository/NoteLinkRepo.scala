package niltf.api.repository

import java.util.UUID

import com.datastax.driver.core.{ResultSet, Row, Session}
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.constants.NoteLinkType
import niltf.api.constants.NoteLinkType.NoteLinkType
import niltf.api.model.{NoteLink, Note}

import scala.collection.JavaConverters._

class NoteLinkRepo(implicit val bindingModule: BindingModule) extends Injectable {

  private val session = inject[Session]

  def get(userId : UUID, noteLinkType: NoteLinkType) : Option[NoteLink] = {
    val cql = "select * from note_link where user_id = ? and note_link_type = ?"
    val rs : ResultSet = session.execute(cql, userId, noteLinkType.toString)
    val row = rs.one()
    Option(row).map(mapNoteLink(_))
  }

  def save(noteLink : NoteLink) : NoteLink = {
    val updateCql = "update note_link set note_id = ? " +
      " where user_id = ? and note_link_type = ?"
    session.execute(updateCql, noteLink.noteId, noteLink.userId, noteLink.noteLinkType.toString)
    noteLink
  }

  def delete(userId : UUID, noteLinkType: NoteLinkType) : Unit  = {
    val cql = "delete from note_link where user_id = ? and note_link_type = ?"
    val rs : ResultSet = session.execute(cql, userId, noteLinkType.toString)
  }

  def mapNoteLink (row : Row) = {
    NoteLink(
      row.getUUID("user_id"),
      NoteLinkType.withName(row.getString("note_link_type")),
      row.getUUID("note_id")
    )
  }

}

