package niltf.api.requestmodel

import java.util.UUID

case class NoteRequest (
  title : Option[String],
  body : Option[String],
  parentId : Option[UUID],
  childrenIds : Option[List[UUID]]
)
