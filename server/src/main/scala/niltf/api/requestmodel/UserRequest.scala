package niltf.api.requestmodel

import java.util.UUID

case class UserRequest(
  email : Option[String],
  password : Option[String]
)
