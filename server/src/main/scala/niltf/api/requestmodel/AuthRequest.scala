package niltf.api.requestmodel

case class AuthRequest (
  email : String,
  password : String
)
