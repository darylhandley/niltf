package niltf.api.play

import java.util.UUID

import com.datastax.driver.core.{Row, Cluster, ResultSet}
import niltf.api.model.Note

import scala.collection.JavaConversions._
import scala.collection.JavaConverters._

class CassandraPlay {


}

object CassandraPlay {

  def main (args : Array[String]): Unit = {

    println("Creating a Cassandra cluster")
    val cluster = Cluster.builder()
      .addContactPoint("127.0.0.1")
      .build()
    val session  = cluster.connect("niltf")

    val noteId = "1f1e14ce-61f0-4fcb-9e2b-40efd1df301d"
    val cql = "select * from note where id = " + noteId;
    val rs : ResultSet = session.execute(cql)
    println(rs.all())

    println("done")

    session.close()

  }

  def mapNote (row : Row) =
    Note(
      row.getUUID("id"),
      row.getUUID("user_id"),
      row.getUUID("parent_id"),
      row.getString("title"),
      row.getString("body"),
      row.getTimestamp("created_date").toInstant,
      null
    )

}
