package niltf.api.play

import java.util.UUID

import com.datastax.driver.core.{ResultSet, Cluster}

class CassandraPrepStatement {

}

object CassandraPrepStatement {

  def main (args : Array[String]): Unit = {

    println("Creating a Cassandra cluster")
    val cluster = Cluster.builder()
      .addContactPoint("127.0.0.1")
      .build()
    val session  = cluster.connect("niltf")

    val noteId = UUID.fromString("1f1e14ce-61f0-4fcb-9e2b-40efd1df301d")

    val cql = "select * from note where id = ?"
    val rs : ResultSet = session.execute(cql, noteId)
    println(rs.all())

    println("done")

    session.close()

  }

}
