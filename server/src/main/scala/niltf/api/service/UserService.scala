package niltf.api.service

import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.constants.{Messages, NoteLinkType}
import niltf.api.model.{Note, NoteLink, User}
import niltf.api.repository.{NoteLinkRepo, NoteRepo, UserRepo}
import niltf.api.requestmodel.{NoteRequest, UserRequest}
import niltf.api.restexception.{E400BadRequest, E409Conflict}
import niltf.api.util.SecurityUtil

case class UserService(implicit val bindingModule: BindingModule)
  extends Injectable {

  val noteLinkRepo = inject[NoteLinkRepo]
  val noteRepo = inject[NoteRepo]
  val userRepo = inject[UserRepo]

  def get(id : UUID, userId : UUID) : Option[User] = {
    None
  }

  def create(userRequest : UserRequest): User = {
    // validate email and password
    val email = userRequest.email match {
      case Some("") | None => throw new E400BadRequest(Messages.EmailIsARequiredField)
      case Some(string) => string
    }

    val password = userRequest.password match {
      case Some("") | None => throw new E400BadRequest(Messages.PasswordIsARequiredField)
      case Some(string) => string
    }

    // validate the password
    if (!SecurityUtil.validate(password)) {
      throw new E400BadRequest(Messages.PasswordNotValid)
    }

    // check user with email exists

    // create the user
    val user = User(
      id = UUID.randomUUID(),
      email = email,
      password = SecurityUtil.hashPassword(password),
      createdInstant = Instant.now
    )
    userRepo.save(user)

    // create the top level note
    val noteId = UUID.randomUUID
    val topLevelNote = Note(
      id = noteId,
      userId = user.id,
      parentId = noteId,
      title = "Top Level Note",
      body = "",
      childrenIds = List()
    )
    val noteLink = NoteLink(
      userId = user.id,
      noteId = noteId,
      noteLinkType = NoteLinkType.TOP_LEVEL_NOTE
    )
    noteRepo.save(topLevelNote)
    noteLinkRepo.save(noteLink)

    user
  }



  def update(user : User, userRequest: UserRequest) : User = {
    user
  }


}
