package niltf.api.service

import java.time.Instant
import java.util.UUID

import com.datastax.driver.core.ResultSet
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.constants.NoteLinkType
import niltf.api.model.{Note, NoteLink, User}
import niltf.api.repository.{NoteLinkRepo, NoteRepo}
import niltf.api.requestmodel.NoteRequest
import niltf.api.restexception.{E400BadRequest, E404NotFound, E409Conflict}

case class NoteService(implicit val bindingModule: BindingModule)
  extends Injectable {

  val noteLinkRepo = inject[NoteLinkRepo]
  val noteRepo = inject[NoteRepo]

  def get(id : UUID, userId : UUID) : Option[Note] = {
    // return note iff we find it and it matches the userId
    noteRepo.get(id).filter(_.userId == userId)
  }

  def getTopLevelNotes(userId : UUID) : List[Note] = {
    val note = getTopLevelNote(userId)
    getChildren(note)
  }


  def getChildren(note : Note) : List[Note] = {
    // TODO: this is probably slow (n+1 query)
    note.childrenIds.map(childId =>
      noteRepo.get(childId)
        .getOrElse(throw new IllegalStateException("Note not found for childId " + childId)
        )
    )
  }

  def getTopLevelNote(userId : UUID) : Note = {
    val noteLink: NoteLink = noteLinkRepo.get(userId, NoteLinkType.TOP_LEVEL_NOTE).getOrElse(
      throw new IllegalStateException("User does not have a top level note link")
    )

    val note = noteRepo.get(noteLink.noteId).getOrElse(
      throw new IllegalStateException("Note not found for top level note link, noteId=" + noteLink.noteId)
    )
    note
  }

  def getAll() : List[Note] = noteRepo.getAll()

  // delete the note if it is owned by the uuid passed in
  def delete(id : UUID, userId: UUID): Unit = {

    get(id, userId) foreach {note =>

      // if parentId == id then it is a top level note and we can't delete it
      if (note.id == note.parentId) {
        throw new E409Conflict("Can not delete top level note")
      }

      // delete all child notes
      // this will recursively delete all descendants
      note.childrenIds.foreach(childId => delete(childId, userId))

      // update the parent note to remove this child from it's list
      noteRepo.get(note.parentId) foreach (parentNote => {
          val newChildrenIds = parentNote.childrenIds.filter(_ != note.id)
          noteRepo.save(parentNote.copy(childrenIds = newChildrenIds))
        }
      )

      // delete the note itself
      noteRepo.delete(id)
    }
  }

  // Returns a note (if found) by the path to it in the tree from a users top level note.
  //
  // see the documentation getByNoteIdAndPath for more information. Note that this can be slow and should
  // not be used for production code, however is useful for testing.
  def getByUserIdAndPath(userId : UUID, path : Seq[Int]): Option[Note] = {
    val topLevelNotes = getTopLevelNote(userId)
    getByNoteIdAndPath(topLevelNotes.id, path)
  }

  // Returns a note (if found) by the path to it in the tree from another note. The path is a sequence of
  // numbers that define the position in the child lists (kind of like the index in a multi dimensional array) .
  // Note this can be quite slow with deeply nested paths and should really only be used for testing.
  //
  // For example if path is Seq((2,3,1) it will recursively
  // - find the 2nd child of the noteId passed in
  // - find the 3rd child of the above note
  // - find the 1st child of the above note
  def getByNoteIdAndPath(id : UUID, path: Seq[Int]): Option[Note] = {
    val noteOpt = noteRepo.get(id)
    path.size match {
      case 0 => noteOpt
      case _ => {
        noteOpt match {
          case None => noteOpt
          case Some(note) => {
            val idx = path.head - 1
            if (note.childrenIds.isDefinedAt(idx)) {
              getByNoteIdAndPath(note.childrenIds(idx) , path.drop(1))
            } else {
              None
            }
          }
        }
      }
    }
  }

  def create(user : User, noteRequest : NoteRequest): Note = {

    // no childrenIds allowed in create
    if (noteRequest.childrenIds.exists(_.size > 0)) {
      throw new E400BadRequest("Can not set childrenIds in post, first create the note " +
        "then add children after with PATCH")
    }

    // get the parent note, if passed in use the note passed in else use the topLevel
    val parentNoteOpt : Option[Note] = noteRequest.parentId match {
      case Some(parentId) => get(parentId, user.id)
      case _ => Some(getTopLevelNote(user.id))
    }
    val parentNote : Note = parentNoteOpt.getOrElse(throw new E400BadRequest(s"Note ${noteRequest.parentId} not found"))

    // create the note
    val note = Note(
      UUID.randomUUID,
      user.id,
      parentNote.id,
      noteRequest.title.getOrElse(""),
      noteRequest.body.getOrElse(""),
      Instant.now,
      List()
    )
    noteRepo.save(note)

    // update the parent note by appending the new note to the children
    val parentNoteUpdated = parentNote.copy(
      childrenIds = parentNote.childrenIds :+ note.id
    )
    noteRepo.save(parentNoteUpdated)

    note
  }

  def update(note : Note, noteRequest: NoteRequest) : Note = {

    var updatedNote = note.copy(
      title = noteRequest.title.getOrElse(note.title),
      body = noteRequest.body.getOrElse(note.body)
    )

    val parentIdUpdated = noteRequest.parentId.exists(_ != note.parentId)
    val childrenIdsUpdated = noteRequest.childrenIds.exists(_ != note.childrenIds)

    // check if client is trying to update childrenIds and parentId at the same time. This is not allowed because it is
    // generally not needed and makes our logic too complicated and potentially slow. Can revisit later and see if
    // there is a good solution that we can come up with to solve this but for now it is more hassle than it's worth
    if (parentIdUpdated && childrenIdsUpdated) {
      throw new E400BadRequest("Can not update parentId and childrenIds in the same request")
    }

    parentIdUpdated match {
      case true => {
        val parentNote = get(noteRequest.parentId.get, note.userId)
        validateNewParentId(noteRequest.parentId.get, parentNote, note)
        updateChildNotes(note.parentId, parentNote.get, note)
        updatedNote = updatedNote.copy(parentId = parentNote.get.id)
      }
      case false => {}
    }

    childrenIdsUpdated match {
      case true => {
        // check for duplicates
        if (noteRequest.childrenIds.get.toSet.size != noteRequest.childrenIds.get.size) {
          throw new E400BadRequest("Can not update note as some children are added more than once.")
        }
        // check if we are orphaning any notes, a note is orphaned if it is removed from the list of children
        // in a children update since we don't know who it's parent will be
        if (!(note.childrenIds forall (noteRequest.childrenIds.get.contains(_)))) {
          throw new E400BadRequest("Can not update note children as it would result in orphaned note(s)")
        }

        // get the children that we are adding
        val childrenAdded : List[UUID] = noteRequest.childrenIds.get.filterNot(note.childrenIds.contains(_))

        // val parentNote = get(noteRequest.parentId.get, note.userId)
        // validateNewParentId(noteRequest.parentId.get, parentNote, note)
        // updateChildNotes(note.parentId, parentNote.get, note)
        updatedNote = updatedNote.copy(childrenIds = noteRequest.childrenIds.get)

        // for all the children added
        // - check that the child is not a predescessor  of the note we are adding to
        // - update the parentId of the child
        // - remove the child from the old parent
        childrenAdded.foreach(childUuid  => {
          noteRepo.get(childUuid).foreach(child => {

            if (isNote1PredescessorOfNote2(child, note)) {
              throw new E400BadRequest(
                s"Can not set note (${child.id}) to be descendant of (${note.id}) as it would cause a " +
                  s"circular reference"
              )
            }

            noteRepo.save(child.copy(parentId = note.id))

            noteRepo.get(child.parentId).foreach(oldParent => {
              val oldParentsNewChildrenIds = oldParent.childrenIds.filterNot(_ == childUuid)
              noteRepo.save(oldParent.copy(childrenIds = oldParentsNewChildrenIds))
            })
          })
        })
      }
      case false => {}
    }

    noteRepo.save(updatedNote)
  }

  /*
   * When moving a note to a new parent we need to validate the new parent to make sure it exists and is a
   * viable parent
   */
  private def validateNewParentId(newParentId : UUID, newParentNote : Option[Note], note : Note): Unit = {

    // check parentId exists for user
    if (!newParentNote.isDefined) {
      throw new E400BadRequest(s"Parent note $newParentId not found")
    }

    // check note we are updating is not top level note
    val topLevelNote = getTopLevelNote(note.userId)
    if (topLevelNote.id == note.id) {
      throw new E400BadRequest("Can not set parent note for top level note")
    }

    // check note is not a predecessor of targetNote
    if (isNote1PredescessorOfNote2(note, newParentNote.get)) {
      throw new E400BadRequest(
        s"Can not set parentId (${newParentNote.get.id}) to a descendant of targetNote (${note.id})"
      )
    }

  }

  private def updateChildNotes(oldParentId : UUID, parentNote : Note, note : Note): Unit = {
      // update children on parent note
      val newChildrenIds = parentNote.childrenIds :+ note.id
      noteRepo.save(parentNote.copy(childrenIds = newChildrenIds ))

      // remove note from children for old parentNote
      get(oldParentId, note.userId).foreach(oldParentNote => {
          val newChildrenIdsOldParent = oldParentNote.childrenIds.filterNot(_ == note.id)
          noteRepo.save(oldParentNote.copy(childrenIds = newChildrenIdsOldParent))
        }
      )
  }

  /**
    * Returns true if note1 is a predecessor (nested parent) of the note2 by recursively walking up the
    * tree from note2 and seeing if we hit note1. We use to check if we can move a note without causing
    * a circular reference.
    *
    * Example given the note structure below
    * - we can't move note_1 to note_1_1_2 because it would then be an ancestor of itself (circular reference)
    * - we can move note_1 to note_2_1 since no circular references will result
    *
    * - note_1
    *   - note 1_1
    *     - note 1_1_1
    *     - note_1_1_2
    *   - note 1_2
    * - note_2
    *   - note 2_1
    *   - note 2_2
    */
  private def isNote1PredescessorOfNote2(note1 : Note, note2 : Note): Boolean = {
    note2.id match {
      // found it, it's a predecesor
      case note1.id => true
      // top level not stop
      case note2.parentId => false
      // no match and not done, recurse to parent
      case _ => {
        noteRepo.get(note2.parentId) match {
          case None => throw new IllegalStateException(s"Note ${note1.id} parent was not found parent id is ${note1.parentId}")
          case Some(nextParent) => isNote1PredescessorOfNote2(note1, nextParent)
        }
      }
    }
  }



}
