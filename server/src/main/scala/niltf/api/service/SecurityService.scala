package niltf.api.service

import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.model.{AuthToken, User, Note}
import niltf.api.repository.{AuthTokenRepo, UserRepo, NoteRepo}
import niltf.api.requestmodel.NoteRequest
import niltf.api.util.SecurityUtil

case class SecurityService(implicit val bindingModule: BindingModule)
  extends Injectable {

  val userRepo = inject[UserRepo]
  val authTokenRepo = inject[AuthTokenRepo]

  var tokenUserMap = Map[String, User]()

  // authenticate returns a tuple with authToken and user if valid and None if not
  def authenticate(email : String, password: String) : Option[(String, User)] = {

    val userOpt = userRepo.getByEmail(email)

    // check the password
    userOpt.filter {user =>
      SecurityUtil.validatePassword(password, user.password)
    }.map{ user =>
        val authToken = new AuthToken(
          UUID.randomUUID().toString,
          user.id,
          Instant.now
        )
        authTokenRepo.save(authToken)
        (authToken.id.toString, user)
    }

  }

  // used for logout
  def removeAuthToken(authToken : String) {
    authTokenRepo.delete(authToken)
  }

  def getUserByAuthToken(authToken : String) : Option[User] = {
    val tokenOpt = authTokenRepo.get(authToken)
    tokenOpt match {
      case Some(token) => userRepo.get(token.userId)
      case None => None
    }
  }





}
