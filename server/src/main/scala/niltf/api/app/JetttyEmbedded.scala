package niltf.api.app

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.webapp.WebAppContext

object JettyEmbedded {

  def main(args: Array[String]) {
    val server = new Server(7273)
    val context: WebAppContext = new WebAppContext()
    context.setServer(server)
    context.setContextPath("/api/niltf/1")
    context.setWar("src/main/webapp")
    server.setHandler(context)

    try {
      server.start()
      server.join()
    } catch {
      case e: Exception => {
        e.printStackTrace()
        System.exit(1)
      }
    }
  }
}