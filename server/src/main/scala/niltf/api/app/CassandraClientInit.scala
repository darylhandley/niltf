package niltf.api.app

import com.datastax.driver.core.{ResultSet, Session, Cluster}
import niltf.api.model.Note
import org.slf4j.LoggerFactory

/**
  * A trait we mix into Scalatra's initalization lifecycle to ensure we've got
  * a Cassandra Session set up after the application starts.
  *
  * We're not using this anymore since we now use subcut for dependency injection, I'll keep it around for now
  * in case I ever write up a tutorial to give back to scalatra.
  *
  * See DefaultBindings in this package for how we do it now.
  *
 */
@Deprecated
trait CassandraClientInit {
  val logger = LoggerFactory.getLogger(getClass)

  var cluster : Cluster = null
  var session : Session = null

  def configureCassandraSession() {
    logger.info("Creating a Cassandra cluster")

    cluster = Cluster.builder()
        .addContactPoint("127.0.0.1")
        .build()
    session  = cluster.connect()
  }


  def closeCassandraSession() {
    logger.info("Closing Cassandra cluster client")
    if (cluster != null) cluster.close()
  }

}