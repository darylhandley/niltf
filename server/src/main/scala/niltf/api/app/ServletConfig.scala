package niltf.api.app

import com.escalatesoft.subcut.inject.BindingModule
import niltf.api.servlet._
import org.scalatra.ScalatraServlet

case class ServletConfig(
  servlet : ScalatraServlet,
  urlPattern : String
)

object ServletConfig {
  def createAll(implicit bindingModule: BindingModule) : List[ServletConfig] = {

    ServletConfig(new NotesServlet, "/notes/*") ::
    ServletConfig(new UsersServlet, "/users/*") ::
    ServletConfig(new AuthServlet, "/auth/*") ::
    ServletConfig(new NiltfServlet, "/*") ::
    ServletConfig(new FilesServlet, "/files/*") ::
    Nil
  }

}
