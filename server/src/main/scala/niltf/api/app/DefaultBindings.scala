package niltf.api.app
import com.typesafe.config.ConfigFactory
import com.datastax.driver.core.{Cluster, Session}
import com.escalatesoft.subcut.inject.NewBindingModule
import niltf.api.repository._
import niltf.api.service.{NoteService, SecurityService, UserService}

import scala.Some

// this defines which components are available for this module
object DefaultBindings extends NewBindingModule(module => {
  import module._   // can now use bind directly

  val config = ConfigFactory.load()

  // create the cassandra session
  val cassandraContactPoints = config.getString("cassandra.contactPoints")
  val cassandraKeyspace = config.getString("cassandra.keyspace")
  val cluster = Cluster.builder()
    .addContactPoint(cassandraContactPoints)
    .build()
  val session  = cluster.connect(cassandraKeyspace)

  // bind all our objects so that they can be wired together  

  // cassandra session
  bind [Session] toSingle session

  // repositories
  bind [NoteRepo] toModuleSingle { implicit module => new NoteRepo}
  bind [UserRepo] toModuleSingle { implicit module => new UserRepo}
  bind [AuthTokenRepo] toModuleSingle { implicit module => new AuthTokenRepo}
  bind [NoteLinkRepo] toModuleSingle { implicit module => new NoteLinkRepo}
  bind [FileDefinitionRepo] toModuleSingle { implicit module => new FileDefinitionRepo}

  // services
  bind [NoteService] toModuleSingle { implicit module => new NoteService}
  bind [SecurityService] toModuleSingle { implicit module => new SecurityService}
  bind [UserService] toModuleSingle { implicit module => new UserService}


})
