package niltf.api.jsonserializer

import java.time.Instant
import java.util.UUID

import niltf.api.util.DateUtil
import org.json4s.CustomSerializer
import org.json4s.JsonAST.JString

class UUIDSerializer extends CustomSerializer[UUID](

  format => (
    {
      case JString(stringVal) => {
        UUID.fromString(stringVal)
      }
    },
    {
      case uuid: UUID =>
        JString(uuid.toString)
    }
    ))
