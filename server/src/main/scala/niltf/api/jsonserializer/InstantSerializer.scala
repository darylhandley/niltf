package niltf.api.jsonserializer

import java.time.Instant
import java.util.Date

import niltf.api.util.DateUtil
import org.json4s.{DefaultFormats, CustomSerializer}
import org.json4s.JsonAST.JString

/** Serialize and deserialize an instant */
class InstantSerializer extends CustomSerializer[Instant](

  format => (
  {
    case JString(stringVal) => {
      DateUtil.fromIso8601(stringVal)
    }
  },
  {
    case instant: Instant =>
      JString(DateUtil.toIso8601(instant))
  }
))
