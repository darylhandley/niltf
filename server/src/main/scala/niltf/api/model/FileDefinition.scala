package niltf.api.model

import java.time.Instant
import java.util.UUID

case class FileDefinition (
  id: UUID,
  filename : String,
  noteId : UUID,
  createdDate : Instant = Instant.now(),
  path : String = ""

) {
  // TODO: need to figure out how to return this in the json
  // def path = s"/files/$id/$filename"
}