package niltf.api.model

import java.util.UUID

import niltf.api.constants.NoteLinkType.NoteLinkType

/**
 * Used to represent "Top Level Notes". These are notes that do not show up in the display, however they
 * are used mostly as a container for their child notes. Currently we only have our TopLevelNote which is the
 * placeholder to hold a list of children ids for the Notes that show up in the  top level of the tree. In the
 * future we may also have an oject for Inbox and Recylce bin.
 *
 *  Unique key is the userId and noteLinktype hence you can only have a single NoteLink per user and noteLinkType.
 */
case class NoteLink (userId : UUID, noteLinkType : NoteLinkType, noteId : UUID)
