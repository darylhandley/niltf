package niltf.api.model

import java.time.Instant
import java.util.{Date, UUID}

import org.joda.time.DateTime

case class Note(
  id: UUID,
  userId : UUID,
  parentId : UUID,
  title : String,
  body: String,
  createdDate : Instant = Instant.now(),
  childrenIds : List[UUID]
) {

  // Returns a copy of the note with the child uuid to the list of children,
  def appendChild(childId : UUID) : Note = {
    this.copy(childrenIds = this.childrenIds ::: List(childId))
  }

  // Returns a copy of the note with the child uuid to the list of children,
  def childrenIdsAsStrings() : List[String]  = {
    childrenIds.map(_.toString)
  }
}
