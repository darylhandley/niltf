package niltf.api.model

import java.time.Instant
import java.util.UUID

case class User(
  id: UUID,
  email : String = "",
  password : String = "",
  isEmailValidated : Boolean = false,
  createdInstant : Instant

)
