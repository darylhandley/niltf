package niltf.api.model

import java.time.Instant
import java.util.UUID

case class AuthToken(
  id: String,
  userId : UUID,
  createdInstant : Instant
)
