package niltf.api.constants

/* Only a single type for now but we expect to add other types for inbox and recycle bin */
object NoteLinkType extends Enumeration {
  type NoteLinkType = Value
  val TOP_LEVEL_NOTE = Value
}
