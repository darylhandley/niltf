package niltf.api.constants

object Messages {


  val EmailNotValid = "Email is a required field"
  val EmailIsARequiredField = "Email is a required field"
  val EmailExists  = "User with email already exists"
  val PasswordIsARequiredField = "Password is a required field"
  val PasswordNotValid = "Password is not valid"


}
