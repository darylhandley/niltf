package niltf.api.util

import org.mindrot.jbcrypt.BCrypt

object SecurityUtil {

  val LogRounds = 12
  val PasswordMinLength = 4

  def validatePassword(password: String, hashedPassword : String) = {
    BCrypt.checkpw(password, hashedPassword)
  }

  def hashPassword(password: String): String = {
    BCrypt.hashpw(password, BCrypt.gensalt(LogRounds))
  }

  /** Validate the password to see if it meets the rules */
  def validate(password: String) = {
    password.length >= 4
  }

}