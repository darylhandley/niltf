package niltf.api.util

import java.time.temporal.ChronoUnit
import java.time.{ZoneOffset, Instant}
import java.time.format.DateTimeFormatter
import java.util.Date

object DateUtil {

  val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(ZoneOffset.UTC)


  def toIso8601(date : Date): String = formatter.format(date.toInstant)

  def toIso8601(instant : Instant) : String = formatter.format(instant)

  def fromIso8601(string : String) : Instant = Instant.parse(string)

  def removeMillis(instant: Instant) : Instant = instant.truncatedTo(ChronoUnit.SECONDS)

}
