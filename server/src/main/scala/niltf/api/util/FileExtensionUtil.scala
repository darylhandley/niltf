package niltf.api.util

import java.util.Date

object FileExtensionUtil {
  private val extToContentType = Map(
    "gif" -> "image/gif",
    "jpg" -> "image/jpg",
    "jpeg" -> "image/jpeg",
    "png" -> "image/png"
  )

  def filepathToContentType(filepath : String): Option[String] = {
    val ext = filepath.substring(filepath.lastIndexOf(".") + 1)
    extToContentType.get(ext)
  }

}
