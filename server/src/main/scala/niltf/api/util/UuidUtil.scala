package niltf.api.util


object UuidUtil {

  def isValid (uuid: String) : Boolean = {
    val pattern =  "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
    uuid.toLowerCase.matches(pattern)
  }


}
