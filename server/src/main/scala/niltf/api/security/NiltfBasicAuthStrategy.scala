package niltf.api.security


import java.time.Instant
import java.util.UUID
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}

import niltf.api.model.User
import org.scalatra.auth.strategy.{BasicAuthStrategy, BasicAuthSupport}
import org.scalatra.auth.{ScentrySupport, ScentryConfig}
import org.scalatra.{ScalatraBase}

class NiltfBasicAuthStrategy(protected override val app: ScalatraBase, realm: String)
  extends BasicAuthStrategy[User](app, realm) {

  protected def validate (userName: String, password: String) (implicit request: HttpServletRequest, response: HttpServletResponse): Option[User] = {
      if (userName == "daryl" && password == "meatloaf") {
        Some(
          User(
            UUID.randomUUID(),
            "email",
            "password",
            true,
            Instant.now
          )
        )
      }
      else {
        None
      }
  }

  override protected def getUserId (user: User) (implicit request: HttpServletRequest, response: HttpServletResponse): String = user.id.toString


}