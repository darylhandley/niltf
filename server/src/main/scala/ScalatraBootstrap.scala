

import javax.servlet.ServletContext

import com.escalatesoft.subcut.inject
import com.escalatesoft.subcut.inject.util.PropertyFileModule
import niltf.api.app.{DefaultBindings, ServletConfig, CassandraClientInit}
import niltf.api.repository.NoteRepo
import niltf.api.service.NoteService
import niltf.api.servlet.{NiltfServlet, NotesServlet}
import org.scalatra._

class ScalatraBootstrap extends LifeCycle with CassandraClientInit {

  override def init(context: ServletContext) {
    // no longer used but keep around in case I wrtie a tutorial
    // configureCassandraSession()

    // this is the subcut binding module it is used to do dependency injection
    // the implicit is passed to all our Servlets that we create below
    implicit val bindingModule = DefaultBindings

    ServletConfig.createAll.foreach { config =>
      context.mount(config.servlet,config.urlPattern)
    }
  }

  // no longer used but keep around in case I wrtie a tutorial
//  override def destroy(context: ServletContext) {
//    closeCassandraSession()
//  }
}
