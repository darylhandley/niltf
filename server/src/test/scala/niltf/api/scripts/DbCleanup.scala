package niltf.api.scripts

import java.time.Instant
import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger

import com.datastax.driver.core.{Cluster, ResultSet, Session}
import com.escalatesoft.subcut.inject.Injectable
import niltf.api.app.DefaultBindings
import niltf.api.constants.NoteLinkType
import niltf.api.model.{Note, NoteLink, User}
import niltf.api.repository.{NoteLinkRepo, NoteRepo, UserRepo}
import niltf.api.test.{TestBindings, TestFixture, UUIDSpoof}

import scala.collection.JavaConverters._

/**
  * Useful object for dev for cleaning up db when something goes awry. Connects to niltf by default
  */
object DbCleanup extends Injectable {

  val noteSeq = new AtomicInteger(100)

  implicit val bindingModule = DefaultBindings ~ TestBindings

  val testFixture = inject[TestFixture]
  val session = inject[Session]
  val noteRepo = inject[NoteRepo]
  val noteLinkRepo = inject[NoteLinkRepo]
  val userRepo = inject[UserRepo]

  def main (args : Array[String]): Unit = {

    deleteAllNotesForDaryl(session)

    val user = userRepo.get(UUID.fromString("55f7b35a-8243-4472-ad03-3bc534fa4d9a")).get
    createNoteStructureForUser(user, "Daryl")

    println("done")

  }

  /* Delete all the notes for my user and set top level children to the empty list. Use it to fix up a borked db */
  def deleteAllNotesForDaryl(session : Session): Unit = {

    val cql = "select id,title from note where user_id = 55f7b35a-8243-4472-ad03-3bc534fa4d9a " +
      " allow filtering;"
    var rs : ResultSet = session.execute(cql)
    rs.all().asScala
      // .filter(_.getString("title") != "Top Level Note")
      .foreach(row => {
        println(row.getString("title"))
        val cqlDelete = "delete from note where id = ?"
        session.execute(cqlDelete, row.getUUID("id"))
      })


  }


  // creates a note structure for the user passed in. Uses the name to differntiate the bodys and titles between
  // different users
  //
  // example : initial structure for name=Gilfoyle will look like this
  //  - Gilfoyle Note 1, Gilfoyle Body 1
  //    - Gilfoyle Note 1_1, Gilfoyle Body 1_1
  //    - Gilfoyle Note 1_2, GilFoyle Body 1_2
  //  - Gilfoyle Note 2, Gilfoyle Body 2
  //    - Gilfoyle Note 2_1, Gilfoyle Body 2_2
  //    - Gilfoyle Note 2_2, Gilfoyle Body 2_2
  //    - Gilfoyle Note 2_3, Gilfoyle Body 2_3
  //      - Gilfoyle Note 2_3_1, Gilfoyle Body 2_3_1
  //        - Gilfoyle Note 2_3_1_1, Gilfoyle Body 2_3_1_1
  def createNoteStructureForUser(user : User, name : String): Unit = {

    val templateNote = Note(
      UUIDSpoof.note(1),
      user.id,
      UUIDSpoof.note(1),
      "",
      "",
      Instant.now(),
      List()
    )

    // create top level note
    val topLevelId = UUIDSpoof.note(noteSeq.getAndIncrement)
    var topLevel = templateNote.copy(
      parentId = topLevelId,
      id = topLevelId,
      title =  name + " top level",
      body = ""
    )

    var note1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = topLevel.id,
      title =  name + " title 1",
      body = name + " body 1"
    )
    topLevel = topLevel.appendChild(note1.id)

    val note1_1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note1.id,
      title =  name + " title 1_1",
      body = name + " body 1_1"
    )
    note1 = note1.appendChild(note1_1.id)

    val note1_2 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note1.id,
      title =  name + " title 1_2",
      body = name + " body 1_2"
    )
    note1 = note1.appendChild(note1_2.id)

    var note2 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = topLevel.id,
      title =  name + " title 2",
      body = name + " body 2"
    )
    topLevel = topLevel.appendChild(note2.id)

    val note2_1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2.id,
      title =  name + " title 2_1",
      body = name + " body 2_1"
    )
    note2 = note2.appendChild(note2_1.id)

    val note2_2 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2.id,
      title =  name + " title 2_2",
      body = name + " body 2_2"
    )
    note2 = note2.appendChild(note2_2.id)

    var note2_3 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2.id,
      title =  name + " title 2_3",
      body = name + " body 2_3"
    )
    note2 = note2.appendChild(note2_3.id)

    var note2_3_1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2_3.id,
      title =  name + " title 2_3_1",
      body = name + " body 2_3_1"
    )
    note2_3 = note2_3.appendChild(note2_3_1.id)

    val note2_3_1_1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2_3_1.id,
      title =  name + " title 2_3_1_1",
      body = name + " body 2_3_1_1"
    )
    note2_3_1 = note2_3_1.appendChild(note2_3_1_1.id)

    // save them all
    val allNotes = List(topLevel, note1, note1_1, note1_2, note2, note2_1, note2_2, note2_3, note2_3_1, note2_3_1_1)
    // val allNotes = List(topLevel, note1, note1_1, note1_2, note2, note2_1, note2_2)
    allNotes.foreach(noteRepo.save(_))

    // set the top level noteLink
    val noteLink = new NoteLink(
      user.id,
      NoteLinkType.TOP_LEVEL_NOTE,
      topLevel.id
    )
    noteLinkRepo.save(noteLink)
  }




}
