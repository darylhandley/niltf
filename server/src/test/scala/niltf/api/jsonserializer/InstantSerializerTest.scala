package niltf.api.jsonserializer

import java.time.{Instant, ZoneId, ZonedDateTime}
import java.util.Date

import org.json4s.jackson.Serialization
import org.json4s.{DefaultFormats, NoTypeHints}
import org.scalatest.{FunSuite, Matchers}

class InstantSerializerTest extends FunSuite with Matchers {

  val utc  = ZoneId.of("UTC")
  val pst  = ZoneId.of("Canada/Pacific")

  import Serialization.{read, write}

  implicit val formats = DefaultFormats + new InstantSerializer

  // see https://github.com/json4s/json4s/blob/3.3/tests/src/test/scala/org/json4s/native/SerializationExamples.scala

  test("shoud serialize instant to ISO8601") {
    val utcDateTime = ZonedDateTime.of(2016, 1, 5, 10, 36, 27, 123000000, utc)
    val result = write(utcDateTime.toInstant)
    result should equal("\"2016-01-05T10:36:27Z\"")
  }

  test("shoud deserialze ISO8601 to instant") {
    val utcDateTime = ZonedDateTime.of(2016, 1, 5, 10, 36, 27, 0, utc)
    val string = "\"2016-01-05T10:36:27Z\""
    read[Instant](string) should equal(utcDateTime.toInstant)
  }



}
