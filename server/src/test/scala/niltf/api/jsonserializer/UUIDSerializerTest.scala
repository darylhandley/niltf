package niltf.api.jsonserializer

import java.time.{Instant, ZoneId, ZonedDateTime}
import java.util.UUID

import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization
import org.scalatest.{FunSuite, Matchers}

class UUIDSerializerTest extends FunSuite with Matchers {

  import Serialization.{read, write}

  implicit val formats = DefaultFormats + new UUIDSerializer

  // see https://github.com/json4s/json4s/blob/3.3/tests/src/test/scala/org/json4s/native/SerializationExamples.scala

  test("shoud serialize uuid") {
    val uuid = UUID.fromString("5aa77d80-1a6c-436e-8c98-9c75ce2175fa")
    val result = write(uuid)
    result should equal("\"5aa77d80-1a6c-436e-8c98-9c75ce2175fa\"")
  }

  test("shoud deserialze uuid") {
    val uuid = UUID.fromString("5aa77d80-1a6c-436e-8c98-9c75ce2175fa")
    val inString  = "\"5aa77d80-1a6c-436e-8c98-9c75ce2175fa\""
    read[UUID](inString) should equal(uuid)
  }



}
