package niltf.api.test

import niltf.api.servlet.ErrorResponse

object TestConstants {

  // we'll use this test password for most (all ?) of our test users
  val TestUserPassword = "meatloaf1"

  val AuthTokenNotSuppliedErorResponse = ErrorResponse(401, "Unauthorized", "Auth Token was not supplied")


}
