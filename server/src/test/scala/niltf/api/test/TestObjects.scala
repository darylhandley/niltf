package niltf.api.test

import niltf.api.model.User

case class TestObjects(users : Users)

case class Users(gilfoyle : User, dinesh : User)
