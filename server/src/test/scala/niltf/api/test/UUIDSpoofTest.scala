package niltf.api.test

import java.time.ZonedDateTime
import java.util.UUID

import niltf.api.util.DateUtil
import org.scalatest.{Matchers, FunSuite}

class UUIDSpoofTest extends FunSuite with Matchers {

  test("Should spoof uuid") {
    UUIDSpoof.of(1) should equal (UUID.fromString("00000000-0000-0000-0000-000000000001"))
    UUIDSpoof.of(2) should equal (UUID.fromString("00000000-0000-0000-0000-000000000002"))
    UUIDSpoof.of(100) should equal (UUID.fromString("00000000-0000-0000-0000-000000000100"))
    UUIDSpoof.of(999999999) should equal (UUID.fromString("00000000-0000-0000-0000-000999999999"))
  }

  test("should throw exception for negative integer") {
    an[IllegalArgumentException] should be thrownBy {
      UUIDSpoof.of(-2)
    }

  }


}
