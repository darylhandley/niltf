package niltf.api.test

import com.escalatesoft.subcut.inject.Injectable
import niltf.api.app.{DefaultBindings, ServletConfig}
import niltf.api.jsonserializer.{InstantSerializer, UUIDSerializer}
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.scalatest.{BeforeAndAfter, FunSuiteLike}
import org.scalatra.test.scalatest._

class BaseJsonServletTest
  extends ScalatraSuite
  with FunSuiteLike
  with BeforeAndAfter
  with Injectable {


  implicit lazy val jsonFormats: Formats = DefaultFormats + new InstantSerializer + new UUIDSerializer
  implicit val bindingModule = DefaultBindings ~ TestBindings

  // val session = inject[Session]
  val testFixture = inject[TestFixture]

  ServletConfig.createAll.foreach(config =>
    addServlet(config.servlet, config.urlPattern)
  )

  def extractString(jValue : JValue, key : String): String= {
    compact(render(jValue \ key)).values.toString
  }

  def jValueToBytes(jValue: JValue): Array[Byte] = compact(render(jValue)).getBytes("UTF8")

  // convert jvalue to bytes and post it. not quite sure why this doesn't work
  // get Error:(14, 7) in class NotesServletTest, multiple overloaded alternatives of method post define default arguments.
  // The members with defaults are defined in class NotesServletTest in package servlet and trait Client in package test.
  // class NotesServletTest extends ScalatraSuite with FunSuiteLike with BeforeAndAfter {
  //  def post[A](uri : String, jValue : JValue, headers: Iterable[(String, String)] = Seq.empty)(f: => A): A =
  //    post(path, compact(render(jValue)).getBytes("UTF8"), headers) { f }

  //  def post[A](uri : String, jValue : JValue, headers: Iterable[(String, String)] = Seq.empty)(f: => A): A =
  //      submit("POST", uri, Seq.empty, headers, compact(render(jValue)).getBytes("UTF8")) { f }

  //  def post[A](uri: String, body: Array[Byte] = Array(), headers: Iterable[(String, String)] = Seq.empty)(f: => A): A =
  //    submit("POST", uri, Seq.empty, headers, body) { f }


  before {
    testFixture.resetCassandra
  }


}