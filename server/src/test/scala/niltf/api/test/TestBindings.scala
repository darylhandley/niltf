package niltf.api.test

import com.datastax.driver.core.{Session, Cluster}
import com.escalatesoft.subcut.inject.NewBindingModule
import com.typesafe.config.ConfigFactory
import niltf.api.repository.{UserRepo, NoteRepo}
import niltf.api.service.{SecurityService, NoteService}

// this defines which components are available for this module
object TestBindings extends NewBindingModule(module => {
  import module._   // can now use bind directly

  // bind our test objects
  bind [TestFixture] toModuleSingle { implicit module => new TestFixture}

})
