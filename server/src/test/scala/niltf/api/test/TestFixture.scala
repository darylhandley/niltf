package niltf.api.test

import java.time.Instant
import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger

import com.datastax.driver.core.Session
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.constants.{NoteLinkType, NiltfConstants}
import niltf.api.model.{NoteLink, Note, User}
import niltf.api.repository.{NoteLinkRepo, NoteRepo, UserRepo}
import niltf.api.service.SecurityService
import niltf.api.util.SecurityUtil

class TestFixture(implicit val bindingModule: BindingModule) extends Injectable {

  val session = inject[Session]
  val userRepo = inject[UserRepo]
  val noteRepo = inject[NoteRepo]
  val noteLinkRepo = inject[NoteLinkRepo]
  val securityService = inject[SecurityService]

  val noteSeq = new AtomicInteger(100)

  private val gilfoyle = User(
    UUIDSpoof.user(1),
    "gilfoyle@piedpiper.com",
    SecurityUtil.hashPassword("meatloaf1"),
    false,
    Instant.now
  )

  private val dinesh = User(
    UUIDSpoof.user(2),
    "dinesh@piedpiper.com",
    SecurityUtil.hashPassword("meatloaf1"),
    false,
    Instant.now
  )

  val testObjects = TestObjects(Users(gilfoyle, dinesh))

  def resetCassandra {
    session.execute("truncate table note")
    session.execute("truncate table note_link")
    session.execute("truncate table user")
    session.execute("truncate table user_by_email")
    session.execute("truncate table auth_token")

    userRepo.save(testObjects.users.gilfoyle)
    userRepo.save(testObjects.users.dinesh)

    // create note structure for our users
    createNoteStructureForUser(testObjects.users.gilfoyle, "Gilfoyle")
    createNoteStructureForUser(testObjects.users.dinesh, "Dinesh")

  }

  // creates a note structure for the user passed in. Uses the name to differntiate the bodys and titles between
  // different users
  //
  // example : initial structure for name=Gilfoyle will look like this
  //  - Gilfoyle Note 1, Gilfoyle Body 1
  //    - Gilfoyle Note 1_1, Gilfoyle Body 1_1
  //    - Gilfoyle Note 1_2, GilFoyle Body 1_2
  //  - Gilfoyle Note 2, Gilfoyle Body 2
  //    - Gilfoyle Note 2_1, Gilfoyle Body 2_2
  //    - Gilfoyle Note 2_2, Gilfoyle Body 2_2
  //    - Gilfoyle Note 2_3, Gilfoyle Body 2_3
  //      - Gilfoyle Note 2_3_1, Gilfoyle Body 2_3_1
  //        - Gilfoyle Note 2_3_1_1, Gilfoyle Body 2_3_1_1

  def createNoteStructureForUser(user : User, name : String): Unit = {

    val templateNote = Note(
      UUIDSpoof.note(1),
      user.id,
      UUIDSpoof.note(1),
      "",
      "",
      Instant.now(),
      List()
    )

    // create top level note
    val topLevelId = UUIDSpoof.note(noteSeq.getAndIncrement)
    var topLevel = templateNote.copy(
      parentId = topLevelId,
      id = topLevelId,
      title =  name + " top level",
      body = ""
    )

    var note1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = topLevel.id,
      title =  name + " title 1",
      body = name + " body 1"
    )
    topLevel = topLevel.appendChild(note1.id)

    val note1_1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note1.id,
      title =  name + " title 1_1",
      body = name + " body 1_1"
    )
    note1 = note1.appendChild(note1_1.id)

    val note1_2 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note1.id,
      title =  name + " title 1_2",
      body = name + " body 1_2"
    )
    note1 = note1.appendChild(note1_2.id)

    var note2 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = topLevel.id,
      title =  name + " title 2",
      body = name + " body 2"
    )
    topLevel = topLevel.appendChild(note2.id)

    val note2_1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2.id,
      title =  name + " title 2_1",
      body = name + " body 2_1"
    )
    note2 = note2.appendChild(note2_1.id)

    val note2_2 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2.id,
      title =  name + " title 2_2",
      body = name + " body 2_2"
    )
    note2 = note2.appendChild(note2_2.id)

    var note2_3 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2.id,
      title =  name + " title 2_3",
      body = name + " body 2_3"
    )
    note2 = note2.appendChild(note2_3.id)

    var note2_3_1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2_3.id,
      title =  name + " title 2_3_1",
      body = name + " body 2_3_1"
    )
    note2_3 = note2_3.appendChild(note2_3_1.id)

    val note2_3_1_1 = templateNote.copy(
      id = UUIDSpoof.note(noteSeq.getAndIncrement),
      parentId = note2_3_1.id,
      title =  name + " title 2_3_1_1",
      body = name + " body 2_3_1_1"
    )
    note2_3_1 = note2_3_1.appendChild(note2_3_1_1.id)

    // save them all
    val allNotes = List(topLevel, note1, note1_1, note1_2, note2, note2_1, note2_2, note2_3, note2_3_1, note2_3_1_1)
    // val allNotes = List(topLevel, note1, note1_1, note1_2, note2, note2_1, note2_2)
    allNotes.foreach(noteRepo.save(_))

    // set the top level noteLink
    val noteLink = new NoteLink(
      user.id,
      NoteLinkType.TOP_LEVEL_NOTE,
      topLevel.id
    )
    noteLinkRepo.save(noteLink)
  }


  def loginUser(user: User, password : String): String = {
    val tokenAndUserOpt = securityService.authenticate(user.email, password)
    tokenAndUserOpt.get._1
  }

  def loginUserAndCreateHeaders(user: User, password : String): Map[String, String]  = {
    val token = loginUser(user, TestConstants.TestUserPassword)
    Map(NiltfConstants.AuthTokenKey -> token)
  }

}
