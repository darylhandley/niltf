package niltf.api.test

import java.util.UUID

/*
 * Creates fake uuids for testing that are a UUID representation of the integer passed in.
 *
 * Example: UUIDSpoof.of(5) return "00000000-0000-0000-0000-000000000005"
 */
object UUIDSpoof {

  val UserOffset = 10000
  val NoteOffset = 20000

  def of(seed : Integer) : UUID = {
    require(seed > 0)
    UUID.fromString("00000000-0000-0000-0000-" + "%012d" format seed)
  }

  def of(offset : Integer, seed : Integer) : UUID = {
    require(offset > 0)
    of(offset + seed)
  }

  def user(seed : Integer) = of(UserOffset, seed)

  def note(seed : Integer) = of(NoteOffset, seed)

}
