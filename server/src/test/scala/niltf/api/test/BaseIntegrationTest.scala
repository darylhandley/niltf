package niltf.api.test

import com.escalatesoft.subcut.inject.Injectable
import niltf.api.app.{ServletConfig, DefaultBindings}
import niltf.api.jsonserializer.{UUIDSerializer, InstantSerializer}
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.scalatest.{Matchers, FunSuite, BeforeAndAfter, FunSuiteLike}
import org.scalatra.test.scalatest.ScalatraSuite

class BaseIntegrationTest extends FunSuite with BeforeAndAfter with Injectable with Matchers {


  implicit val bindingModule = DefaultBindings ~ TestBindings

  val testFixture = inject[TestFixture]

  before {
    testFixture.resetCassandra
  }


}
