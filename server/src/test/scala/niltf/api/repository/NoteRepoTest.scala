package niltf.api.repository

import java.time.Instant
import java.util.{Date, UUID}

import com.datastax.driver.core.Cluster
import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import niltf.api.app.DefaultBindings
import niltf.api.model.Note
import niltf.api.service.NoteService
import niltf.api.test.{UUIDSpoof, TestFixture, TestBindings}
import niltf.api.util.UuidUtil
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.scalatest._
import org.scalatra.test.scalatest._

class NoteRepoTest extends FunSuite with BeforeAndAfter with Injectable with Matchers {

  implicit val bindingModule: BindingModule = TestBindings ~ DefaultBindings

  val noteRepo = inject[NoteRepo]
  val testFixture = inject[TestFixture]

  test("Get should return None for non existent note") {
    noteRepo.get(UUID.randomUUID()) should be (None)
  }

  test("Save should create new and get should get") {

    // create a new note
    val note = Note(
      UUIDSpoof.note(1),
      testFixture.testObjects.users.gilfoyle.id,
      UUIDSpoof.note(2),
      "theTitle",
      "theBody",
      Instant.now,
      List(UUIDSpoof.note(3), UUIDSpoof.note(4))
    )
    noteRepo.save(note)


    // read it back and check it
    val savedNote = noteRepo.get(note.id)
    savedNote should not be (None)
    savedNote.get should equal(note)
  }

  test("Save should update ") {
    // create a new note
    val note = Note(
      UUID.randomUUID,
      testFixture.testObjects.users.gilfoyle.id,
      UUID.randomUUID,
      "theTitle",
      "theBody",
      Instant.now,
      List(UUIDSpoof.note(3), UUIDSpoof.note(4))
    )
    noteRepo.save(note)

    val updatedNote = note.copy(
      userId = testFixture.testObjects.users.dinesh.id,
      title = "new title",
      body = "new body",
      createdDate = note.createdDate.minusMillis(10000),
      childrenIds = List(UUIDSpoof.note(3), UUIDSpoof.note(4))
    )
    noteRepo.save(updatedNote)

    val savedNoteOpt = noteRepo.get(note.id)
    savedNoteOpt.get should equal(updatedNote)
  }

  test("Delete should delete.") {

    // create a new note
    val note = Note(
      UUID.randomUUID,
      testFixture.testObjects.users.gilfoyle.id,
      UUID.randomUUID,
      "theTitle",
      "theBody",
      Instant.now,
      List()
    )
    noteRepo.save(note)

    // delete it
    noteRepo.delete(note.id)

    // read it back and check it
    val savedNote = noteRepo.get(note.id)
    savedNote should be (None)
  }

  before {
    testFixture.resetCassandra
  }

}