package niltf.api.repository

import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.app.DefaultBindings
import niltf.api.model.{AuthToken, User}
import niltf.api.restexception.E409Conflict
import niltf.api.test.{TestBindings, TestFixture}
import org.scalatest._

class AuthTokenRepoTest extends FunSuite with BeforeAndAfter with Injectable with MustMatchers {

  implicit val bindingModule: BindingModule = DefaultBindings ~ TestBindings

  val authTokenRepo = inject[AuthTokenRepo]
  val testFixture  = inject[TestFixture]

  test("Get should return None for non existent token") {
    authTokenRepo.get("Unknown") must be (None)
  }

  test("Save should create new and get should retrieve it") {
    val token = createDefaultToken()

    // read it back and check it
    val savedToken = authTokenRepo.get(token.id)
    savedToken must not be (None)
    savedToken.get must equal(token)
  }

  test("Save should update") {
    // create a new authToken
    val authToken = createDefaultToken()

    // update it
    val updatedAuthToken = authToken.copy(
      userId = UUID.randomUUID
    )
    authTokenRepo.save(updatedAuthToken)

    // read it back and check it
    val savedAuthToken = authTokenRepo.get(authToken.id)
    savedAuthToken must not be (None)
    savedAuthToken.get must equal(updatedAuthToken)
  }


  test("Delete should delete authToken") {

    val authToken = createDefaultToken()
    authTokenRepo.save(authToken)

    authTokenRepo.delete(authToken.id)
    authTokenRepo.get(authToken.id) must be (None)
  }

  // as long as no errors thrown we are good
  test("Delete should gracefully delete non existent token") {
    authTokenRepo.delete("noneExistentToken")
  }


  def createDefaultToken(): AuthToken = {
    val token = AuthToken(
      "MyToken",
      UUID.fromString("00000000-0000-0000-0000-000000000002"),
      Instant.now
    )
    authTokenRepo.save(token)
  }

  before {
    testFixture.resetCassandra
  }

}