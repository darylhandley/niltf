package niltf.api.repository

import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.app.DefaultBindings
import niltf.api.constants.NoteLinkType
import niltf.api.model.{NoteLink, Note}
import niltf.api.test.{BaseIntegrationTest, TestBindings, TestFixture, UUIDSpoof}
import org.scalatest._

class NoteLinkRepoTest extends BaseIntegrationTest {

  val noteLinkRepo = inject[NoteLinkRepo]

  test("Get should return None for non existent note") {
    noteLinkRepo.get(UUID.randomUUID(), NoteLinkType.TOP_LEVEL_NOTE) should be (None)
  }

  test("Save should create new and get should get") {
    // create a new note link
    val noteLink = NoteLink(
      UUIDSpoof.user(1),
      NoteLinkType.TOP_LEVEL_NOTE,
      UUIDSpoof.note(1)
    )
    noteLinkRepo.save(noteLink)

    // read it back and check it
    val savedNoteLink = noteLinkRepo.get(noteLink.userId, NoteLinkType.TOP_LEVEL_NOTE)
    savedNoteLink should not be (None)
    savedNoteLink.get should equal(noteLink)
  }

  test("Save should update ") {
    // create a new note link
    val noteLink = NoteLink(
      UUIDSpoof.user(1),
      NoteLinkType.TOP_LEVEL_NOTE,
      UUIDSpoof.note(1)
    )
    noteLinkRepo.save(noteLink)

    // update it
    val updatedNoteLink = noteLink.copy(noteId = UUIDSpoof.note(2))
    noteLinkRepo.save(updatedNoteLink)
    val savedNoteLink = noteLinkRepo.get(noteLink.userId, NoteLinkType.TOP_LEVEL_NOTE)
    savedNoteLink should not be (None)
    savedNoteLink.get should equal(updatedNoteLink)

  }

  test("Delete should delete.") {
    // create a new note link
    val noteLink = NoteLink(
      UUIDSpoof.user(1),
      NoteLinkType.TOP_LEVEL_NOTE,
      UUIDSpoof.note(1)
    )
    noteLinkRepo.save(noteLink)

    // delete it
    noteLinkRepo.delete(noteLink.userId, noteLink.noteLinkType)

    // read it back and check it
    val savedNote = noteLinkRepo.get(noteLink.userId, noteLink.noteLinkType)
    savedNote should be (None)
  }

  test("Delete should not throw an error if it does not exist.") {
    noteLinkRepo.delete(UUIDSpoof.user(1), NoteLinkType.TOP_LEVEL_NOTE)
  }



}