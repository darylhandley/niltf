package niltf.api.repository

import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.app.DefaultBindings
import niltf.api.model.{User, Note}
import niltf.api.restexception.E409Conflict
import niltf.api.test.{TestFixture, TestBindings}
import org.scalatest._

class UserRepoTest extends FunSuite with BeforeAndAfter with Injectable with MustMatchers {

  implicit val bindingModule: BindingModule = DefaultBindings ~ TestBindings

  val userRepo = inject[UserRepo]
  val testFixture  = inject[TestFixture]

  test("Get should return None for non existent user") {
    assert(userRepo.get(UUID.randomUUID()).isEmpty)
  }

  test("Save should create new and get should retrieve it") {
    val user = createDefaultUser()

    // read it back and check it
    val savedUser = userRepo.get(user.id)
    savedUser must not be (None)
    savedUser.get must equal(user)
  }

  test("Save should update") {
    // create a new user
    val user = createDefaultUser()

    // update it
    val updatedUser = user.copy(
      email = "email1",
      password = "password1",
      isEmailValidated = true,
      createdInstant = Instant.now.plusMillis(1000)
    )
    userRepo.save(updatedUser)

    // read it back and check it
    val savedUser = userRepo.get(user.id)
    savedUser must not be (None)
    savedUser.get must equal(updatedUser)
    savedUser.get must not equal(user)
  }

  test("Save should throw error on duplicate email") {

    // create a new user
    val user = createDefaultUser()

    // create another user with a different uuid and same email
    val thrown = intercept[E409Conflict] {
      val user2 = user.copy(id = UUID.randomUUID)
      userRepo.save(user2)
    }
    thrown.getMessage must equal("User with email already exists")

  }

  test("getByEmail should work after save new") {
    // create a new user
    val user = createDefaultUser()

    // read back by email
    val savedUser = userRepo.getByEmail(user.email)
    savedUser must not be (None)
    savedUser.get must equal(user)
  }

  test("getByEmail should work after save update") {
    // create a new user
    val user = createDefaultUser()

    // update the email
    val updatedUser = user.copy(email = "darylhandley73@yahoo.com")
    userRepo.save(updatedUser)
    val savedUser = userRepo.getByEmail(updatedUser.email)
    savedUser must not be (None)
    savedUser.get must equal(updatedUser)

    // check the old email should no longer exist
    val oldEmailUser = userRepo.getByEmail(user.email)
    oldEmailUser must be (None)

  }

  test("getByEmail should return None when not found ") {
    userRepo.getByEmail("notAValidEmail")  must be (None)
  }

  test("Delete should delete user") {

    val user = createDefaultUser()
    userRepo.save(user)

    userRepo.delete(user.id)
    userRepo.get(user.id) must be (None)
  }

  // as long as no errors thrown we are good
  test("Delete should gracefully delete non existent user") {
    userRepo.delete(UUID.randomUUID())
  }

  def createDefaultUser(): User = {
    val user = User(
      UUID.randomUUID,
      "email",
      "password",
      false,
      Instant.now
    )
    userRepo.save(user)
  }

  before {
    testFixture.resetCassandra
  }

}