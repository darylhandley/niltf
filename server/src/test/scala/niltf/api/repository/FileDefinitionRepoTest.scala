package niltf.api.repository

import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.app.DefaultBindings
import niltf.api.model.{FileDefinition, Note}
import niltf.api.test.{TestBindings, TestFixture, UUIDSpoof}
import org.scalatest._

class FileDefinitionRepoTest extends FunSuite with BeforeAndAfter with Injectable with Matchers {

  implicit val bindingModule: BindingModule = TestBindings ~ DefaultBindings

  val fileDefRepo = inject[FileDefinitionRepo]
  val testFixture = inject[TestFixture]

  test("Get should return None for non existent file definition") {
    fileDefRepo.get(UUID.randomUUID()) should be (None)
  }

  test("Save should create new and get should get") {

    // create a new note
    val fileDef = FileDefinition(
      UUIDSpoof.of(1),
      "MyFilename.tst",
      UUIDSpoof.note(2),
      Instant.now
    )

    fileDefRepo.save(fileDef)

    // read it back and check it
    val savedFileDef = fileDefRepo.get(fileDef.id)
    savedFileDef should not be (None)
    savedFileDef.get should equal(fileDef)
  }

  test("Save should update ") {
    // create a new fileDef
    val fileDef = FileDefinition(
      UUIDSpoof.of(1),
      "MyFilename.tst",
      UUIDSpoof.note(2),
      Instant.now
    )
    fileDefRepo.save(fileDef)

    val updatedFileDef = fileDef.copy(
      filename = "newFilename.txt",
      noteId = UUIDSpoof.note(3),
      createdDate = fileDef.createdDate.minusMillis(10000)
    )
    fileDefRepo.save(updatedFileDef)

    val savedFileDef = fileDefRepo.get(fileDef.id)
    savedFileDef.get should equal(updatedFileDef)
  }

  test("Delete should delete.") {

    // create a new fileDef
    val fileDef = FileDefinition(
      UUIDSpoof.of(1),
      "MyFilename.tst",
      UUIDSpoof.note(2),
      Instant.now
    )
    fileDefRepo.save(fileDef)

    // make sure exists
    fileDefRepo.get(fileDef.id) should not be (None)

    // delete it
    fileDefRepo.delete(fileDef.id)

    // read it back and check it
    val savedFileDef = fileDefRepo.get(fileDef.id)
    savedFileDef should be (None)
  }

  before {
    testFixture.resetCassandra
  }

}