package niltf.api.util

import java.time.{ZoneId, ZonedDateTime}
import java.util.Date

import org.scalatest.{FunSuite, Matchers}

class FileExtensionUtilTest extends FunSuite with Matchers {

  val utc  = ZoneId.of("UTC")
  val pst  = ZoneId.of("Canada/Pacific")


  test("filenameToContentType should work for filenames with known extensions") {
    FileExtensionUtil.filepathToContentType("/home/blah/myfile.gif") should be (Some("image/gif"))
    FileExtensionUtil.filepathToContentType("/home/blah/myfile.png") should be (Some("image/png"))
    FileExtensionUtil.filepathToContentType("/home/blah/myfile.jpg") should be (Some("image/jpg"))
    FileExtensionUtil.filepathToContentType("/home/blah/myfile.jpeg") should be (Some("image/jpeg"))
  }

  test("filenameToContentType should work for filepaths with known extensions") {
    FileExtensionUtil.filepathToContentType("myfile.gif") should be (Some("image/gif"))
    FileExtensionUtil.filepathToContentType("myfile.png") should be (Some("image/png"))
    FileExtensionUtil.filepathToContentType("myfile.jpg") should be (Some("image/jpg"))
    FileExtensionUtil.filepathToContentType("myfile.jpeg") should be (Some("image/jpeg"))
  }

  test("filenameToContentType should return None for filenames with unknown extensions") {
    FileExtensionUtil.filepathToContentType("myfile.sss") should be (None)
    FileExtensionUtil.filepathToContentType("myfile.txt") should be (None)
    FileExtensionUtil.filepathToContentType("myfile.pdf") should be (None)
    FileExtensionUtil.filepathToContentType("myfile.html") should be (None)
    FileExtensionUtil.filepathToContentType("/myfile") should be (None)
    FileExtensionUtil.filepathToContentType("") should be (None)
  }

  test("filenameToContentType should return None for filepaths with unknown extensions") {
    FileExtensionUtil.filepathToContentType("/home/blah/myfile.sss") should be (None)
    FileExtensionUtil.filepathToContentType("/home/blah/myfile.txt") should be (None)
    FileExtensionUtil.filepathToContentType("/home/blah/myfile.pdf") should be (None)
    FileExtensionUtil.filepathToContentType("/home/blah/myfile.html") should be (None)
    FileExtensionUtil.filepathToContentType("/home/blah/myfile") should be (None)
  }

  test("filenameToContentType should work for filenames with known extensions and multiple perionds") {
    FileExtensionUtil.filepathToContentType("blah.myfile.gif") should be (Some("image/gif"))
    FileExtensionUtil.filepathToContentType("blah.blah.myfile.png") should be (Some("image/png"))
    FileExtensionUtil.filepathToContentType("blah.blah.blah.myfile.jpg") should be (Some("image/jpg"))
  }

  test("filenameToContentType should work for filenames with unknown extensions and multiple periods") {
    FileExtensionUtil.filepathToContentType("blah.myfile.ttt") should be (None)
    FileExtensionUtil.filepathToContentType("blah.blah.myfile.ttt") should be (None)
    FileExtensionUtil.filepathToContentType("blah.blah.blah.myfile.ttt") should be (None)
  }


}
