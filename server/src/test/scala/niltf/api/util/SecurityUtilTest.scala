package niltf.api.util

import org.scalatest.{MustMatchers, FunSuite}

class SecurityUtilTest extends FunSuite with MustMatchers {

  test("should encrypt and vaildate password") {
    val encrypted = SecurityUtil.hashPassword("MyPass")
    SecurityUtil.validatePassword("MyPass", encrypted) must be (true)
  }

  test("encrypt should return false on invalid password") {
    val encrypted = SecurityUtil.hashPassword("MyPass")
    SecurityUtil.validatePassword("MyPass1", encrypted) must be (false)
  }



}
