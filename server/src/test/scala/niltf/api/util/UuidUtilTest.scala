package niltf.api.util

import java.util.UUID

import org.scalatest.FunSuite

class UuidUtilTest extends FunSuite {

  test("isValid returns true on valid UUID") {

    assert(UuidUtil.isValid("ba39b0d8-c18d-4118-932b-4e8b1139fc23"))
    assert(UuidUtil.isValid("0ebca3c2-edd9-49d0-b743-84c2fd41459d"))
    assert(UuidUtil.isValid("cbe081f2-574f-4fd2-8e31-67dd747b004f"))
    assert(UuidUtil.isValid("7f59dd27-02e9-4c84-a5bf-c99b587681dc"))
    assert(UuidUtil.isValid("ddfd8734-a3fc-490e-8dc0-33c5d680e551"))
    assert(UuidUtil.isValid("5b640f9a-ec5e-4455-8192-85caf27f9cda"))
    assert(UuidUtil.isValid("ddfd8734-a3fc-490e-8dc0-33c5d680e551".toUpperCase()))
  }

  test("isValid returns false on invalid UUID") {
    assert(!UuidUtil.isValid("daryl"))
    assert(!UuidUtil.isValid(""))

    val validUuid = "ddfd8734-a3fc-490e-8dc0-33c5d680e551"
    assert(!UuidUtil.isValid(validUuid + " "))
    assert(!UuidUtil.isValid(validUuid + "5"))
    assert(!UuidUtil.isValid("5" + validUuid))
    assert(!UuidUtil.isValid("5" + validUuid + "5"))
    assert(!UuidUtil.isValid(" " + validUuid + " "))
    assert(!UuidUtil.isValid("5" + validUuid.replace('d', 'x')))
  }

}
