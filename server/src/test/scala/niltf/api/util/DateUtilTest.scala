package niltf.api.util

import java.time.{ZoneId, ZonedDateTime, Instant}
import java.util.Date
import org.scalatest.{Matchers, MustMatchers, FunSuite}

class DateUtilTest extends FunSuite with Matchers {

  val utc  = ZoneId.of("UTC")
  val pst  = ZoneId.of("Canada/Pacific")


  test("toISO8601(Instant) returns valid ISO8601 string") {
    val utcDateTime = ZonedDateTime.of(2016, 1, 5, 10, 36, 27, 123000000, utc)
    DateUtil.toIso8601(utcDateTime.toInstant) should be ("2016-01-05T10:36:27Z")
  }

  test("toISO8601(Date) returns valid ISO8601 string") {
    val utcDateTime = ZonedDateTime.of(2016, 1, 5, 10, 36, 27, 123000000, utc)
    DateUtil.toIso8601(Date.from(utcDateTime.toInstant)) should be ("2016-01-05T10:36:27Z")
  }

  test("toISO8601(Instant) vancouver timezone non DST returns valid ISO8601 string") {
    val utcDateTime = ZonedDateTime.of(2016, 1, 5, 10, 36, 27, 123000000, pst)
    DateUtil.toIso8601(utcDateTime.toInstant) should be ("2016-01-05T18:36:27Z")
  }

  test("toISO8601(Instant) vancouver timezone DST returns valid ISO8601 string") {
    val utcDateTime = ZonedDateTime.of(2016, 8, 5, 10, 36, 27, 123000000, pst)
    DateUtil.toIso8601(utcDateTime.toInstant) should be ("2016-08-05T17:36:27Z")
  }

  test("fromIso8601 should return correct instant") {
    val isoString = "2016-01-05T17:36:27Z"
    val utcDateTime = ZonedDateTime.of(2016, 1, 5, 17, 36, 27, 0, utc).toInstant
    DateUtil.fromIso8601(isoString) should be (utcDateTime)
  }

}
