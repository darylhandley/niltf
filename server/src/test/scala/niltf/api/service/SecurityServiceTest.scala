package niltf.api.service

import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.app.DefaultBindings
import niltf.api.model.{User, Note}
import niltf.api.repository.{UserRepo, NoteRepo}
import niltf.api.test.{TestFixture, TestBindings}
import niltf.api.util.{SecurityUtil, UuidUtil}
import org.scalatest.{BeforeAndAfter, MustMatchers, FunSuite}

import scala.Some

class SecurityServiceTest extends FunSuite with BeforeAndAfter with Injectable with MustMatchers {
  implicit val bindingModule: BindingModule = DefaultBindings ~ TestBindings

  val securityService = inject[SecurityService]
  val userRepo = inject[UserRepo]
  val testFixture = inject[TestFixture]

  test("authenticate should return user and token for valid creds") {

    val defaultUser = createDefaultUser

    val tokenAndUserOpt = securityService.authenticate("darylhandley72@yahoo.com", "meatloaf1")
    tokenAndUserOpt must not be None

    val (token, user) = tokenAndUserOpt.get
    UuidUtil.isValid(token) must be (true)
    user must be (defaultUser)
  }

  test("authenticate should return None for invalid creds") {
    val defaultUser = createDefaultUser

    val tokenAndUserOpt = securityService.authenticate("darylhandley72@yahoo.com", "meatloaf2")
    tokenAndUserOpt must be (None)
  }

  test("getUserByAuthToken should return User ") {

    val defaultUser = createDefaultUser

    // authenticate user to add to map
    val tokenAndUserOpt = securityService.authenticate("darylhandley72@yahoo.com", "meatloaf1")
    tokenAndUserOpt must not be None
    val (token, user) = tokenAndUserOpt.get

    val userFromAuthOpt = securityService.getUserByAuthToken(token)
    userFromAuthOpt must not be None
    userFromAuthOpt.get must be (user)
  }

  test("getUserByAuthToken should return None when not found  ") {
    val defaultUser = createDefaultUser

    val userFromAuthOpt = securityService.getUserByAuthToken(UUID.randomUUID.toString)
    userFromAuthOpt must be (None)
  }

  test("removeAuthToken should be no op when token not known") {
    val defaultUser = createDefaultUser

    // as long as no exception we are good
    securityService.removeAuthToken(UUID.randomUUID.toString)
  }

  test("removeAuthToken should remove token when token is known") {
    val defaultUser = createDefaultUser

    // authenticate user to add to map
    val tokenAndUserOpt = securityService.authenticate("darylhandley72@yahoo.com", "meatloaf1")
    tokenAndUserOpt must not be None
    val (token, user) = tokenAndUserOpt.get

    securityService.removeAuthToken(token)
    val userFromAuthOpt = securityService.getUserByAuthToken(token)
    userFromAuthOpt must be (None)
  }

  def createDefaultUser(): User = {

    val hashedPass = SecurityUtil.hashPassword("meatloaf1")
    val user = new User(
      id = UUID.randomUUID,
      email = "darylhandley72@yahoo.com",
      password = hashedPass,
      createdInstant = Instant.now
    )
    userRepo.save(user)

  }

  before {
    testFixture.resetCassandra
  }


}
