package niltf.api.service

import java.time.Instant
import java.util.UUID

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import niltf.api.app.DefaultBindings
import niltf.api.constants.NoteLinkType
import niltf.api.model.{Note, NoteLink, User}
import niltf.api.repository.{NoteLinkRepo, NoteLinkRepoTest, UserRepo}
import niltf.api.test.{BaseIntegrationTest, TestBindings, TestFixture, UUIDSpoof}
import niltf.api.util.{SecurityUtil, UuidUtil}
import org.scalatest.{BeforeAndAfter, FunSuite, MustMatchers}

class NoteServiceTest extends BaseIntegrationTest {

  val noteService = inject[NoteService]
  val noteLinkRepo = inject[NoteLinkRepo]

  test("getTopLevelNote should return topLevelNote") {
    val note = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)
    note should not be (None)
    note.title should be ("Gilfoyle top level")
  }

  test("getTopLevelNote should throw IllegalArgumentException when no topLevelNote exists") {
    noteLinkRepo.delete(testFixture.testObjects.users.gilfoyle.id, NoteLinkType.TOP_LEVEL_NOTE)
    intercept[IllegalStateException] {
      noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)
    }
  }

  test("getByNoteIdAndPath should return note with passed in uuid when Seq is empty") {
    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)
    val note = noteService.getByNoteIdAndPath(topLevelNote.id, Seq())
    note.get.id should be (topLevelNote.id)
    note.get.title should be ("Gilfoyle top level")
  }

  test("getByNoteIdAndPath should return None when uuid unknown") {
    val note = noteService.getByNoteIdAndPath(UUIDSpoof.note(999), Seq(1,1))
    note should be (None)
  }

  test("getByNoteIdAndPath should return correct note when sequence exists for one level") {
    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)
    val note = noteService.getByNoteIdAndPath(topLevelNote.id, Seq(2))
    note.get.title should be ("Gilfoyle title 2")
  }

  test("getByNoteIdAndPath should return correct note when sequence exists for two levels") {
    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)
    val note = noteService.getByNoteIdAndPath(topLevelNote.id, Seq(2,2))
    note.get.title should be ("Gilfoyle title 2_2")
  }

  test("getByNoteIdAndPath should return correct note when sequence is too deep") {
    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)
    val note = noteService.getByNoteIdAndPath(topLevelNote.id, Seq(1,1,1))
    note should be (None)
  }

  test("getByNoteIdAndPath should return None when sequence not found") {
    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)
    val note = noteService.getByNoteIdAndPath(topLevelNote.id, Seq(999,999))
    note should be (None)
  }

  test("getByUserIdAndPath should return top level note when when Seq is empty") {
    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)
    val note = noteService.getByUserIdAndPath(testFixture.testObjects.users.gilfoyle.id, Seq())
    note.get.id should be (topLevelNote.id)
    note.get.title should be ("Gilfoyle top level")
  }

  test("getByUserIdAndPath should throw IllegalStateException when user id unknown") {
    intercept[IllegalStateException] {
      noteService.getByUserIdAndPath(UUIDSpoof.user(999), Seq(1))
    }
  }

  test("getByUserIdAndPath should return correct note when sequence exists for one level") {
    val note = noteService.getByUserIdAndPath(testFixture.testObjects.users.gilfoyle.id,  Seq(2))
    note.get.title should be ("Gilfoyle title 2")
  }

  test("getByUserIdAndPath should return correct note when sequence exists for two levels") {
    val note = noteService.getByUserIdAndPath(testFixture.testObjects.users.gilfoyle.id, Seq(2,2))
    note.get.title should be ("Gilfoyle title 2_2")
  }

  test("getByUserIdAndPath should return correct note when sequence is too deep") {
    val note = noteService.getByUserIdAndPath(testFixture.testObjects.users.gilfoyle.id, Seq(1,1,1))
    note should be (None)
  }

  test("getByUserIdAndPath should return None when sequence not found") {
    val note = noteService.getByUserIdAndPath(testFixture.testObjects.users.gilfoyle.id, Seq(999,999))
    note should be (None)
  }


}
