package niltf.api.servlet

import java.time.Instant
import java.time.temporal.{ChronoUnit, TemporalUnit, Temporal}
import java.util.{Date, UUID}

import niltf.api.jsonserializer.{UUIDSerializer, InstantSerializer}
import niltf.api.model.{AuthToken, User, Note}
import niltf.api.repository.{AuthTokenRepo, UserRepo, NoteRepo}
import niltf.api.service.{SecurityService, NoteService}
import niltf.api.test.BaseJsonServletTest
import niltf.api.util
import niltf.api.util.{SecurityUtil, DateUtil, UuidUtil}
import org.joda.time.Seconds
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import scala.None
import org.scalatest.time.Millis

class AuthServletTest extends BaseJsonServletTest {

  val userRepo = inject[UserRepo]
  val authTokenRepo = inject[AuthTokenRepo]
  val securityService = inject[SecurityService]



  test("POST auth/token should authenticate user with valid username and password ") {

    val user = createDefaultUser()

    val json : JValue =
        ("email" -> user.email) ~
        ("password" -> "meatloaf1")

    // post it
    post("/auth/token", jValueToBytes(json)) {
      status should equal (200)

      val parsedBody = parse(body)
      val userJson = (parsedBody \ "user")

      (userJson \ "email").extract[String] should equal (user.email)
      (userJson \ "id").extract[String] should equal (user.id.toString)
      (userJson \ "isEmailValidated").extract[Boolean] should equal (false)

      val createdInstantStr = (userJson \ "createdInstant").extract[String]
      createdInstantStr should equal (util.DateUtil.toIso8601(user.createdInstant))

      val createdInstant = (userJson \ "createdInstant").extract[Instant]
      createdInstant should equal (user.createdInstant.truncatedTo(ChronoUnit.SECONDS))

      // just check it's a valid uuid,
      val token = (parsedBody \ "authToken").extract[String]
      val uuid = UUID.fromString(token)

      // check auth token is in the db
      val authTokenOpt = authTokenRepo.get(token)
      authTokenOpt should not be None
      authTokenOpt.get.userId should be (user.id)
      assert(authTokenOpt.get.createdInstant.plusMillis(1000).isAfter(Instant.now))

    }
  }

  test("POST auth/token should return 401 when password invalid") {

    val user = createDefaultUser()

    val json : JValue =
      ("email" -> user.email) ~
        ("password" -> "meatloaf2")

    post("/auth/token", jValueToBytes(json)) {
      status should equal (401)
      val parsedBody = parse(body)
      (parsedBody \ "status").extract[Integer] should equal (401)
      (parsedBody \ "reason").extract[String] should equal ("Unauthorized")
      (parsedBody \ "message").extract[String] should equal ("Invalid email or password")

    }

  }

  test("GET should return user when valid token passed") {

    val user = createDefaultUser()

    val tokenAndUserOpt = securityService.authenticate(user.email, "meatloaf1")
    val token = tokenAndUserOpt.get._1

    get("/auth/token/" + token) {
      status should equal (200)
      val parsedBody = parse(body)
      (parsedBody \ "id").extract[UUID] should equal (user.id)
      (parsedBody \ "email").extract[String] should equal (user.email)
      (parsedBody \ "isEmailValidated").extract[Boolean] should equal (false)
      (parsedBody \ "createdInstant").extract[Instant] should equal (DateUtil.removeMillis(user.createdInstant))
    }
  }

  test("GET should return 404 when unknown token passed") {

    val user = createDefaultUser()

    val token = UUID.randomUUID

    get("/auth/token/" + token) {
      status should equal (404)
      val parsedBody = parse(body)
      (parsedBody \ "status").extract[Integer] should equal (404)
      (parsedBody \ "reason").extract[String] should equal ("Not Found")
      (parsedBody \ "message").extract[String] should equal (s"AuthToken $token not found")
    }
  }

  test("DELETE should remove token when known token passed") {

    val user = createDefaultUser()

    val tokenAndUserOpt = securityService.authenticate(user.email, "meatloaf1")
    val token = tokenAndUserOpt.get._1

    securityService.getUserByAuthToken(token) should not be (None)

    delete("/auth/token/" + token) {
      status should equal (200)
      body should equal ("")
    }

    // make sure it's gone
    securityService.getUserByAuthToken(token) should be (None)
  }

  test("DELETE should be no op when unknown token passed") {

    val token = UUID.randomUUID

    delete("/auth/token/" + token) {
      status should equal (200)
      body should equal ("")
    }

  }


  def createDefaultUser(): User = {
    val user = User(
      UUID.randomUUID,
      "daryhandley72@yahoo.com",
      SecurityUtil.hashPassword("meatloaf1"),
      false,
      Instant.now
    )
    userRepo.save(user)
  }





}