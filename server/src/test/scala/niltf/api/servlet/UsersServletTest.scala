package niltf.api.servlet

import java.time.Instant
import java.util.{Date, UUID}

import niltf.api.constants.Messages
import niltf.api.model.Note
import niltf.api.repository.{NoteLinkRepo, NoteRepo, UserRepo}
import niltf.api.restexception.E400BadRequest
import niltf.api.service.NoteService
import niltf.api.test.{BaseJsonServletTest, TestConstants, UUIDSpoof}
import niltf.api.util.{DateUtil, SecurityUtil, UuidUtil}
import org.apache.http.HttpStatus
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._

class UsersServletTest extends BaseJsonServletTest {

  private val userRepo = inject[UserRepo]
  private val noteService = inject[NoteService]

  def loginGilfoyleAndCreateHeaders = testFixture.loginUserAndCreateHeaders(
    testFixture.testObjects.users.gilfoyle, TestConstants.TestUserPassword)

  test("GET /me should return user with no password") {
    val headers = loginGilfoyleAndCreateHeaders

    get("/users/me", Seq.empty, headers) {
      status should equal (HttpStatus.SC_OK)

      val parsedBody = parse(body)
      val expectedCreatedInstant = DateUtil.removeMillis(testFixture.testObjects.users.gilfoyle.createdInstant)
      (parsedBody \ "email").extract[String] should equal("gilfoyle@piedpiper.com")
      (parsedBody \ "id").extract[String] should equal(testFixture.testObjects.users.gilfoyle.id.toString)
      (parsedBody \ "createdInstant").extract[Instant] should equal(expectedCreatedInstant)
      (parsedBody \ "password") should equal (JNothing)

    }
  }

  test("GET /me should return unauthorized when not logged in") {
    get("/users/me") {
      status should equal (HttpStatus.SC_UNAUTHORIZED)
      val parsedBody = parse(body)
    }
  }

  test("POST ehrlich should create new user ehrlich") {
    val json : JValue =
      ("email" -> "ehrlich@piedpiper.com") ~
      ("password" -> "thePassword")

    post("/users", jValueToBytes(json)) {
      status should equal (HttpStatus.SC_OK)

      val parsedBody = parse(body)
      val id = (parsedBody \ "id").extract[UUID]
      val createdInstant = (parsedBody \ "createdInstant").extract[Instant]
      (parsedBody \ "email").extract[String] should be("ehrlich@piedpiper.com")

      assert(createdInstant.plusMillis(1000).isAfter(Instant.now))

      // check the user in db
      val ehrlich = userRepo.get(id).get
      ehrlich.email should be ("ehrlich@piedpiper.com")
      DateUtil.removeMillis(ehrlich.createdInstant) should be (createdInstant)
      SecurityUtil.validatePassword("thePassword", ehrlich.password) should be(true)

      // check the top level note created
      val topLevelNote = noteService.getTopLevelNote(ehrlich.id)
      topLevelNote.userId should be (ehrlich.id)
      topLevelNote.childrenIds should be (List())
      topLevelNote.body should be ("")
      topLevelNote.title should be ("Top Level Note")
      topLevelNote.parentId should be (topLevelNote.id)
      assert(topLevelNote.createdDate.plusMillis(1000).isAfter(Instant.now))

    }
  }


  test("POST with existing email should return Conflict") {
    val gilfoyle = testFixture.testObjects.users.gilfoyle
    val json : JValue =
      ("email" -> gilfoyle.email) ~
        ("password" -> "thePassword")

    post("/users", jValueToBytes(json)) {
      status should equal (HttpStatus.SC_CONFLICT)

      val parsedBody = parse(body)
      val errorResponse = (parsedBody).extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e409Conflict(Messages.EmailExists))

    }
  }

  test("POST with missing email should return BadRequest") {
    val json : JValue =
        ("password" -> "thePassword")

    post("/users", jValueToBytes(json)) {
      status should equal (HttpStatus.SC_BAD_REQUEST)

      val parsedBody = parse(body)
      val errorResponse = (parsedBody).extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e400BadRequest(Messages.EmailIsARequiredField))

    }
  }

  test("POST with blank email should return BadRequest") {
    val json : JValue =
      ("password" -> "thePassword") ~
        ("email" -> "")


    post("/users", jValueToBytes(json)) {
      status should equal (HttpStatus.SC_BAD_REQUEST)

      val parsedBody = parse(body)
      val errorResponse = (parsedBody).extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e400BadRequest(Messages.EmailIsARequiredField))

    }
  }

  test("POST with missing password should return BadRequest") {
    val json : JValue =
      ("email" -> "ehrlich@piedpiper.com")

    post("/users", jValueToBytes(json)) {
      status should equal (HttpStatus.SC_BAD_REQUEST)

      val parsedBody = parse(body)
      val errorResponse = (parsedBody).extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e400BadRequest(Messages.PasswordIsARequiredField))
    }
  }

  test("POST with blank password should return BadRequest") {
    val json : JValue =
      ("email" -> "ehrlich@piedpiper.com") ~
        ("password" -> "")

    post("/users", jValueToBytes(json)) {
      status should equal (HttpStatus.SC_BAD_REQUEST)

      val parsedBody = parse(body)
      val errorResponse = (parsedBody).extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e400BadRequest(Messages.PasswordIsARequiredField))
    }
  }

  test("POST with missing email  should return BadRequest") {
    val json : JValue =
      ("password" -> "12345678")

    post("/users", jValueToBytes(json)) {
      status should equal (HttpStatus.SC_BAD_REQUEST)

      val parsedBody = parse(body)
      val errorResponse = (parsedBody).extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e400BadRequest(Messages.EmailIsARequiredField))

    }
  }

  test("POST with invalid password should return bad request") {
    val json : JValue =
      ("email" -> "ehrlich@piedpiper.com") ~
        ("password" -> "123")

    post("/users", jValueToBytes(json)) {
      status should equal (HttpStatus.SC_BAD_REQUEST)
      val parsedBody = parse(body)
      val errorResponse = (parsedBody).extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e400BadRequest(Messages.PasswordNotValid))
    }
  }

  // POST for logged in user should fail ?


}