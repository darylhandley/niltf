package niltf.api.servlet

import java.time.{Instant, ZonedDateTime}
import java.util.{Date, UUID}

import niltf.api.constants.{NiltfConstants, NoteLinkType}
import niltf.api.model.Note
import niltf.api.repository.{NoteLinkRepo, NoteRepo}
import niltf.api.restexception.E400BadRequest
import niltf.api.service.NoteService
import niltf.api.servlet
import niltf.api.test.{BaseJsonServletTest, TestConstants, TestObjects, UUIDSpoof}
import niltf.api.util.{DateUtil, UuidUtil}
import org.apache.http.HttpStatus
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.JsonDSL._
import org.scalatra.{HttpMethod, NoContent}

class NotesServletTest extends BaseJsonServletTest {

  private val noteRepo = inject[NoteRepo]
  private val noteService = inject[NoteService]
  private val noteLinkRepo = inject[NoteLinkRepo]

  def loginGilfoyleAndCreateHeaders = testFixture.loginUserAndCreateHeaders(
    testFixture.testObjects.users.gilfoyle, TestConstants.TestUserPassword)

  test("GET all should return top level children") {
    val headers = loginGilfoyleAndCreateHeaders

    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)

    get("/notes", headers = headers) {
      status should equal (200)
      val parsedBody = parse(body)
      parsedBody.children.size should equal (2)

      val child1 : Note = parsedBody.children(0).extract[Note]
      child1.title should equal("Gilfoyle title 1")
      child1.body should equal("Gilfoyle body 1")

      val child2 : Note = parsedBody.children(1).extract[Note]
      child2.title should equal("Gilfoyle title 2")
      child2.body should equal("Gilfoyle body 2")
    }
  }

  test("GET children should return children") {
    val headers = loginGilfoyleAndCreateHeaders

    val parentNote = noteService.getByUserIdAndPath(testFixture.testObjects.users.gilfoyle.id, Seq(1)).get

    get(s"/notes/${parentNote.id}/children", headers = headers) {
      status should equal (200)
      val parsedBody = parse(body)
      parsedBody.children.size should equal (2)

      val child1 : Note = parsedBody.children(0).extract[Note]
      child1.title should equal("Gilfoyle title 1_1")
      child1.body should equal("Gilfoyle body 1_1")

      val child2 : Note = parsedBody.children(1).extract[Note]
      child2.title should equal("Gilfoyle title 1_2")
      child2.body should equal("Gilfoyle body 1_2")
    }
  }

  test("GET children should return 404 when parent note found") {
    val headers = loginGilfoyleAndCreateHeaders
    val parentNoteId = UUIDSpoof.note(999)

    get(s"/notes/${parentNoteId}/children", headers = headers) {
      status should equal (404)
      val parsedBody = parse(body)
      val expectedResponse = ErrorResponse(404, "Not Found", s"Note $parentNoteId not found")
      (parsedBody).extract[ErrorResponse] should equal(expectedResponse)
    }
  }

  test("GET children should return empty list when no children exist") {
    val headers = loginGilfoyleAndCreateHeaders
    val parentNote = noteService.getByUserIdAndPath(testFixture.testObjects.users.gilfoyle.id,
      Seq(1,1)).get

    get(s"/notes/${parentNote.id}/children", headers = headers) {
      status should equal (200)
      val parsedBody = parse(body)
      parsedBody.children.size should equal (0)
    }
  }

  test("GET children should return 404 when gilfoyle tries to see the children of dinesh's note") {
    val headers = loginGilfoyleAndCreateHeaders
    val parentNote = noteService.getByUserIdAndPath(testFixture.testObjects.users.dinesh.id, Seq(1)).get

    get(s"/notes/${parentNote.id}/children", headers = headers) {
      status should equal (404)
      val parsedBody = parse(body)
      val expectedResponse = ErrorResponse(404, "Not Found", s"Note ${parentNote.id} not found")
      (parsedBody).extract[ErrorResponse] should equal(expectedResponse)
    }
  }

  test("GET all should return 401 when no token provided ") {
    get("/notes") {
      status should equal (401)
    }
  }

  test("GET noteId should work") {
    val headers = loginGilfoyleAndCreateHeaders

    val createdDate = new Date()
    var note = Note(
      UUID.randomUUID,
      testFixture.testObjects.users.gilfoyle.id,
      UUIDSpoof.of(5),
      "theTitle",
      "theBody",
      Instant.now,
      List()
    )
    note = noteRepo.save(note)

    get("/notes/" + note.id, Seq.empty, headers) {
      status should equal (200)

      val parsedBody = parse(body)
      val getNoteId = (parsedBody \ "id").extract[String]
      getNoteId should equal (note.id.toString)
      (parsedBody \ "title").extract[String] should equal ("theTitle")
      (parsedBody \ "body").extract[String] should equal ("theBody")

      val createdDateStr = (parsedBody \ "createdDate").extract[String]
      createdDateStr should equal(DateUtil.toIso8601(createdDate))
    }
  }

  test("GET noteId should return 404 when gilfoyle tries to see dinesh's note ") {
    val headers = loginGilfoyleAndCreateHeaders

    var note = Note(
      UUID.randomUUID,
      testFixture.testObjects.users.dinesh.id,
      UUIDSpoof.of(5),
      "theTitle",
      "theBody",
      Instant.now,
      List()
    )
    note = noteRepo.save(note)

    get("/notes/" + note.id, Seq.empty, headers) {
      status should equal (404)
      val parsedBody = parse(body)
      (parsedBody \ "status").extract[Int] should equal (404)
      (parsedBody \ "reason").extract[String] should equal ("Not Found")
      (parsedBody \ "message").extract[String] should equal (s"Note ${note.id} not found")
    }
  }


  test("GET noteId should return 404 and error message for not found") {
    val headers = loginGilfoyleAndCreateHeaders
    val noteId = UUIDSpoof.of(999)

    // read it back
    get("/notes/" + noteId, Seq.empty, headers) {
      status should equal (404)
      val parsedBody = parse(body)
      (parsedBody \ "status").extract[Int] should equal (404)
      (parsedBody \ "reason").extract[String] should equal ("Not Found")
      (parsedBody \ "message").extract[String] should equal (s"Note $noteId not found")
    }
  }

  test("GET noteId should return 401 when no token provided ") {
    val noteId = UUIDSpoof.of(999)
    get("/notes/" + noteId) {
      status should equal (401)
    }
  }

  test("POST should create") {

    val headers = loginGilfoyleAndCreateHeaders

    val parentNote = noteService.getByUserIdAndPath(testFixture.testObjects.users.gilfoyle.id, Seq(1)).get

    val json : JValue =
        ("title" -> "theTitle") ~
        ("body" -> "theBody") ~
        ("parentId" -> parentNote.id.toString)

    // post it
    post("/notes", jValueToBytes(json), headers) {
      status should equal (200)

      val parsedBody = parse(body)
      val noteId = (parsedBody \ "id").extract[String]
      UuidUtil.isValid(noteId) should be (true)
      (parsedBody \ "title").extract[String] should equal ("theTitle")
      (parsedBody \ "body").extract[String] should equal ("theBody")
      (parsedBody \ "userId").extract[String] should equal (testFixture.testObjects.users.gilfoyle.id.toString)
      (parsedBody \ "parentId").extract[String] should equal (parentNote.id.toString)

      val createdDateStr = (parsedBody \ "createdDate").extract[String]
      createdDateStr should not be ("")

      // read it back from db
      val note = noteRepo.get(UUID.fromString(noteId))
      note should not be (None)
      note.get.title should equal ("theTitle")
      note.get.body should equal ("theBody")
      note.get.userId should equal(testFixture.testObjects.users.gilfoyle.id)
      note.get.parentId should be (parentNote.id)
      createdDateStr should be (DateUtil.toIso8601(note.get.createdDate))

      // note id should be appended to parents children
      val updatedParentNote = noteRepo.get(parentNote.id).get
      updatedParentNote.childrenIds should be (parentNote.childrenIds :+ note.get.id)

    }
  }

  test("GET root should work") {
    val headers = loginGilfoyleAndCreateHeaders

    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)

    get("/notes/root", Seq.empty, headers) {
      status should equal (200)

      val parsedBody = parse(body)
      val getNoteId = (parsedBody \ "id").extract[String]
      getNoteId should equal (topLevelNote.id.toString)
      (parsedBody \ "title").extract[String] should equal (topLevelNote.title)
      (parsedBody \ "body").extract[String] should equal (topLevelNote.body)

      val createdDateStr = (parsedBody \ "createdDate").extract[String]
      createdDateStr should equal(DateUtil.toIso8601(topLevelNote.createdDate))
    }
  }

  test("POST should note be able to set childrenIds") {

    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val parentNote = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val childNote = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1,1)).get

    val childrenIds = childNote.id.toString :: Nil

    val json : JValue =
      ("title" -> "theTitle") ~
        ("body" -> "theBody") ~
        ("parentId" -> parentNote.id.toString) ~
        ("childrenIds" -> childrenIds)

    // post it
    post("/notes", jValueToBytes(json), headers) {
      status should equal (HttpStatus.SC_BAD_REQUEST)

      val parsedBody = parse(body)
      val errorResponse = (parsedBody).extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e400BadRequest("Can not set childrenIds in post, first create the note " +
        "then add children after with PATCH"))

    }
  }

  test("POST should return Bad Request when parentId unknown") {

    val headers = loginGilfoyleAndCreateHeaders
    val parentId = UUIDSpoof.note(900).toString

    val json : JValue =
        ("title" -> "theTitle") ~
        ("body" -> "theBody") ~
        ("parentId" -> parentId)

    // post it
    post("/notes", jValueToBytes(json), headers) {
      status should equal (400)
      val parsedBody = parse(body)
      val expectedResponse = ErrorResponse(400, "Bad Request", s"Unknown parentId $parentId")
    }
  }

  test("POST should return Bad Request for gilfoyle when parentId belongs to dinesh") {

    val headers = loginGilfoyleAndCreateHeaders
    val parentNote = noteService.getByUserIdAndPath(testFixture.testObjects.users.dinesh.id, Seq(1))
    val parentId = parentNote.get.id.toString

    val json : JValue =
      ("title" -> "theTitle") ~
        ("body" -> "theBody") ~
        ("parentId" -> parentId)

    // post it
    post("/notes", jValueToBytes(json), headers) {
      status should equal (400)
      val parsedBody = parse(body)
      val expectedResponse = ErrorResponse(400, "Bad Request", s"Unknown parentId $parentId")
    }
  }

  test("POST should create note at top level if no parentId specified") {

    val headers = loginGilfoyleAndCreateHeaders
    val topLevelNote = noteService.getTopLevelNote(testFixture.testObjects.users.gilfoyle.id)

    val json : JValue =
        ("title" -> "theTitle") ~
        ("body" -> "theBody")

    // post it
    post("/notes", jValueToBytes(json), headers) {
      status should equal (200)

      val parsedBody = parse(body)
      (parsedBody \ "parentId").extract[String] should equal (topLevelNote.id.toString)
      (parsedBody \ "title").extract[String] should equal ("theTitle")
      (parsedBody \ "body").extract[String] should equal ("theBody")
      (parsedBody \ "userId").extract[String] should equal (testFixture.testObjects.users.gilfoyle.id.toString)

      // read it back and check the db value
      val newNoteId = (parsedBody \ "id").extract[UUID]
      val savedNote = noteRepo.get(newNoteId)
      savedNote.get.parentId should equal(topLevelNote.id)

      // note id should be appended to parents children
      val updatedParentNote = noteRepo.get(topLevelNote.id).get
      updatedParentNote.childrenIds should be (topLevelNote.childrenIds :+ savedNote.get.id)

    }
  }

  test("POST should return 401 when no token provided") {

    val json : JValue =
      ("title" -> "theTitle") ~
        ("body" -> "theBody")

    // post it
    post("/notes", jValueToBytes(json)) {
      status should equal (401)
      val parsedBody = parse(body)
      (parsedBody).extract[ErrorResponse] should equal(TestConstants.AuthTokenNotSuppliedErorResponse)
    }
  }

  test("POST should not be able to set createdDate or id ") {

    val headers = loginGilfoyleAndCreateHeaders

    val uuid = UUID.fromString("ba39b0d8-c18d-4118-932b-4e8b1139fc23")
    val createdDate = "2016-06-02T23:56:53Z"
    var noteId =  ""

    val json : JValue =
        ("id" -> uuid.toString) ~
        ("title" -> "theTitle") ~
        ("body" -> "theBody") ~
        ("createdDate" -> createdDate)

    // post it
    post("/notes", jValueToBytes(json), headers) {
      status should equal (200)

      val parsedBody = parse(body)
      noteId = (parsedBody \ "id").extract[String]
      (parsedBody \ "id").extract[String] should not equal (uuid)
      (parsedBody \ "createdDate").extract[String] should not equal (createdDate)
      noteId should not equal(uuid)

      noteRepo.get(uuid) should be (None)

    }
  }

  test("POST should not be able to set userId (it should be set implicitly for the logged in user)") {

    val headers = loginGilfoyleAndCreateHeaders

    val json : JValue =
        ("title" -> "theTitle") ~
        ("body" -> "theBody") ~
        ("userId" -> UUIDSpoof.user(999).toString)

    // post it
    post("/notes", jValueToBytes(json), headers) {
      status should equal (200)

      val parsedBody = parse(body)
      val noteId = (parsedBody \ "id").extract[UUID]
      (parsedBody \ "userId").extract[String] should equal (testFixture.testObjects.users.gilfoyle.id.toString)

      val note = noteRepo.get(noteId).get
      note.userId should equal (testFixture.testObjects.users.gilfoyle.id)

    }
  }


  test("PATCH should update body and title") {
    val headers = loginGilfoyleAndCreateHeaders

    var note = Note(
      UUID.randomUUID,
      testFixture.testObjects.users.gilfoyle.id,
      UUIDSpoof.of(5),
      "theTitle",
      "theBody",
      Instant.now,
      List()
    )
    note = noteRepo.save(note)

    val json : JValue =
        ("title" -> "theNewTitle") ~
        ("body" -> "theNewBody")

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should equal (200)

      // check response is correct
      val parsedBody = parse(body)
      // val noteId =
      (parsedBody \ "id").extract[String] should equal (note.id.toString)
      (parsedBody \ "body").extract[String] should equal ("theNewBody")
      (parsedBody \ "title").extract[String] should equal ("theNewTitle")
      (parsedBody \ "createdDate").extract[String] should not be (None)

      val savedNote = noteRepo.get(note.id)
      savedNote should not be (None)
      savedNote.get.title should be ("theNewTitle")
      savedNote.get.body should be ("theNewBody")

    }
  }

  test("PATCH with no fields should return 200 and update nothing") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get

    val json: JValue = ("invalidField" -> "")

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should be(HttpStatus.SC_OK)

      // read back the note
      val savedNote = noteRepo.get(note.id).get
      savedNote should be(note)
    }
  }

  test("PATCH with all fields but none altered should work") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get

    val json: JValue = ("parentId" -> note.parentId.toString) ~
      ("childrenIds" -> note.childrenIds.map(_.toString)) ~
      ("body" -> note.body) ~
      ("title" -> note.title)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should be(HttpStatus.SC_OK)

      // read back the note
      val savedNote = noteRepo.get(note.id).get
      savedNote should be(note)
    }
  }


  test("PATCH note owner should return 200 but not update the owner") {
    val headers = loginGilfoyleAndCreateHeaders

    var note = Note(
      UUID.randomUUID,
      testFixture.testObjects.users.gilfoyle.id,
      UUIDSpoof.of(5),
      "theTitle",
      "theBody",
      Instant.now,
      List()
    )
    note = noteRepo.save(note)

    val json : JValue =
      ("userId" -> testFixture.testObjects.users.dinesh.id.toString)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should equal (200)

      val savedNote = noteRepo.get(note.id)
      savedNote should not be (None)
      savedNote.get.userId should be (testFixture.testObjects.users.gilfoyle.id)

    }
  }

  test("PATCH should not update createdDate and id") {

    val headers = loginGilfoyleAndCreateHeaders

    val originalDate = Instant.now
    val uuid = UUID.fromString("ba39b0d8-c18d-4118-932b-4e8b1139fc23")
    var note = Note(
      UUID.randomUUID,
      testFixture.testObjects.users.gilfoyle.id,
      UUIDSpoof.of(5),
      "theTitle",
      "theBody",
      originalDate,
      List()
    )
    note = noteRepo.save(note)

    val amendedDate = originalDate.plusSeconds(5)
    val json : JValue =
      ("createdDate" -> DateUtil.toIso8601(amendedDate)) ~
      ("id" -> uuid.toString)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should equal (200)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "createdDate").extract[String] should be (DateUtil.toIso8601(originalDate))
      (parsedBody \ "id").extract[String] should be (note.id.toString)

      val savedNote = noteRepo.get(note.id)
      savedNote.get.createdDate should be (originalDate)

      // check db existence of ids
      noteRepo.get(uuid) should be (None)
      noteRepo.get(note.id) should not be (None)

    }
  }

  // when parentId is updated 3 things need to happen
  // - parentId is updated on the note
  // - note is added to children of new parent
  // - note is removed from children list of old parent
  test("PATCH should update parentId") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val newParent = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get
    val oldParent = noteService.get(note.parentId, gilfoyle.id).get

    val json : JValue =
      ("parentId" -> newParent.id.toString)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should equal (200)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "parentId").extract[UUID] should equal (newParent.id)

      val savedNote = noteService.get(note.id ,gilfoyle.id).get
      val savedNewParent = noteService.get(newParent.id ,gilfoyle.id).get
      val savedOldParent = noteService.get(oldParent.id ,gilfoyle.id).get

      // check note parentId is updated
      savedNote.parentId should be (newParent.id)

      // check new note parent's children are updated
      savedNewParent.childrenIds should be (newParent.childrenIds :+ note.id)

      // check old note parent's children are updated
      val expectedChildrenIds = oldParent.childrenIds.filter(_ != note.id)
      savedOldParent.childrenIds should be (expectedChildrenIds)

    }
  }

  test("PATCH should return 400 when attempt to update parentId to non existing note") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get

    val parentId = UUIDSpoof.note(900).toString
    val json : JValue = ("parentId" -> parentId)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should be (E400BadRequest.code)

      // check response is correct
      val parsedBody = parse(body)
      val expectedError = new ErrorResponse(E400BadRequest.code, E400BadRequest.reason, s"Parent note $parentId not found")
      parsedBody.extract[ErrorResponse] should equal(expectedError)
    }
  }

  test("PATCH should return 400 when attempt to update parentId of gilfoyles note to one of dinesh's notes") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle
    val dinesh = testFixture.testObjects.users.dinesh
    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val parentNote = noteService.getByUserIdAndPath(dinesh.id, Seq(1)).get

    val parentId = parentNote.id.toString
    val json : JValue = ("parentId" -> parentId)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should be (E400BadRequest.code)

      // check response is correct
      val parsedBody = parse(body)
      val expectedError = ErrorResponse.e400BadRequest(s"Parent note $parentId not found")
      parsedBody.extract[ErrorResponse] should equal(expectedError)
    }
  }

  test("PATCH should return 400 when attempt to update parentId of top level note") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getTopLevelNote(gilfoyle.id)
    val parentNote = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get

    val parentId = parentNote.id.toString
    val json : JValue = ("parentId" -> parentId)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should be (E400BadRequest.code)

      // check response is correct
      val parsedBody = parse(body)
      val expectedError = ErrorResponse.e400BadRequest("Can not set parent note for top level note")
      parsedBody.extract[ErrorResponse] should equal(expectedError)
    }

  }

  test("PATCH should return 400 when attempt to update parentId to note itself") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get


    val json : JValue = ("parentId" -> note.id.toString)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should be (E400BadRequest.code)

      // check response is correct
      val parsedBody = parse(body)
      val expectedError = ErrorResponse.e400BadRequest(
        s"Can not set parentId (${note.id}) to a descendant of targetNote (${note.id})")
      parsedBody.extract[ErrorResponse] should equal(expectedError)
    }

  }

  test("PATCH should return 400 when attempt to update parentId to a descendant of note") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val parentNote = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1,2)).get

    val parentId = parentNote.id.toString
    val json : JValue = ("parentId" -> parentId)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should be (E400BadRequest.code)

      // check response is correct
      val parsedBody = parse(body)
      val expectedError = ErrorResponse.e400BadRequest(
        s"Can not set parentId (${parentNote.id}) to a descendant of targetNote (${note.id})")
      parsedBody.extract[ErrorResponse] should equal(expectedError)
    }

  }

  // probably a couple of different cases here with descendants and stuff with different levels

  test("PATCH should return 400 when attempt to update parentId and childrenIds in the same request") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val noteToMakeNewParent = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get
    val noteToAddToChildren = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,1)).get

    val childrenIdStrs = noteToUpdate.childrenIdsAsStrings :+ noteToAddToChildren.id.toString

    val json : JValue = ("parentId" -> noteToMakeNewParent.id.toString) ~
      ("childrenIds" -> childrenIdStrs)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (E400BadRequest.code)

      // check response is correct
      val parsedBody = parse(body)
      val expectedError = ErrorResponse.e400BadRequest(s"Can not update parentId and childrenIds in the same request")
      parsedBody.extract[ErrorResponse] should equal(expectedError)
    }

  }

  test("PATCH should be able to re-sort childrenIds") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    note.childrenIds.size should be (2)

    val newChildrenIds = note.childrenIds.reverse
    val newChildrenIdStrs = note.childrenIdsAsStrings.reverse

    val json : JValue =
      ("childrenIds" -> newChildrenIdStrs)

    // patch it
    patch("/notes/" + note.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_OK)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "childrenIds").extract[List[String]] should equal (newChildrenIdStrs)

      // read note back and check children
      val savedNote = noteService.get(note.id ,gilfoyle.id).get
      savedNote.childrenIds should be (newChildrenIds)

    }

  }

  test("PATCH should be able to append a child note") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val noteToAddToChildren = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,1)).get
    val noteOldParent  = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get

    val childrenIds = noteToUpdate.childrenIds :+ noteToAddToChildren.id
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_OK)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "childrenIds").extract[List[UUID]] should be (childrenIds)

      // read back the note
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote.childrenIds should be (childrenIds)

      // read back the new child and check the parentId
      val noteToAddToChildrenSaved = noteRepo.get(noteToAddToChildren.id).get
      noteToAddToChildrenSaved.parentId should be (noteToUpdate.id)

      // check the old parent and make sure child removed
      val noteOldParentSaved = noteRepo.get(noteOldParent.id).get
      val expectedChildrenIds = noteOldParent.childrenIds.filterNot(_ == noteToAddToChildren.id)
      noteOldParentSaved.childrenIds should be (expectedChildrenIds)

    }
  }

  test("PATCH should be able to prepend a child note") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val noteToAddToChildren = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,1)).get
    val noteOldParent  = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get

    val childrenIds = noteToAddToChildren.id :: noteToUpdate.childrenIds
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_OK)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "childrenIds").extract[List[UUID]] should be (childrenIds)

      // read back the note
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote.childrenIds should be (childrenIds)

      // read back the new child and check the parentId
      val noteToAddToChildrenSaved = noteRepo.get(noteToAddToChildren.id).get
      noteToAddToChildrenSaved.parentId should be (noteToUpdate.id)

      // check the old parent and make sure child removed
      val noteOldParentSaved = noteRepo.get(noteOldParent.id).get
      val expectedChildrenIds = noteOldParent.childrenIds.filterNot(_ == noteToAddToChildren.id)
      noteOldParentSaved.childrenIds should be (expectedChildrenIds)
    }
  }

  test("PATCH should be able to insert a child note") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val noteToAddToChildren = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,1)).get
    val noteOldParent  = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get

    // sanity check
    noteToUpdate.childrenIds.size should be (2)

    val childrenIds = noteToUpdate.childrenIds.take(1) ++ List(noteToAddToChildren.id) ++
      noteToUpdate.childrenIds.takeRight(1)
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_OK)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "childrenIds").extract[List[UUID]] should be (childrenIds)

      // read back the note
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote.childrenIds should be (childrenIds)

      // read back the new child and check the parentId
      val noteToAddToChildrenSaved = noteRepo.get(noteToAddToChildren.id).get
      noteToAddToChildrenSaved.parentId should be (noteToUpdate.id)

      // check the old parent and make sure child removed
      val noteOldParentSaved = noteRepo.get(noteOldParent.id).get
      val expectedChildrenIds = noteOldParent.childrenIds.filterNot(_ == noteToAddToChildren.id)
      noteOldParentSaved.childrenIds should be (expectedChildrenIds)
    }
  }

  test("PATCH should not be able to set note as child note of itself") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get

    val childrenIds = noteToUpdate.childrenIds :+ noteToUpdate.id
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_BAD_REQUEST)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody).extract[ErrorResponse] should be (ErrorResponse.e400BadRequest(
        s"Can not set note (${noteToUpdate.id}) to be descendant of (${noteToUpdate.id}) as it would cause a " +
          s"circular reference"))

      // read back the note, should not have changed
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote should be (noteToUpdate)

    }
  }

  test("PATCH should not be able to set note as descendant of itself") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1,1)).get
    val noteToAddToChildren = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get

    val childrenIds = noteToUpdate.childrenIds :+ noteToAddToChildren.id
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_BAD_REQUEST)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody).extract[ErrorResponse] should be (ErrorResponse.e400BadRequest(
        s"Can not set note (${noteToAddToChildren.id}) to be descendant of (${noteToUpdate.id}) as it would cause a " +
          s"circular reference"))

      // read back the note, should not have changed
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote should be (noteToUpdate)

    }
  }

  test("PATCH should not be able to orphan a child note") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get

    val childrenIds = noteToUpdate.childrenIds.take(1)
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_BAD_REQUEST)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody).extract[ErrorResponse] should be (ErrorResponse.e400BadRequest(
        s"Can not update note children as it would result in orphaned note(s)"))

      // read back the note, should not have changed
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote should be (noteToUpdate)
    }
  }

  test("PATCH should not be able to add the same note to childrenIds twice") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get

    val childrenIds = noteToUpdate.childrenIds ++ noteToUpdate.childrenIds.take(1)
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_BAD_REQUEST)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody).extract[ErrorResponse] should be (ErrorResponse.e400BadRequest(
        s"Can not update note as some children are added more than once."))

      // read back the note, should not have changed
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote should be (noteToUpdate)
    }
  }

  test("PATCH should be able to update children if parentId provided and has not changed") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val noteToAddToChildren = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,1)).get
    val noteOldParent  = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get

    val childrenIds = noteToUpdate.childrenIds :+ noteToAddToChildren.id
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs) ~
      ("parentId" -> noteToUpdate.parentId.toString)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_OK)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "childrenIds").extract[List[UUID]] should be (childrenIds)

      // read back the note
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote.childrenIds should be (childrenIds)

      // read back the new child and check the parentId
      val noteToAddToChildrenSaved = noteRepo.get(noteToAddToChildren.id).get
      noteToAddToChildrenSaved.parentId should be (noteToUpdate.id)

      // check the old parent and make sure child removed
      val noteOldParentSaved = noteRepo.get(noteOldParent.id).get
      val expectedChildrenIds = noteOldParent.childrenIds.filterNot(_ == noteToAddToChildren.id)
      noteOldParentSaved.childrenIds should be (expectedChildrenIds)

    }
  }

  test("PATCH should be able to update parentId if children provided but unchanged") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToUpdate = noteService.getByUserIdAndPath(gilfoyle.id, Seq(1)).get
    val noteToSetAsParent = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get

    val childrenIds = noteToUpdate.childrenIds
    val childrenIdStrs = childrenIds.map(_.toString)

    val json : JValue = ("childrenIds" -> childrenIdStrs) ~
      ("parentId" -> noteToSetAsParent.id.toString)

    // patch it
    patch("/notes/" + noteToUpdate.id, jValueToBytes(json), headers) {
      status should be (HttpStatus.SC_OK)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "childrenIds").extract[List[UUID]] should be (childrenIds)

      // read back the note
      val savedNote = noteRepo.get(noteToUpdate.id).get
      savedNote.childrenIds should be (childrenIds)
      savedNote.parentId should be (noteToSetAsParent.id)
    }
  }

  test("PATCH should return 404 for unknown note ") {
    val headers = loginGilfoyleAndCreateHeaders
    val noteId = UUIDSpoof.of(1).toString

    val json : JValue =
      ("title" -> "theNewTitle") ~
        ("body" -> "theNewBody")

    patch("/notes/" + noteId, jValueToBytes(json), loginGilfoyleAndCreateHeaders) {
      status should equal (404)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "status").extract[Int] should equal (404)
      (parsedBody \ "reason").extract[String] should equal ("Not Found")
      (parsedBody \ "message").extract[String] should equal (s"Note $noteId not found")

    }
  }

  test("PATCH should return 404 when gilfoyle tries to update dinesh's note") {

    val headers = loginGilfoyleAndCreateHeaders

    val noteId = UUIDSpoof.note(101)

    var note = Note(
      noteId,
      testFixture.testObjects.users.dinesh.id,
      noteId,
      "theTitle",
      "theBody",
      Instant.now,
      List()
    )
    note = noteRepo.save(note)

    val json : JValue =
      ("title" -> "theNewTitle") ~
        ("body" -> "theNewBody")

    patch("/notes/" + noteId, jValueToBytes(json), loginGilfoyleAndCreateHeaders) {
      status should equal (404)

      // check response is correct
      val parsedBody = parse(body)
      (parsedBody \ "status").extract[Int] should equal (404)
      (parsedBody \ "reason").extract[String] should equal ("Not Found")
      (parsedBody \ "message").extract[String] should equal (s"Note $noteId not found")

    }
  }

  test("PATCH should return 401 when no token provided") {

    val json : JValue =
      ("title" -> "theTitle") ~
        ("body" -> "theBody")

    val noteId = UUIDSpoof.of(1).toString

    patch("/notes/" + noteId, jValueToBytes(json)) {
      status should equal (401)
      val parsedBody = parse(body)
      (parsedBody).extract[ErrorResponse] should equal(TestConstants.AuthTokenNotSuppliedErorResponse)
    }
  }

  // TODO : PATCH set topLevelNote as child note of another note should throw bad request
  // this should already work as part of our circular reference testing, but would like to
  // write a test to be sure

  test("DELETE noteId should delete for existing note") {

    val headers = loginGilfoyleAndCreateHeaders

    val noteId = UUID.randomUUID()

    // create note
    val note = Note(
      noteId,
      testFixture.testObjects.users.gilfoyle.id,
      UUIDSpoof.of(5),
      "title",
      "body",
      Instant.now,
      List()
    )

    // delete it
    delete("/notes/" + noteId, Seq.empty, headers) {
      status should equal (200)
      body should be (NoContent.toString)
    }

    // check it is gone
    val dbNote = noteRepo.get(noteId)
    dbNote should be (None)

  }

  test("DELETE noteId should delete all child notes") {

    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get

    // save all the child notes in a list so that we can check if they are deleted later
    var childNotes = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,1)).get :: Nil
    childNotes = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,2)).get :: childNotes
    childNotes = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,3)).get :: childNotes
    childNotes = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,3,1)).get :: childNotes
    childNotes = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2,3,1,1)).get :: childNotes

    // delete it
    delete("/notes/" + note.id, Seq.empty, headers) {
      status should equal (200)
      body should be (NoContent.toString)
    }

    // check it is gone
    var dbNote = noteRepo.get(note.id)
    dbNote should be (None)

    // check children are gone
    childNotes foreach {childNote =>
      noteRepo.get(childNote.id) should be (None)
    }

  }

  test("DELETE noteId should remove note from childrenIds of parent") {

    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val noteToDelete = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2, 1)).get
    val parentNote = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get

    // precheck to make sure we have the right setup
    parentNote.childrenIds.contains(noteToDelete.id) should be (true)

    // delete it
    delete("/notes/" + noteToDelete.id, Seq.empty, headers) {
      status should equal (200)
      body should be (NoContent.toString)
    }

    // check it is gone
    var dbNote = noteRepo.get(noteToDelete.id)
    dbNote should be (None)

    // check children are gone
    val parentNoteAfter = noteService.getByUserIdAndPath(gilfoyle.id, Seq(2)).get
    parentNoteAfter.childrenIds.contains(noteToDelete.id) should be (false)

  }

  test("DELETE toplevel note should throw conflict") {

    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    val note = noteService.getTopLevelNote(gilfoyle.id)

    // delete it
    delete("/notes/" + note.id, Seq.empty, headers) {
      status should equal (HttpStatus.SC_CONFLICT)
      val parsedBody = parse(body)
      val errorResponse = parsedBody.extract[ErrorResponse]
      errorResponse should be (ErrorResponse.e409Conflict("Can not delete top level note"))
    }


  }

  test("DELETE noteId should return 200 for not  existing note") {

    val headers = loginGilfoyleAndCreateHeaders

    val noteId = UUID.randomUUID()

    // delete non existing note
    delete("/notes/" + noteId, Seq.empty, headers) {
      status should equal (200)
      body should be (NoContent.toString)
    }

    // check it is gone
    val dbNote = noteRepo.get(noteId)
    dbNote should be (None)

  }

  test("DELETE noteId should return 401 when no token provided") {

    val noteId = UUID.randomUUID()

    delete("/notes/" + noteId) {
      status should equal (401)
      val parsedBody = parse(body)
      (parsedBody).extract[ErrorResponse] should equal(TestConstants.AuthTokenNotSuppliedErorResponse)
    }
  }

  test("DELETE should return 200 when gilfoyle tries to delete dinesh's note, but shouldn't actually delete the note") {

    val headers = loginGilfoyleAndCreateHeaders

    val noteId = UUIDSpoof.note(101)

    var note = Note(
      noteId,
      testFixture.testObjects.users.dinesh.id,
      noteId,
      "theTitle",
      "theBody",
      Instant.now,
      List()
    )
    note = noteRepo.save(note)

    // delete
    delete("/notes/" + noteId, Seq.empty, headers) {
      status should equal (200)
      body should be (NoContent.toString)
    }

    // check it is gone
    val dbNote = noteRepo.get(noteId)
    dbNote should equal(Some(note))
  }



}