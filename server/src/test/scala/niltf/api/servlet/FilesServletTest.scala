package niltf.api.servlet

import java.time.Instant
import java.util.{Date, UUID}

import niltf.api.model.Note
import niltf.api.repository.{FileDefinitionRepo, NoteLinkRepo, NoteRepo}
import niltf.api.restexception.E400BadRequest
import niltf.api.service.NoteService
import niltf.api.test.{BaseJsonServletTest, TestConstants, UUIDSpoof}
import niltf.api.util.{DateUtil, UuidUtil}
import org.apache.http.HttpStatus
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.scalatra.NoContent

class FilesServletTest extends BaseJsonServletTest {

  private val noteRepo = inject[NoteRepo]
  private val noteService = inject[NoteService]
  private val noteLinkRepo = inject[NoteLinkRepo]
  private val fileDefRepo = inject[FileDefinitionRepo]

  def loginGilfoyleAndCreateHeaders = testFixture.loginUserAndCreateHeaders(
    testFixture.testObjects.users.gilfoyle, TestConstants.TestUserPassword)

  test("POST note should create") {
    val headers = loginGilfoyleAndCreateHeaders
    val gilfoyle = testFixture.testObjects.users.gilfoyle

    // right here, need to figure out how to post a file, might be tough
//    post("/files", jValueToBytes(json), headers) {
//
//      status should equal (200)
//      val parsedBody = parse(body)
//      parsedBody.children.size should equal (2)
//
//    }
  }





}