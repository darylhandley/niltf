package niltf.api.servlet

import java.time.Instant
import java.util.{Date, UUID}

import niltf.api.constants.NiltfConstants
import niltf.api.model.Note
import niltf.api.repository.NoteRepo
import niltf.api.service.{SecurityService, NoteService}
import niltf.api.test.{BaseJsonServletTest, TestConstants, UUIDSpoof}
import niltf.api.util.{DateUtil, UuidUtil}
import org.json4s.JsonDSL._
import org.json4s._
import org.json4s.jackson.JsonMethods._

/*
 * This class does not test any specific servlet but will test our security setup to makes sure our app is secured.
 *
 */
class SecurityServletTest extends BaseJsonServletTest {

  val noteRepo = inject[NoteRepo]
  val noteService = inject[NoteService]
  val securityService = inject[SecurityService]

  test("GET /notes should return result when valid auth token provided") {

    val gilfoyleUser = testFixture.testObjects.users.gilfoyle
    val userAndPassOpt = securityService.authenticate(gilfoyleUser.email, TestConstants.TestUserPassword)
    val (token, user) = userAndPassOpt.get

    val headers = Map (NiltfConstants.AuthTokenKey -> token)

    get("/notes", Seq.empty, headers) {
      status should equal (200)
    }
  }

  test("GET /notes should return 401 when no token provided ") {
    val headers = Map ()
    get("/notes", Seq.empty, headers) {
      status should equal (401)
      val parsedBody = parse(body)
      val errorResponse = parsedBody.extract[ErrorResponse]
      errorResponse should equal(ErrorResponse(401, "Unauthorized", "Auth Token was not supplied"))
    }
  }

  test("GET /notes should return 401 when invalid token provided ") {
    val headers = Map (NiltfConstants.AuthTokenKey -> "notAValidToken")
    get("/notes", Seq.empty, headers) {
      status should equal (401)
      val parsedBody = parse(body)
      val errorResponse = parsedBody.extract[ErrorResponse]
      errorResponse should equal(ErrorResponse(401, "Unauthorized", "Unknown Auth Token 'notAValidToken'"))
    }
  }





  // post should return 202 and not 200


}