import sbt._
import Keys._
import org.scalatra.sbt._
import org.scalatra.sbt.PluginKeys._
import com.earldouglas.xwp.JettyPlugin
import com.mojolly.scalate.ScalatePlugin._
import ScalateKeys._
import com.earldouglas.xwp.JettyPlugin
import com.earldouglas.xwp.JettyPlugin.autoImport._
import com.earldouglas.xwp.ContainerPlugin.autoImport._
import sbtassembly.{MergeStrategy, PathList}
import sbtassembly.AssemblyKeys._


object ScalatraserverBuild extends Build {
  val Organization = "niltf"
  val Name = "niltfServer"
  val Version = "0.1.0-SNAPSHOT"
  val ScalaVersion = "2.11.8"
  val ScalatraVersion = "2.4.0"

  lazy val project = Project (
    "niltfserver",
    file("."),
    settings = ScalatraPlugin.scalatraSettings ++ scalateSettings ++ Seq(
      organization := Organization,
      name := Name,
      version := Version,
      scalaVersion := ScalaVersion,
      resolvers += Classpaths.typesafeReleases,
      // disable parallel exeution of tests since it was causing issues in cassandra
      parallelExecution in Test := false,
      resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases",
      libraryDependencies ++= Seq(
        "org.scalatra"            %% "scalatra"             % ScalatraVersion,
        "org.scalatra"            %% "scalatra-scalate"     % ScalatraVersion,
        "org.scalatra"            %% "scalatra-specs2"      % ScalatraVersion     % "test",
        "org.scalatra"            %% "scalatra-scalatest"   % ScalatraVersion     % "test",
        "ch.qos.logback"          % "logback-classic"       % "1.1.5"             % "runtime",
        "org.eclipse.jetty"       % "jetty-webapp"          % "9.2.15.v20160210",
        "javax.servlet"           % "javax.servlet-api"     % "3.1.0"             % "provided",
        "org.scalatra"            %% "scalatra-json"        % ScalatraVersion,
        "org.json4s"              %% "json4s-jackson"       % "3.3.0",
        "com.datastax.cassandra"  % "cassandra-driver-core" % "3.0.1",
        "com.escalatesoft.subcut" % "subcut_2.11"           % "2.1",
        "com.typesafe"            % "config" % "1.3.0",
        "org.scalatra"            %% "scalatra-auth"        % ScalatraVersion,
        "org.mindrot"             % "jbcrypt"               % "0.3m"

      ),

      // set the jetty port when running from sbt, note that there is also a port set in
      // JettyEmbedded class which is the way we normally run it
      containerPort in Jetty := 7273,

      // assembly settings, Assembly assembles our code into an executable fat jar for deployment
      // merge strategy tells it how to resolve conflicts when duplicate config files are found in our dependencies
      mainClass in assembly := Some("niltf.api.app.JettyEmbedded"),
      test in assembly := {},
      assemblyMergeStrategy in assembly := {
        case "META-INF/io.netty.versions.properties" => MergeStrategy.first
        case x =>
          val oldStrategy = (assemblyMergeStrategy in assembly).value
          oldStrategy(x)
      },


      scalateTemplateConfig in Compile <<= (sourceDirectory in Compile){ base =>
        Seq(
          TemplateConfig(
            base / "webapp" / "WEB-INF" / "templates",
            Seq.empty,  /* default imports should be added here */
            Seq(
              Binding("context", "_root_.org.scalatra.scalate.ScalatraRenderContext", importMembers = true, isImplicit = true)
            ),  /* add extra bindings here */
            Some("templates")
          )
        )
      })



  ).enablePlugins(JettyPlugin)
}
