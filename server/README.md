# scalatraServer #

## Build & Run ##

```sh
$ cd scalatraServer
$ ./sbt
> jetty:start
> browse
```

or 

```
sbt "run-main niltf.api.app.JettyEmbedded"
```


If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.

## import to intellij

The best way to do this is to import a project from the dialog. Need to import as an sbt project, can't
remember how to do it at the moment.
