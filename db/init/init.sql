CREATE  KEYSPACE niltf
  WITH REPLICATION = {
    'class' : 'SimpleStrategy',
    'replication_factor' : 1
    }
;
