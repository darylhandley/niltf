# Introduction  

Niltf is a web app that is used for taking notes similar to Evernote or OneNote.
The main difference from Evernote is that notes in Niltf are maintaned in a tree like structure
rather than Evernotes flat structure (and we have about 20,000 less other features).
It's a work in progress and generally pretty rough around the edges. 

Niltf stands for "Notes I’d  Like to File”.

# Prerequisites

* JDK8 + avaialble on your path
* Cassandra installed and listening on 127.0.0.1
* Nginx installed
* npm installed  

# General Struture

Niltfs basic structure has 3 layers.

* The client. The client is a React/Redux client that is written in javascript. It relies on the the MaterialUI component framework.
* The server. The server is a Rest service written in Scala using the scalatra framework.
* The database. We use Cassandra for the database.

# Getting it Running for Dev

## Database Setup

First get the database up and running. Aftter installing cassandra, create a database called niltf and a
user to access it by executing the database setup script init.sql. This just sets up a simple cassandra keyspace.
This setup is not suitable for produciton, only for dev/testing.

```
cqlsh -f db/init/init.sql
```

Then run all of the migrations in the folder db/migrations. You can do this manually or
with the following one liner.

```
find db/migrations -type f  | sort | xargs -I %  cqlsh -k niltf -f %
```

Sanity check; run this

```
cqlsh -k niltf -e "select * from note"
```

 and you should see something like this

```
 id | body | children_ids | created_timestamp | parent_id | title | user_id
----+------+--------------+-------------------+-----------+-------+---------
```

## Running the Server

Now we have the db setup we can run the server. To run it use;

```
cd NITLF_HOME/server
./sbt "runMain niltf.api.app.JettyEmbedded"
```

You can also start up from your IDE. Import the project from the build.sbt.  Then go to the the class
niltf.api.app.JettyEmbedded and run/debug it.

Test your setup

```
curl http://localhost:7273/api/niltf/1/notes
```


You should get a 401 with something like this returned

```
{"status":401,"reason":"Unauthorized","message":"Auth Token was not supplied"}
```


## Running the client

Start it up

```
cd NITLF_HOME/client
npm start
```

You should now be able to load the main login page at http://localhost:7272, however we’re not quite done yet… We
need to get our client talking to our server.   

## Set up hosts file and nginx

Since the client and server run on different ports, we need to port forward the traffic using nginx.
We also need to set up our hosts file so that we can use nginx server_name configruation.

Add the following (or similar you can choose any hostname that works for you) to your hosts file

```
127.0.0.1 niltf.dev
```

Then we will configure nginx to

* Forward server traffic to port 7273
* Forward client traffic to port 7272

To do this add something like this to your nginx.conf.

```
  server {
      listen       80;
      server_name  niltf.dev;

      # client
      location / {
          proxy_pass http://127.0.0.1:7272;
          proxy_set_header Host $host;
          proxy_set_header X-Forwarded-For $remote_addr;
      }

      # api
      location /api {
          proxy_pass http://127.0.0.1:7273;
          proxy_set_header Host $host;
          proxy_set_header X-Forwarded-For $remote_addr;
      }

  }
```    


Reload nginx

```
sudo nginx -s reload
```

All done, you should now be able to load the app at

http://niltf.dev/
