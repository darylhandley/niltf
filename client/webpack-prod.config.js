var webpack = require('webpack');
var path = require('path');
var buildPath = path.resolve(__dirname, 'build');
var nodeModulesPath = path.resolve(__dirname, 'node_modules');
// var TransferWebpackPlugin = require('transfer-webpack-plugin');

module.exports = {
  entry: [path.join(__dirname, '/src/index.jsx')],
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  //Render source-map file for final build
  devtool: 'source-map',
  // output config
  output: {
    path: './dist',
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    //Minify the bundle
    // new webpack.optimize.UglifyJsPlugin({
    //   compress: {
    //     //supresses warnings, usually from module minification
    //     warnings: false
    //   }
    // }),
    //Allows error warnings but does not stop compiling. Will remove when eslint is added
    // new webpack.NoErrorsPlugin(),
    //Transfer Files
    // new TransferWebpackPlugin([
    //   {from: 'www'}
    // ], path.resolve(__dirname,"src"))
    // new webpack.DefinePlugin({
    //   'process.env': {
    //     NODE_ENV: JSON.stringify('production')
    //   }
    // }),
    // new webpack.optimize.UglifyJsPlugin()
  ],
  module: {
    // preLoaders: [
    //   {
    //     test: /\.(js|jsx)$/,
    //     loader: 'eslint-loader',
    //     include: [path.resolve(__dirname, "src")],
    //     exclude: [nodeModulesPath]
    //   },
    // ],
    loaders: [
      // {test: /\.jsx?$/, exclude: /node_modules/, loader: 'react-hot-loader/babel'},
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['babel'], // 'babel-loader' is also a legal name to reference
      },
      // the url-loader uses DataUrls.
      // the file-loader emits files.
      { test: /\.(woff|woff2)$/,  loader: "url-loader?limit=10000&mimetype=application/font-woff" },
      { test: /\.ttf$/,    loader: "file-loader" },
      { test: /\.eot$/,    loader: "file-loader" },
      { test: /\.svg$/,    loader: "file-loader" },
      { test: /\.css$/, loader: 'style!css!postcss'}

      // { test: require.resolve("jquery"), loader: "imports?jQuery=jquery" }
    ]
  },
  //Eslint config
  eslint: {
    configFile: '.eslintrc' //Rules for eslint
  }
};