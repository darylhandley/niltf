import * as colors from 'material-ui/styles/colors'

export const Colors = {
  default : colors.grey600,
  defaultLight : colors.grey300,
  defaultDark : colors.grey900,
  highlight : colors.orange600,
  icon : colors.grey600,
  iconActive : colors.orange700
}