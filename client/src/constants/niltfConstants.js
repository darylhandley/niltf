
export const NiltfConstants = {
  drawerWidth : 300,
  ApiBasePath : "/api/niltf/1"
}

export const DndItemTypes = {
  NOTE: 'note',
  SQUARE: 'square'
}
