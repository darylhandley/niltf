import {List, Map, fromJS} from 'immutable';
import  formatJson from 'format-json';
import {CREATE_NOTE_RECEIVE} from './actionCreators'
import  Cookies from 'js-cookie';
import {browserHistory} from 'react-router'
import {
  Editor,
  EditorState,
  ContentState,
  RichUtils,
  convertToRaw,
  convertFromRaw
} from 'draft-js';
import {Block} from './views/Notes/components/NoteEditor/NoteEditorConstants'
import {addNewBlock} from  './views/Notes/components/NoteEditor/functions/editorFunctions'


// state looks lke this
// - activeNote
//  - id
//  - title
//  - body
// - notes []
//  - id
//  - title
//  - body

// state looks like this
// - activeNote // activeNote is what is in the editor, it gets update to the notes
//  - id
//  - title
//  - body
// - notes Map(id, note)  // the "database"
//    - note
//      - id
//      - title
//      - body
// - noteList List(noteId) // notes as displayed in the tree


function setState(state, newState) {
  return state.merge(newState);
}

function updateActiveNoteId(state, activeNoteId) {
  const newNote = state.get('notes').get(activeNoteId);
  state = state.set('activeNote', newNote);

  const body = newNote.get('body');
  let contentState;
  // If json load the editorState with convertFromRaw
  // If plain text Load the editorState with the plain text
  // This will be legacy code once all notes are converted
  // alert("creating contentState " + body)
  if (body != null && body.trim().startsWith("{")) {
    contentState = convertFromRaw(JSON.parse(body))
  } else {
    contentState = ContentState.createFromText(body)
  }

  // create editorState
  const editorState = EditorState.createWithContent(contentState)

  state = state.setIn(['activeNote', 'editorState'], editorState);

  return state
}

function deleteNoteId(state, noteId) {
  // alert("deleting note in reducer, noteId= " + noteId);
  const notes = state.get("notes")
  // get the note to delete
  const noteToDelete = notes.get(noteId)
  // alert(noteToDelete)
  // alert (noteToDelete.get("parentId"))
  const parent = notes.get(noteToDelete.get("parentId"))
  // alert(parent)
  //
  // get the parent note and delete the note from the child list
  let childrenIds = parent.get('childrenIds')
  // alert(childrenIds)
  let idx = childrenIds.indexOf(noteId)
  // alert(idx)
  if (idx >= 0) {
    // alert("deleting from children")
    childrenIds = childrenIds.remove(idx);
    // alert (noteList);
    // state = state.set('noteList', noteList);
  }
  state = state.setIn(['notes', parent.get("id"), "childrenIds"], childrenIds);

  // remove the note from the note map
  state = state.set('notes', state.get('notes').remove(noteId));

  return state
}

function deleteNoteInit(state, noteId) {
  // set the ui for the deleteNoteConfirm dialog
  state = state.setIn(['ui', 'deleteConfirmDialog', 'isOpen'], true);
  state = state.setIn(['ui', 'deleteConfirmDialog', 'noteId'], noteId);
  return state
}

function deleteNoteDialogClose(state, noteId) {
  // set the ui for the deleteNoteConfirm dialog
  state = state.setIn(['ui', 'deleteConfirmDialog', 'isOpen'], false);
  // state = state.setIn(['ui', 'deleteConfirmDialog', 'noteId'], noteId);
  return state
}

function updateActiveNoteTitle(state, newTitle) {
  state = state.setIn(['activeNote', 'title'], newTitle);
  return state
}

function updateActiveNoteBody(state, newBody) {
  state = state.setIn(['activeNote', 'body'], newBody);
  return state
}

function updateActiveNoteEditorState(state, newEditorState) {
  state = state.setIn(['activeNote', 'editorState'], newEditorState);
  return state
}

function saveNoteSuccess(state, newBody) {
  let activeNote = state.get('activeNote');
  state = state.setIn(['notes', activeNote.get('id'), 'body'], activeNote.get('body'));
  state = state.setIn(['notes', activeNote.get('id'), 'title'], activeNote.get('title'));
  return state
}

function moveNoteToParentSuccess(state, noteId, parentNoteId) {

  // alert(`reducer -> moving note ${noteId} to ${parentNoteId}`)
  const parentNote = state.getIn(['notes', parentNoteId])
  const note = state.getIn(['notes', noteId])
  const oldParentNote = state.getIn(['notes', note.get("parentId")])

  // update parentId of childNote
  state = state.setIn(['notes', noteId, 'parentId'], parentNoteId);

  // add note to childrenIds of new parent
  let childrenIds = parentNote.get('childrenIds').push(noteId)
  state = state.setIn(['notes', parentNoteId, 'childrenIds'], childrenIds);

  // remove note from childrenIds of old parent
  let oldParentChildrenIds = oldParentNote.get("childrenIds")
  oldParentChildrenIds = oldParentChildrenIds.filterNot(id => id === noteId)
  state = state.setIn(['notes', oldParentNote.get("id"), 'childrenIds'], oldParentChildrenIds)


  return state
}

function moveNoteSuccess(state, noteId, parentNoteId, newChildrenIds) {
  // alert(`reducer -> moving note ${noteId} to ${parentNoteId}`)
  const parentNote = state.getIn(['notes', parentNoteId])
  const note = state.getIn(['notes', noteId])
  const oldParentNote = state.getIn(['notes', note.get("parentId")])

  // update childrenIds of parent
  state = state.setIn(['notes', parentNoteId, 'childrenIds'], newChildrenIds);

  // if changing parents, update parentId of childNote
  // and remove note from childrenIds
  if (oldParentNote.get("id") !== parentNoteId) {
    state = state.setIn(['notes', noteId, 'parentId'], parentNoteId);

    let oldParentChildrenIds = oldParentNote.get("childrenIds")
    oldParentChildrenIds = oldParentChildrenIds.filterNot(id => id === noteId)
    state = state.setIn(['notes', oldParentNote.get("id"), 'childrenIds'], oldParentChildrenIds)
  }


  return state
}

function clearLogin(state) {
  console.log("reducer.clearLogin")
  state = state.setIn(['ui', "Login", "componentLevelError"], "");
  return state
}

function loginFailed(state) {
  console.log("reducer.loginFailed")
  state = state.setIn(['ui', "Login", "componentLevelError"],
    "Invalid username or passwrd");
  return state
}

function loginSuccess(state, json) {
  console.log("reducer.loginSuccess")
  // alert("you are succesfully logged in")
  // set the authToken in a cookie
  Cookies.set("authToken", json.authToken)
  return state
}

function loginError(state, message) {
  console.log("reducer.loginError")
  alert("An unexpected error occured" + message)
  // set the authToken in a cookie
  return state
}

// function createNewNote(state, newNoteTitle) {
//   const noteId = uuid.v4();
//   // alert(noteId);
//   const newNote = {
//     id : noteId,
//     title : newNoteTitle,
//     body : ""
//   };
//   // state = state.set('noteList', state.get('noteList').push(noteId));
//   const rootNote.
//   const rootNote = state.get("notes2").get("rootNote")
//   alert (rootNote.id)
//   state = state.set('notes', state.get('notes').set(noteId, fromJS(newNote)));
//   state = updateActiveNoteId(state, noteId);
//   return state
// }

function listToMap(list, keyField) {
  let map = Map()
  for (let listItem of list) {
    map = map.set(listItem.get(keyField), listItem)
  }
  return map
}

function getNotesReceive(state, action) {

  const notes = fromJS(action.json);
  // alert(notes)
  const noteMap = listToMap(notes, "id");
  // alert(formatJson.plain(noteMap));

  let noteIds = notes.map(n => n.get("id"))
  console.log("noteIds is" + noteIds)

  // state = state.set('noteList', fromJS(noteIds));
  let currentNotes = state.get('notes')
  if (currentNotes) {
    state = state.set('notes', currentNotes.merge(noteMap))
  } else {
    state = state.set('notes', noteMap)
  }
  // console.log("receive posts state is now " + formatJson.plain(state))

  // set the first note to be active
  // if (notes.first() != undefined) {
  //   state = updateActiveNoteId(state, notes.first().get("id"))
  // }

  return state;
}

function getRootNoteReceive(state, action) {

  const note = fromJS(action.json);
  // alert(formatJson.plain(noteMap));

  console.log("received root note " + note.get("title"))
  // state = state.setIn(['notes2', 'rootNote'], note)
  state = state.setIn(['notes2', 'rootNoteId'], note.get("id"));
  state = state.setIn(['notes', note.get("id")], note);
  // console.log("receive posts state is now " + formatJson.plain(state))
  return state;
}

function createNoteReceive(state, action) {
  // convert JS to immutable
  let note = fromJS(action.json)

  // add to note map
  // console.log("state is " + formatJson.plain(state))
  state = state.set("notes", state.get("notes").set(note.get("id"), note))

  // add to noteList at end
  // console.log("state is " + formatJson.plain(state))
  // state = state.set("noteList", state.get("noteList").push(note.get("id")))

  // add to parents children
  // const rootNoteId = state.get("notes2").get("rootNoteId")
  const parentNoteId = note.get("parentId")
  // alert(parentNoteId)
  const currentChildrenIds = state.get("notes").get(parentNoteId).get("childrenIds")
  // alert(currentChildrenIds)
  state = state.setIn(["notes", parentNoteId, "childrenIds"], currentChildrenIds.push(note.get("id")))


  // set our new note to be active
  state = updateActiveNoteId(state, note.get("id"));

  return state
}


function uploadFileSuccess(state, result) {
  // alert(`reducer -> moving note ${noteId} to ${parentNoteId}`)
  alert("upload file success, path=" + result.path)

  // insert the newly created file into our document
  // const src = URL.createObjectURL(result.path);
  // alert(src);
  const src = result.path

  const editorState = state.getIn(['activeNote', 'editorState']);
  // alert(editorState)
  // alert(addNewBlock)
  const newEditorState = addNewBlock(
    editorState,
      Block.IMAGE, {src,
    }
  )
  state = state.setIn(['activeNote', 'editorState'], newEditorState);
  return state

  // onChange(e) {
  //   // e.preventDefault();
  //   const file = e.target.files[0];
  //   if (file.type.indexOf('image/') === 0) {
  //     // console.log(this.props.getEditorState());
  //     // eslint-disable-next-line no-undef
  //     const src = URL.createObjectURL(file);
  //     this.props.setEditorState(addNewBlock(
  //       this.props.getEditorState(),
  //       Block.IMAGE, {
  //         src,
  //       }
  //     ));
  //   }
  //   this.props.close();
  // }
}

export default function(state = Map(), action) {
  console.log("main reducer actionType= " + action.type)
  switch (action.type) {
  case 'SET_STATE':
    return setState(state, action.state);
  case 'UPDATE_ACTIVE_NOTE_ID':
    return updateActiveNoteId(state, action.activeNoteId)
  case 'CREATE_NEW_NOTE':
    return createNewNote(state, action.newNoteTitle)
  case 'UPDATE_ACTIVE_NOTE_TITLE':
    return updateActiveNoteTitle(state, action.activeNoteTitle)
  case 'UPDATE_ACTIVE_NOTE_BODY':
    return updateActiveNoteBody(state, action.activeNoteBody)
  case 'UPDATE_ACTIVE_NOTE_EDITOR_STATE':
    return updateActiveNoteEditorState(state, action.editorState)
  case 'DELETE_NOTE_ID':
    // alert("DELETE_NOTE_ID reduce switch");
    return deleteNoteId(state, action.noteId);
  case 'FETCH_NOTES_REQUEST':
      alert("Fetching notes.");
      // return deleteNoteId(state, action.noteId);
  case 'GET_NOTES_RECEIVE':
    // alert("Received posts.");
    let newState = getNotesReceive(state, action)
    // console.log("new state is " + formatJson.plain(newState))
    return newState
  case 'GET_ROOT_NOTE_RECEIVE': {
    // alert("Received posts.");
    let newState = getRootNoteReceive(state, action)
    // console.log("new state is " + formatJson.plain(newState))
    return newState
  }
  case 'CREATE_NOTE_RECEIVE': {
    let myNewState = createNoteReceive(state, action)
    // console.log("new state is " + formatJson.plain(myNewState))
    return myNewState
  }
  case 'DELETE_NOTE_RECEIVE': {
      console.log("deleting note locally, noteId=" + action.noteId)
      let myNewState = deleteNoteId(state, action.noteId)
      // console.log("new state is " + formatJson.plain(myNewState))
      return myNewState
  }
  case 'DELETE_NOTE_INIT': {
    console.log("Initializing note delete for, noteId=" + action.noteId)
    let myNewState = deleteNoteInit(state, action.noteId)
    // console.log("new state is " + formatJson.plain(myNewState))
    return myNewState
  }
  case 'DELETE_NOTE_DIALOG_CLOSE': {
    console.log("Closing delete note dialog")
    let myNewState = deleteNoteDialogClose(state)
    // console.log("new state is " + formatJson.plain(myNewState))
    return myNewState
    }
  case 'SAVE_NOTE_RECEIVE': {
      console.log("updating note" + action.noteId)
      let myNewState = saveNoteSuccess(state)
      // console.log("new state is " + formatJson.plain(myNewState))
      return myNewState
  }
  case 'CLEAR_LOGIN': {
      console.log("Reducer -> clearing login ")
      let myNewState = clearLogin(state)
      // console.log("new state is " + formatJson.plain(myNewState))
      return myNewState
  }
  case 'LOGIN_FAILED': {
      console.log("Reducer -> login failed")
      let myNewState = loginFailed(state)
      // console.log("new state is " + formatJson.plain(myNewState))
      return myNewState
  }
  case 'LOGIN_SUCCESS': {
      console.log("Reducer -> login success")
      let myNewState = loginSuccess(state, action.json)
      // console.log("new state is " + formatJson.plain(myNewState))
      return myNewState
  }
  case 'LOGIN_ERROR': {
      console.log("Reducer -> login error")
      let myNewState = loginError(state, action.message)
      // console.log("new state is " + formatJson.plain(myNewState))
      return myNewState
  }
  case 'LOGOUT': {
    console.log("Reducer -> logout")
    // TODO: we should probable clear all the state here
    // set the cookie to null
    Cookies.remove("authToken")

    // redirect to splash
    browserHistory.push('/')

    return state
  }
  case 'MOVE_NOTE_TO_PARENT_SUCCESS': {
    console.log("Reducer -> move not to parent success")
    let myNewState = moveNoteToParentSuccess(state, action.noteId, action.parentNoteId)
    // console.log("new state is " + formatJson.plain(myNewState))
    return myNewState
  }
  case 'MOVE_NOTE_SUCCESS': {
    console.log("Reducer -> move note success")
    let myNewState = moveNoteSuccess(state, action.noteId, action.parentNoteId, action.newChildrenIds)
    // console.log("new state is " + formatJson.plain(myNewState))
    return myNewState
  }
  case 'UPLOAD_FILE_SUCCESS': {
    console.log("Reducer -> upload file success")
    let myNewState = uploadFileSuccess(state, action.result)
    // console.log("new state is " + formatJson.plain(myNewState))
    return myNewState
  }

  }
  return state;
}
