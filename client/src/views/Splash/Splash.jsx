import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import * as actionCreators from '../../actionCreators';
import {Tabs, Tab} from 'material-ui/Tabs';

// material ui
import TextField from 'material-ui/TextField'
import {Login} from './Login'
import {Signup} from './Signup'

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};

export const Splash = React.createClass({
  mixins: [PureRenderMixin],
  // don't need this right now but keep around for later, it lets you get the router using this.context.router
  // right now hashHistory seems to work great
  // contextTypes: {
  //   router: React.PropTypes.object.isRequired
  // },

  handleLogin : function(email, password) {
    // alert ("in splash handling login, email=" + email + ", password=" + password)
    this.props.login(email, password)
    // alert ("done splash handling")

  },

  clearLogin : function() {
    console.log ("Splash.clearLogin")
    this.props.clearLogin()
  },

  handleSignup : function(email, password) {
    // alert ("in splash handling login, email=" + email + ", password=" + password)
    this.props.signup(email, password)
    // alert ("done splash handling")

  },

  render: function() {
      //alert("Splash render :"  + this.props.loginUiState)
      return (
          <div>
            <Tabs>
               <Tab label="Login">
                 <div>
                   <h2 style={styles.headline}>Login</h2>
                   <Login
                     clearLogin={this.clearLogin}
                     handleLogin={this.handleLogin}
                     uiState={this.props.loginUiState}
                     />
                 </div>
               </Tab>
               <Tab label="Signup">
                 <div>
                  <h2 style={styles.headline}>Signup</h2>
                   <Signup
                     handleSignup={this.handleSignup}
                    />
                 </div>
               </Tab>
             </Tabs>
          </div>
      )
  }
});

function mapStateToProps(state) {
  // alert(state.get('ui').get("Login"))
  return {
    loginUiState: state.get('ui').get("Login"),
  }
}

export const SplashContainer = connect(
  mapStateToProps,
  actionCreators
)(Splash);
