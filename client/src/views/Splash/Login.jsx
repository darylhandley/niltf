import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import * as actionCreators from '../../actionCreators';

// material ui
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton';

export const Login = React.createClass({
  getInitialState : function() {
    return {"email": "", "password" : ""}
  },

  handleEmailChange : function(ev) {
    //alert("changing email  to " + ev.target.value)
    this.setState({email : ev.target.value})
  },

  handlePasswordChange : function(ev) {
    //alert("changing password  to " + ev.target.value)
    this.setState({password : ev.target.value})
  },

  handleLogin : function(ev) {

    console.log("Login.clearLogin")
    this.props.clearLogin()
    console.log("Login.clearLogin Complete")


    // alert("logging in with email=" + this.state.email + ",password=" + this.state.password)
    const password = this.state.password
    const email = this.state.email
    let errors = false

    this.setState({
      passwordError : "",
      emailError : ""
    })

    if (!email || email === "") {
      this.setState({emailError : "Email required"})
      errors = true
    }

    if (!password || password === "") {
      // alert("password required")
      this.setState({passwordError : "Password required"})
      errors = true
    }

    if (!errors) {
      this.props.handleLogin(this.state.email, this.state.password)
    }
  },


  render: function() {
      // alert("in login render " + this.props.uiState.get("componentLevelError"))
      return (
        <div>

          <TextField
            id="loginEmail"
            floatingLabelText="Email"
            value={this.state.email}
            multiLine={false}
            fullWidth={true}
            errorText={this.state.emailError}
            onChange={this.handleEmailChange} />

          <TextField
            id="loginPassword"
            floatingLabelText="Password"
            value={this.state.password}
            multiLine={false}
            fullWidth={true}
            errorText={this.state.passwordError}
            type="password"
            onChange={this.handlePasswordChange} />

          <div>
            {this.props.uiState.get("componentLevelError")}
          </div>

          <RaisedButton
            primary={true}
            label="Login"
            onClick={this.handleLogin}/>
        </div>
      )
  }
});
