import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import * as actionCreators from '../../actionCreators';

// material ui
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton';

export const Signup = React.createClass({
  getInitialState : function() {
    return {"email": "", "password" : ""}
  },

  handleEmailChange : function(ev) {
    //alert("changing email  to " + ev.target.value)
    this.setState({email : ev.target.value})
  },

  handlePasswordChange : function(ev) {
    //alert("changing password  to " + ev.target.value)
    this.setState({password : ev.target.value})
  },

  handleSignup : function(ev) {
    // alert("logging in with email=" + this.state.email + ",password=" + this.state.password)
    this.props.handleSignup(this.state.email, this.state.password)
  },


  render: function() {
      return (
        <div>

          <TextField
            id="email"
            floatingLabelText="Email"
            value={this.state.email}
            multiLine={false}
            fullWidth={true}
            onChange={this.handleEmailChange} />

          <TextField
            id="password"
            floatingLabelText="Password"
            value={this.state.password}
            multiLine={false}
            fullWidth={true}
            type="password"
            onChange={this.handlePasswordChange} />

          <RaisedButton
            primary={true}
            label="Signup"
            onClick={this.handleSignup}/>
        </div>
      )
  }
});
