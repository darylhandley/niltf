import React from 'react';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';

/**
 * I think we can also use this style to create React objects, keeping for now so that I can revisit later
 * and use as an exmple for posssible future cleanups. Not sure exactly how it works but it appears to
 * just do the render part. I am not sure exactly how we will get the rest of our stuff in here if we needed more
 * than just a render object.
 */
// const NiltfAppBar = () => (
//   <AppBar
//     title="Niltf"
//     iconElementRight={
//       <RaisedButton label="Logout"  onClick={this.logout}/>
//     }
//   />
// );


var NiltfAppBar = React.createClass({
  render: function() {
    return (
      <AppBar
        title="Niltf"
        iconElementRight={
          <RaisedButton label="Logout"  onClick={this.props.handleLogout} />
        }
      />
    );
  }
});

export default NiltfAppBar;
