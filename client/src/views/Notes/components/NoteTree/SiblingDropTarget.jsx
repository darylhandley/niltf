

import React, { Component, PropTypes } from 'react'
import {connect} from 'react-redux'
import {List, ListItem} from 'material-ui/List'
import { DropTarget } from 'react-dnd'
import { DragSource } from 'react-dnd'
import { NiltfConstants, DndItemTypes } from '../../../../constants/niltfConstants'
import { isNote1AncestorOfNote2 } from '../../../../statequery/IsNote1ValidParentForNote2'
import * as colors from 'material-ui/styles/colors'

const noteDropSpec = {
  canDrop(props, monitor, component) {
    const item = monitor.getItem()
    // const noteId = props.note.get('id')
    const parentId = props.note.get('parentId')

    // can not drop note to be a sibling of something in it's own sub tree
    // console.log(`checking can drop ${item.noteId} as ancestor of ${noteId}`)
    return (parentId !== item.noteId && !isNote1AncestorOfNote2(props.notes, item.noteId, parentId))

  },

  drop(props, monitor, component) {

    const item = monitor.getItem()
    const noteId = props.note.get('id')
    const parentId = props.note.get('parentId')
    const dropType  = props.dropType

    props.moveNoteToSibling(item.noteId, parentId, noteId, dropType)
  }
};

function noteDropCollect(connect, monitor) {
  // console.log("boardSquare.collect")
  // alert("collect for drop")
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  };
}


@DropTarget(DndItemTypes.NOTE, noteDropSpec, noteDropCollect)
export class SiblingDropTarget extends Component {

  render() {

    const { connectDropTarget, isOver, canDrop } = this.props

    const iconSize = 24
    const myIndent = this.props.level * iconSize
    // const fullWidth = NiltfConstants.drawerWidth
    const mainElementWidth = NiltfConstants.drawerWidth - myIndent

    const mainElementStyles = {
      height: 5,
      width: mainElementWidth,
      backgroundColor: (isOver && canDrop) ? colors.orange700 : null,
      padding: 0,
      marginLeft : myIndent,
      // backgroundColor: (isOver && canDrop) ? "red" : null,
      // border: "solid 1px blue"
    }

    return connectDropTarget(
      <div style={mainElementStyles}>
      </div>
    )
  }



}