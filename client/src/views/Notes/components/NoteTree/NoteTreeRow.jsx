

import React, { Component, PropTypes } from 'react'
import {connect} from 'react-redux'
import * as colors from 'material-ui/styles/colors'
import Delete from 'material-ui/svg-icons/action/delete'
import PregnantWoman from 'material-ui/svg-icons/action/pregnant-woman'
import MenuItem from 'material-ui/MenuItem'
import {List, ListItem} from 'material-ui/List'
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right'
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import {NiltfConstants, DndItemTypes} from '../../../../constants/niltfConstants'
import { DropTarget } from 'react-dnd'
import { DragSource } from 'react-dnd'
import { isNote1AncestorOfNote2 } from '../../../../statequery/IsNote1ValidParentForNote2'

const mainColor = colors.grey900
const mainHoverColor = colors.orange900

const noteDropSpec = {
  canDrop(props, monitor, component) {
    const item = monitor.getItem()
    const noteId = props.note.get('id')

    // can not drop note on itself or descendant of itself
    console.log(`checking can drop ${item.noteId} on ${noteId}`)
    return (noteId !== item.noteId && !isNote1AncestorOfNote2(props.notes, item.noteId, noteId))

  },

  drop(props, monitor, component) {
    const item = monitor.getItem()
    const noteId = props.note.get('id')
    props.moveNoteToNewParent(item.noteId, props.note.get('id'))
  }
};

function noteDropCollect(connect, monitor) {
  // console.log("boardSquare.collect")
  // alert("collect for drop")
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  };
}



@DropTarget(DndItemTypes.NOTE, noteDropSpec, noteDropCollect)
export class NoteTreeRow extends Component {

  constructor () {
    super()
    this.state = {
      titleHover: false
    }

  }

  handleClick(e) {
    this.props.updateActiveNoteId(this.props.note.get("id"))
  }

  handleDelete(e) {
    // alert("delete " + this.props.noteId)
    this.props.onDelete(this.props.note.get("id"))
    e.preventDefault()
  }

  handleAddChild(e) {
    // alert("add child " + this.props.note.get("id"))
    this.props.addChildNote(this.props.note.get("id"))
    // e.preventDefault()
  }

  handleTitleMouseEnter (e) {
    this.setState({titleHover: true})
  }

  handleTitleMouseLeave (e) {
    this.setState({titleHover: false})
  }


  render() {

    const iconSize = 24
    const myIndent = this.props.level * iconSize
    // here we calculate the widths of all the elements of the NoteTreeRow
    // - title width is the space remaining from our full width after we subtract the width for all the other components
    // - title space will decrease as the indent increases and all others will remain constant
    // - scrollBarWidth is an estimate of the width of the scrollbar and is used so that we don't get any weird
    // wrapping when the scroll bar appears (for the container)
    const numRightIcons = 1
    const fullWidth = NiltfConstants.drawerWidth
    const scrollBarWidth = 14
    const leftMenuWidth = 30
    const rightMenuWidth = numRightIcons * iconSize
    const titleWidth = fullWidth - scrollBarWidth - leftMenuWidth - rightMenuWidth - myIndent

    const { isOver, canDrop, connectDropTarget } = this.props

    const iconStyles = {
      margin: 0,
      cursor: "pointer",
      // border: "solid 1px purple"
    }

    const mainElementStyles = {
      padding : 0,
      cursor: "pointer",
      backgroundColor: (isOver && canDrop) ? colors.orange700 : null,
      // border: "solid 1px blue",
    }

    const leftMenuStyles = {
      display : "inline-block",
      margin: 0,
      padding: 0,
      width: leftMenuWidth,
      verticalAlign : "top",
      marginLeft : myIndent,
      // border: "solid 1px red"
    }

    const rightMenuStyles = {
      display : "inline-block",
      margin: 0,
      padding: 0,
      width: rightMenuWidth,
      verticalAlign : "top",
      // border: "solid 1px red"

    }

    const iconMenuStyles = {
      // display : "inline-block",
      // margin: 0,
      // padding: 0,
      // width: 32,
      // height: 32,
      // verticalAlign : "top"
      // border: "solid 1px purple"
    }

    const iconButtonStyles = {
      display : "inline-block",
      margin: 0,
      padding: 0,
      width: iconSize,
      height: iconSize,
      verticalAlign : "top",
      // border: "solid 1px red"
    }


    const titleStyles = {
      display : "inline-block",
      marginTop: 2,
      padding: 0,
      cursor: "pointer",
      width : titleWidth,
      color : this.state.titleHover ? mainHoverColor : mainColor,
      // border: "solid 1px green"
    }


    let treeExpandIcon = null
    if (this.props.note.get("childrenIds").size > 0)  {
      treeExpandIcon = this.props.isOpen ?
        <ExpandMore style={iconStyles} hoverColor={mainHoverColor} color={mainColor} />
        :
        <ChevronRight style={iconStyles} hoverColor={mainHoverColor} color={mainColor} />
    }

    return connectDropTarget(
      <div style={mainElementStyles}>
        {/*left menu (expand contract) */}
        <div style={leftMenuStyles} >
          <IconButton style={iconButtonStyles} onClick={this.props.handleExpand}>
            {treeExpandIcon}
          </IconButton>
        </div>

        {/*the title div*/}
        <div style={titleStyles} onClick={::this.handleClick}
             onMouseEnter={::this.handleTitleMouseEnter} onMouseLeave={::this.handleTitleMouseLeave}>
          {this.props.note.get("title")}
        </div>

        {/*the right menu */}
        <div style={rightMenuStyles} >
          {/*<IconButton style={iconButtonStyles} onClick={::this.handleDelete}>*/}
            {/*<Delete style={iconStyles}*/}
                    {/*hoverColor={mainHoverColor}*/}
                    {/*color={mainColor}*/}
            {/*/>*/}
          {/*</IconButton>*/}

          <IconMenu style={iconMenuStyles}
                    iconButtonElement={
                      <IconButton style={iconButtonStyles} >
                        <MoreVertIcon hoverColor={mainHoverColor} color={mainColor} />
                      </IconButton>
                    }
                    anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    color={mainColor}
          >
            <MenuItem
              primaryText="Delete"
              onClick={::this.handleDelete}
              leftIcon={<Delete/>}
            />
            <MenuItem
              primaryText="Add Child"
              onClick={::this.handleAddChild}
              leftIcon={<PregnantWoman/>}
            />
          </IconMenu>
        </div>
      </div>
    );
  }

}