import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import * as actionCreators from '../../../../actionCreators';
import {List, ListItem} from 'material-ui/List'
import {NoteTreeList} from './NoteTreeList'
import {NoteTreeAddNoteForm} from './NoteTreeAddNoteForm'
import DragAndDropSource from './DragAndDropSource'
import DragAndDropTarget from './DragAndDropTarget'

/*******************************************************************************
  NoteTree :
*******************************************************************************/
export const NoteTree = React.createClass({
  mixins: [PureRenderMixin],
  updateActiveNoteId: function(activeNoteId) {
    this.props.updateActiveNoteId(activeNoteId);
  },
  // loadChildNote: function(noteId) {
  //   alert("NoteTree load child notea")
  //   // this.props.updateActiveNoteId(activeNoteId);
  // },
  render: function() {
    // alert("noteList is" + this.props.noteList);
    if (this.props.rootNoteId != null && this.props.notes != null) {
      // alert (this.props.notes)
      // alert(this.props.rootNoteId)
      const rootNote = this.props.notes.get(this.props.rootNoteId)
      // alert(rootNote)
      return (
        <div>
          <NoteTreeAddNoteForm {...this.props} />
          <div>
            <NoteTreeList
              {...this.props}
              notes={this.props.notes}
              parentNote={rootNote}
              level={0}
            />
          </div>
          
          {/*<div>*/}
            {/*<DragAndDropSource />*/}
            {/*<DragAndDropTarget />*/}
          {/*</div>*/}
        </div>
      );
    } else {
      return (<div>Loading....</div>)
    }
  }
});


function mapStateToProps(state) {
  // alert("mapping state to props")
  return {
    notes: state.get('notes'),
    noteList: state.get('noteList'),
    rootNoteId: state.getIn(['notes2', 'rootNoteId']),
    // activeNote: state.get('activeNote')
  }
}

export const NoteTreeContainer = connect(
  mapStateToProps,
  actionCreators
)(NoteTree);

/******************************************************************************
General notes
---------------
- NoteTreeContainer
  - NoteTreeAddNoteForm
  - NoteTree - mapStateToProps, actionCreators
    - NoteTreeList
      - NoteTreeNode *
        - NoteTreeList

******************************************************************************/
