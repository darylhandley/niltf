import React from 'react'
import {connect} from 'react-redux';
import {List, ListItem} from 'material-ui/List'
import {NoteTreeNode} from './NoteTreeNode'



/*******************************************************************************
 NoteTreeList : A list of noteTreeNodes. It can be at the top level or it can
 be a sub list of a noteTreeNode that gets expanded.
 *******************************************************************************/
export const NoteTreeList = React.createClass({
  updateActiveNoteId: function(activeNoteId) {
    this.props.updateActiveNoteId(activeNoteId)
  },
  deleteNoteInit: function(noteId) {
    // alert("NoteTreeList::deleteNoteId " + noteId);
    this.props.deleteNoteInit(noteId);
  },
  addChildNote: function(noteId) {
    // alert("NoteTreeList::addChildNote " + noteId);
    this.props.addChildNote(noteId);
  },

  handleDrop : function(noteToDrop, noteToDropOn) {
    // alert(`dropping ${noteToDrop} on ${noteToDropOn}`)
    this.props.handleDrop(noteToDrop, noteToDropOn);
  },

  componentWillMount: function () {
    // alert("NoteTreeList component will mount")
  },

  componentDidMount: function () {
    // alert("NoteTreeList component did mount")
    this.props.loadChildNotes(this.props.parentNote.get("id"))
  },

  render: function() {
    var noteTreeNoteParent = this; // strange, can we fix with bind ?

    // alert(this.props.notes);
    // alert("NoteTreeList.parentNote is  " + this.props.parentNote)
    let nodes = "";
    // alert(this.props.parentNote)
    // alert(this.props.parentNote.get("childrenIds"))
    const childrenNotes = this.props.parentNote.get("childrenIds")
    // alert(childrenNotes)
    if (childrenNotes) {
      let idx = 0
      nodes = childrenNotes.map(function(noteId) {
        console.log("creating note for " + noteId)
        const note = noteTreeNoteParent.props.notes != null ? noteTreeNoteParent.props.notes.get(noteId) : null;
        if (note) {
          idx = idx + 1
          return (
            <NoteTreeNode
              {...noteTreeNoteParent.props}
              key={note.get('id')}
              note={note}
              updateActiveNoteId={noteTreeNoteParent.updateActiveNoteId}
              onDelete={noteTreeNoteParent.deleteNoteInit}
              handleDrop={noteTreeNoteParent.handleDrop}
              addChildNote={noteTreeNoteParent.addChildNote}
              level={noteTreeNoteParent.props.level}
              isFirst={idx === 1}
            />
          )
        }
      })
    }


    return (
      <div>
        {nodes}
      </div>
    );
  }

});