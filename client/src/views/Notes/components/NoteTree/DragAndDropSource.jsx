import React, { Component, PropTypes } from 'react';
import { DndItemTypes } from '../../../../constants/niltfConstants';
import { DropTarget } from 'react-dnd';
import { DragSource } from 'react-dnd';

// const squareDropSpec = {
//   canDrop(props) {
//     // return canMoveKnight(props.x, props.y);
//     return true
//   },
//
//   drop(props, monitor, component) {
//     // alert("component is " + component)
//     // alert("result is  " + monitor.getDropResult())
//     const item = monitor.getItem()
//     alert(`dropped [${item.x},${item.y}] on [${props.x},${props.y}]`)
//   }
// };
//
// function collectDrop(connect, monitor) {
//   // console.log("boardSquare.collect")
//   return {
//     connectDropTarget: connect.dropTarget(),
//     isOver: monitor.isOver(),
//     canDrop: monitor.canDrop()
//   };
// }

const squareDragSpec = {
  beginDrag(props, monitor, component) {
    // alert('begin drag')
    return {};
  }
};

function collectDrag(connect, monitor) {
  // console.log("knight.collect")
  // alert("colecting")
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  }
}

@DragSource(DndItemTypes.NOTE, squareDragSpec, collectDrag)
export default class DragAndDropSource extends Component {

  // static propTypes = {
  //   isOver: PropTypes.bool.isRequired,
  //   canDrop: PropTypes.bool.isRequired,
  //   isDragging: PropTypes.bool.isRequired
  // };


  // renderOverlay(color) {
  //   return (
  //     <div style={{
  //       position: 'absolute',
  //       top: 0,
  //       left: 0,
  //       height: '100%',
  //       width: '100%',
  //       zIndex: 1,
  //       opacity: 0.5,
  //       backgroundColor: color,
  //     }} />
  //   );
  // }

  render() {
    // const {isOver, canDrop, connectDragSource, isDragging } = this.props;
    const { connectDragSource } = this.props
    // const black = (x + y) % 2 === 1;

    // return connectDragSource(
    //   <div style={{ position: 'relative', width: 100, height: 100}}>
    //     Drag
    //   </div>
    // );
    // return connectDragSource(
    // alert(connectDragSource)
    return connectDragSource(
      <div style={{ position: 'relative', width: 60, height: 60, backgroundColor: 'red'}}>
        Drag
      </div>
    );

  }
}

