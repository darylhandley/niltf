

import React, { Component, PropTypes } from 'react'
import {connect} from 'react-redux'
import {List, ListItem} from 'material-ui/List'
import {NoteTreeList} from './NoteTreeList'
import {NoteTreeRow} from './NoteTreeRow'
import {DndItemTypes} from '../../../../constants/niltfConstants'
import { DropTarget } from 'react-dnd';
import { DragSource } from 'react-dnd';
import { SiblingDropTarget } from './SiblingDropTarget';

const noteDragSpec = {
  beginDrag(props, monitor, component) {
    // alert("begin drag of" + props.note.get('id'));
    return {
      'noteId' : props.note.get('id')
    }
  }
}

function noteDragCollect(connect, monitor) {
  console.log("noteDragCollect")
  // alert("dragCollect")
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  }
}

@DragSource(DndItemTypes.NOTE, noteDragSpec, noteDragCollect)
export class NoteTreeNode extends Component {

  constructor () {
    super()
    this.state = {
      isOpen : false
    }
  }


  render() {

    let childNotesTree = ""
    if (this.props.note.get("childrenIds").size > 0 && this.state.isOpen) {
      childNotesTree =
        <NoteTreeList
          {...this.props}
          notes={this.props.notes}
          parentNote={this.props.note}
          level={this.props.level + 1}
        />
    }

    const { connectDragSource, isDragging, connectDragPreview } = this.props
    // console.log(`isDragging = ${isDragging}`)

    // only show drop before for the first in a tree level, otherwise we can simply use drop after
    let dropBefore = this.props.isFirst ? <SiblingDropTarget {...this.props} dropType="before"/> : ""

    let style = {
      // border: "solid 1px red",
      // opacity: isDragging ? 0.5 : 1,
    }

    // not currently working but leaving for now in case I figure it out later
    // trying to make it less opaque than the default html drag and drop does
    // so we can see what we are doing better
    // let dragPreview = connectDragPreview(
    //   <div style={{
    //     backgroundColor : "pink",
    //     // display: isDragging ? "block" : "none",
    //   }}>
    //     You are dragging this
    //   </div>
    // )

    return connectDragSource(

      <div style={style} >
        {/*{dragPreview}*/}
        {dropBefore}
        <NoteTreeRow
          {...this.props}
          handleExpand={::this.handleExpand}
          isOpen={this.state.isOpen}
        />
        {childNotesTree}
        <SiblingDropTarget {...this.props} dropType="after" />
      </div>
    );
  }

  handleExpand(e) {
    // alert("toggling nested nodes " + this.state.isOpen)
    this.setState({isOpen: !this.state.isOpen})
  }



}