import React, { Component, PropTypes } from 'react';
import { DndItemTypes } from '../../../../constants/niltfConstants';
import { DropTarget } from 'react-dnd';
import { DragSource } from 'react-dnd';

const squareDropSpec = {
  canDrop(props) {
    // return canMoveKnight(props.x, props.y);
    return true
  },

  drop(props, monitor, component) {
    // alert("component is " + component)
    alert("dropped");
    const item = monitor.getItem()
    // alert(`dropped [${item.x},${item.y}] on [${props.x},${props.y}]`)
  }
};

function collectDrop(connect, monitor) {
  // console.log("boardSquare.collect")
  // alert("collect for drop")
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  };
}

@DropTarget(DndItemTypes.NOTE, squareDropSpec, collectDrop)
export default class DragAndDropTarget extends Component {


  render() {
    const { connectDropTarget } = this.props

    return connectDropTarget(
      <div style={{ position: 'relative', width: 60, height: 60, backgroundColor: 'green'}}>
        Drop
      </div>
    );
  }
}

