import React from 'react'
import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List'


/******************************************************************************
 NoteTreeAddNoteForm : allows us to add a new note by entering
 a title and clicking submit
 *******************************************************************************/
export const NoteTreeAddNoteForm = React.createClass({
  getInitialState: function() {
    return {noteTitle: ''};
  },
  handleNoteTitleChange : function(e) {
    this.setState({noteTitle : e.target.value})
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var noteTitle = this.state.noteTitle.trim();
    if (!noteTitle) {
      return;
    }
    this.props.createNewNote(noteTitle);
    this.setState({noteTitle: ''})
  },
  render: function() {
    return (
      <div>
        <TextField
          size="20"
          hintText="Enter new note title..."
          value = {this.state.noteTitle}
          onChange={this.handleNoteTitleChange}
        />
        <RaisedButton label="Add" onClick={this.handleSubmit}/>
      </div>
    );
  }
});
