import {connect} from 'react-redux';
import IconButton from 'material-ui/IconButton';
import ActionCode from 'material-ui/svg-icons/action/code';
import FormatBold from 'material-ui/svg-icons/editor/format-bold';
import FormatItalic from 'material-ui/svg-icons/editor/format-italic';
import FormatQuote from 'material-ui/svg-icons/editor/format-quote';
import FormatListBulleted from 'material-ui/svg-icons/editor/format-list-bulleted';
import FormatListNumbered from 'material-ui/svg-icons/editor/format-list-numbered';
import FormatUnderlined from 'material-ui/svg-icons/editor/format-underlined';
import InsertPhoto from 'material-ui/svg-icons/editor/insert-photo';
import RaisedButton from 'material-ui/RaisedButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

import ContentSave from 'material-ui/svg-icons/content/save';
import React, {Component}  from 'react';
import {StyleButton} from './StyleButton';
import {NoteEditorConstants} from './NoteEditorConstants';
import SelectField from 'material-ui/SelectField';
import {Colors} from '../../../../constants/Colors';

/** all headers removed in favor of dropdown, probably don't need them but leave for now */
const BLOCK_TYPES = [
  // {label: 'H1', style: 'header-one', icon : <FormatBold/>},
  // {label: 'H2', style: 'header-two', icon : <FormatBold/>},
  // {label: 'H3', style: 'header-three', icon : <FormatBold/>},
  // {label: 'H4', style: 'header-four', icon : <FormatBold/>},
  // {label: 'H5', style: 'header-five', icon : <FormatBold/>},
  // {label: 'H6', style: 'header-six', icon : <FormatBold/>},
  {label: 'Quote', style: 'blockquote', icon: <FormatQuote/>},
  {label: 'Bulleted List', style: 'unordered-list-item', icon: <FormatListBulleted/>},
  {label: 'Numbered List', style: 'ordered-list-item', icon: <FormatListNumbered/>},
  {label: 'Code Block', style: 'code-block', icon: <ActionCode/>},
];

var INLINE_STYLES = [
  {label: 'Bold', style: 'BOLD',  icon: <FormatBold/>},
  {label: 'Italic', style: 'ITALIC',  icon: <FormatItalic/>},
  {label: 'Underline', style: 'UNDERLINE', icon: <FormatUnderlined />},
  {label: 'Monospace', style: 'CODE', icon: <ActionCode/>},
];

// determines the header type from the blockType
// returs the headerType (ex: "header-one") if it's a header and "noheader" otherwise
function headerTypeFromBlockType(blockType) {
  return blockType.startsWith("header") ? blockType : "noheader"
}

export class NoteEditorMenuBar extends Component {

  constructor(props) {
    super();
  }

  handleHeaderChange(event, index, value) {
    if (value == "noheader") {
      value = "unstyled"
    }
    console.log("Setting blockType to  " + value)
    this.props.onSetBlock(value)
  }

  fileUploadClick(e) {
    this.fileInput.click()
  }

  // as soon as the user selects a file we send it off to the server
  onFileInputChange(e) {

    // get the file if one selected
    const files = e.target.files
    if (files.length == 0) {
      return
    }
    var file = files[0];

    // send to the server
    this.props.onFileUpload(file)

  }



  render() {

    const {editorState} = this.props;
    const selection = editorState.getSelection();
    const blockType = editorState
      .getCurrentContent()
      .getBlockForKey(selection.getStartKey())
      .getType();

    console.log("BlockType is " + blockType)

    const currentStyle = editorState.getCurrentInlineStyle();

    const styles = {
      width: NoteEditorConstants.MENU_ICON_WIDTH,
    }

    const headingSelectStyle = {
      width: 130,
      marginLeft: 20,
      marginRight: 10,
    }

    // leave these for now, not sure why they don't work as expected
    const headingSelectMenuStyle = {
      paddingTop: 0,      // doesn't work not sure why
      width: 130,          // breaks with anything less than 130 but is too wide at 130,
    }

    // doesn't work not sure why
    const selectUnderlineStyle = {
      color: "red"
    }


    const headerType = headerTypeFromBlockType(blockType)
    console.log("headerType is " + headerType)


    return (
      <div>
        <IconButton style={styles} tooltip="Save" onClick={this.props.handleSave} >
          <ContentSave color={Colors.icon} />
        </IconButton>

        {/*<IconButton style={styles} tooltip="Show State" onClick={this.props.showComponentState} >*/}
          {/*<ActionCode style={iconStyles} />*/}
        {/*</IconButton>*/}

        {/* Headings */}
        <SelectField
          style={headingSelectStyle}
          value={headerType}
          menuStyle={headingSelectMenuStyle}
          underlineStyle={selectUnderlineStyle}
          onChange={::this.handleHeaderChange}
        >
          <MenuItem value={'noheader'}  primaryText="No Header" />
          <MenuItem value={'header-one'} primaryText="Header 1" />
          <MenuItem value={'header-two'} primaryText="Header 2" />
          <MenuItem value={'header-three'} primaryText="Header 3" />
          <MenuItem value={'header-four'} primaryText="Header 4" />
          <MenuItem value={'header-five'} primaryText="Header 5" />
          <MenuItem value={'header-six'} primaryText="Header 6" />
        </SelectField>

        {BLOCK_TYPES.map((type) =>
          <StyleButton
            key={type.label}
            active={type.style === blockType}
            label={type.label}
            onToggle={this.props.onToggleBlock}
            style={type.style}
            icon={type.icon}
          />
        )}

        {INLINE_STYLES.map(type =>
          <StyleButton
            key={type.label}
            active={currentStyle.has(type.style)}
            label={type.label}
            onToggle={this.props.onToggleInline}
            style={type.style}
            icon={type.icon}
          />
        )}

        {/* Insert file widget */}
        <div style={{display: 'inline-block'}} >
          <form  action="handler.php" method="POST" style={{display: 'none'}} onSubmit={this.onUploadFormSubmit}>
            My file input form
            <input
              type="file"
              ref={(input) => { this.fileInput = input; }} onChange={::this.onFileInputChange}

            />
            <button type="submit" id="upload-button">Upload</button>
          </form>
          <IconButton onClick={::this.fileUploadClick}>
            <InsertPhoto/>
          </IconButton>
        </div>


      </div>
    );
  }
}