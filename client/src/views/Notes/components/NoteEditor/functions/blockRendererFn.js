// import QuoteCaptionBlock from './blocks/blockquotecaption';
// import CaptionBlock from './blocks/caption';
// import AtomicBlock from './blocks/atomic';
// import TodoBlock from './blocks/todo';
import ImageBlock from '../components/blocks/ImageBlock';
// import BreakBlock from './blocks/break';

import { Block } from '../NoteEditorConstants';

/**
 *  Copied this from medium draft, I don't think the setEditorState is needed nor does it seem to work
 *  in either my class or the one in Medium Draft. Will leave for now but likely it is just cruft
 *
 *  I believe this defines a function that returns a function. Seems a bit complicated for what we are trying to
 *  do, but I will leave for now and see if we can simplify it later. Might just be my limited experience with
 *  functional programming.
 *
 */

export default (setEditorState, getEditorState) => (contentBlock) => {
  console.log("Niltf blockRendererFn");
  // console.log(getEditorState);
  // console.log(getEditorState());
  const type = contentBlock.getType();
  console.log("type is " + type);
  switch (type) {
    // case Block.BLOCKQUOTE_CAPTION: return {
    //   component: QuoteCaptionBlock,
    // };
    // case Block.CAPTION: return {
    //   component: CaptionBlock,
    // };
    // case Block.ATOMIC: return {
    //   component: AtomicBlock,
    //   editable: false,
    // };
    // case Block.TODO: return {
    //   component: TodoBlock,
    //   props: {
    //     setEditorState,
    //     getEditorState,
    //   },
    // };
    case Block.IMAGE: {
      console.log("returning the block")
      return {
        component: ImageBlock,
        props: {
          setEditorState,
          getEditorState,
        },
      };
    }
    // case Block.BREAK: return {
    //   component: BreakBlock,
    //   editable: false,
    // };
    default: return null;
  }
};
