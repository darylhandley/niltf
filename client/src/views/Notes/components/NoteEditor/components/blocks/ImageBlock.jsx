import React, { PropTypes } from 'react';

import { EditorBlock } from 'draft-js';

import { getCurrentBlock } from '../../functions/editorFunctions';

const ImageBlock = (props) => {
  const { block, blockProps } = props;
  const { getEditorState } = blockProps;
  const data = block.getData();
  const src = data.get('src');
  const currentBlock = getCurrentBlock(getEditorState());
  const className = currentBlock.getKey() === block.getKey() ? 'md-image-is-selected' : '';
  console.log("Creating image block")
  if (src !== null) {
    console.log("src is not null")
    return (
      <div>
        <div className="md-block-image-inner-container">
          <img role="presentation" className={className} src={src} />
        </div>
        <figcaption>
          <EditorBlock {...props} />
        </figcaption>
      </div>
    );
  }
  return <EditorBlock {...props} />;
};

ImageBlock.propTypes = {
  block: PropTypes.object,
  blockProps: PropTypes.object,
};


export default ImageBlock;
