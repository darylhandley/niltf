import React from 'react';
import {Editor, EditorState, RichUtils, convertToRaw} from 'draft-js';
import IconButton from 'material-ui/IconButton';
import FormatBold from 'material-ui/svg-icons/editor/format-bold';
import {NoteEditorConstants} from './NoteEditorConstants';
import {Colors} from '../../../../constants/Colors';

export class StyleButton extends React.Component {
  constructor() {
    super();
    this.onToggle = (e) => {
      e.preventDefault();
      this.props.onToggle(this.props.style);
    };
  }

  render() {

    const styles = {
      paddingLeft: 0,
      paddingRight: 0,
      width: NoteEditorConstants.MENU_ICON_WIDTH,
      cursor: "pointer",
    }

    const iconWithStyle = React.cloneElement(
      this.props.icon,
      {
        color: this.props.active ? Colors.iconActive : Colors.icon
      },
    )

    // alert(this.props.icon)
    return (
      <IconButton style={styles} tooltip={this.props.label} onMouseDown={this.onToggle} >
        {iconWithStyle}
      </IconButton>
    );
  }
}
