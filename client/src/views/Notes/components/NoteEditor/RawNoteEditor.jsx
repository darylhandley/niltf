import React, {Component}  from 'react';
import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';

export class RawNoteEditor extends Component {
  render() {
    return (
      <div>
        <TextField
          value={this.props.noteBody}
          hintText="... just start typing the note you'd like to ..."
          fullWidth={true}
          hintStyle={{verticalAlign: "top"}}
          multiLine={true}
          underlineShow={false}
          onChange={this.props.handleNoteBodyChange}
        />
      </div>
    );
  }
}