
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import * as actionCreators from '../../../../actionCreators';
import  formatJson from 'format-json';
import {NiltfConstants} from '../../../../constants/niltfConstants'
import {RawNoteEditor} from './RawNoteEditor'
import {NoteEditorMenuBar} from './NoteEditorMenuBar'
import {NoteEditorTitle} from './NoteEditorTitle'
import {Editor, EditorState, RichUtils, convertToRaw} from 'draft-js';
import 'draft-js/dist/Draft.css'
import './RichEditor.css'
import blockRendererFn from './functions/blockRendererFn';

function getBlockStyle(block) {
  switch (block.getType()) {
    case 'blockquote': return 'RichEditor-blockquote';
    default: return null;
  }
}

// Custom overrides for "code" style.
const styleMap = {
  CODE: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
  },
};

export const NoteEditor = React.createClass({
  mixins: [PureRenderMixin],

  // getInitialState() {
  //   // alert("getting intial state");
  //   // return {editorState: EditorState.createEmpty()};
  // },

  saveNote: function(ev) {
    this.props.saveActiveNote();
  },

  showComponentState: function(ev) {
    var out = formatJson.plain(this.props.fullState);
    // alert (out);
    console.log (out);
  },

  handleNoteTitleChange: function(ev) {
    this.props.updateActiveNoteTitle(ev.target.value);
  },

  handleNoteBodyChange: function(ev) {
    this.props.updateActiveNoteBody(ev.target.value);
  },

  onChange : function(editorState) {
    this.props.updateActiveNoteEditorState(editorState);
  },

  onFileUpload : function(file) {
    this.props.uploadFileToActiveNote(file);
  },

  handleKeyCommand: function(command) {
    const editorState = this.props.activeNote.get("editorState");
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this.onChange(newState);
      return true;
    }
    return false;
  },

  toggleBlockType: function(blockType) {
    this.onChange(
      RichUtils.toggleBlockType(
        this.props.activeNote.get("editorState"),
        blockType
      )
    );
  },

  // absolutely sets the blockType of the editorState rather than toggling
  setBlockType: function(blockType) {
    const editorState = this.props.activeNote.get("editorState")

    // if the current style is already the same as the style we want do nothing
    const currentBlockType = RichUtils.getCurrentBlockType(editorState)
    if (currentBlockType === blockType) {
      return
    }


    this.onChange(
      RichUtils.toggleBlockType(
        editorState,
        blockType
      )
    );
  },

  getEditorState() {
    return this.props.activeNote.get("editorState")
  },

  toggleInlineStyle: function(inlineStyle) {
    this.onChange(
      RichUtils.toggleInlineStyle(
        this.props.activeNote.get("editorState"),
        inlineStyle
      )
    );
  },

  render: function() {
    const styles = {
      marginLeft : NiltfConstants.drawerWidth + 25,
    };

    // alert(this.props.activeNote.get('title'))
    const editorState = this.props.activeNote.get("editorState");
    // alert (editorState)

    // If the user changes block type before entering any text, we can
    // either style the placeholder or hide it. Let's just hide it now.
    let className = 'RichEditor-editor';
    var contentState = editorState.getCurrentContent();
    if (!contentState.hasText()) {
      if (contentState.getBlockMap().first().getType() !== 'unstyled') {
        className += ' RichEditor-hidePlaceholder';
      }
    }

    return (
      <div style={styles} >
        <NoteEditorMenuBar
          handleSave = {this.saveNote}
          showComponentState={this.showComponentState}
          editorState={editorState}
          onToggleBlock={this.toggleBlockType}
          onSetBlock={this.setBlockType}
          onToggleInline={this.toggleInlineStyle}
          onFileUpload={this.onFileUpload}
        />
        <NoteEditorTitle
          noteTitle={this.props.activeNote.get('title')}
          handleNoteTitleChange={this.handleNoteTitleChange}
        />
        {/*<RawNoteEditor*/}
          {/*noteBody={this.props.activeNote.get('body')}*/}
          {/*handleNoteBodyChange={this.handleNoteBodyChange}*/}
        {/*/>*/}
        <div className={className} onClick={this.focus}>
          <Editor
            blockStyleFn={getBlockStyle}
            customStyleMap={styleMap}
            editorState={editorState}
            handleKeyCommand={this.handleKeyCommand}
            onChange={this.onChange}
            placeholder="Tell a story..."
            ref="editor"
            spellCheck={true}
            blockRendererFn={blockRendererFn(this.onChange, this.getEditorState)}
          />
        </div>
      </div>
    );
  },
});



function mapStateToProps(state) {
  return {
    activeNote: state.get('activeNote'),
    // fullState: state
  }
}

export const NoteEditorContainer = connect(
  mapStateToProps,
  actionCreators
)(NoteEditor);