import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';
import React, {Component}  from 'react';

export class NoteEditorTitle extends Component {
  render() {
    return (
      <div>
        <TextField
          hintText="Enter a title and then ..."
          fullWidth={true}
          value={this.props.noteTitle}
          onChange={this.props.handleNoteTitleChange}
          inputStyle={{fontSize: 22}}
          hintStyle={{fontSize: 22}}
        />
      </div>
    );
  }
}