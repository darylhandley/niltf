import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import * as actionCreators from '../../actionCreators';
import {NoteTreeContainer} from './components/NoteTree/NoteTree'
import {NoteEditorContainer} from './components/NoteEditor/NoteEditor'
import NiltfAppBar from './NiltfAppBar'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import {NiltfConstants} from '../../constants/niltfConstants'
import { DragDropContext } from 'react-dnd';
import { DndItemTypes } from '../../constants/niltfConstants'
import { DropTarget } from 'react-dnd';
import { DragSource } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

const NotesView = React.createClass({
  mixins: [PureRenderMixin],
  handleLogout: function(ev) {
    // alert("handleLogout in Niltf");
    this.props.logout();
  },
  deleteConfirm() {
    this.props.deleteNoteId(
      this.props.ui.getIn(["deleteConfirmDialog", "noteId"])
    )
    this.props.deleteNoteDialogClose()
  },
  render: function() {

      const deleteConfirmActions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onTouchTap={this.props.deleteNoteDialogClose}
        />,
        <FlatButton
          label="Delete"
          primary={true}
          onTouchTap={this.deleteConfirm}
        />
      ]

      return (
        <div>
          <Dialog
            actions={deleteConfirmActions}
            open={this.props.ui.getIn(["deleteConfirmDialog", "isOpen"]) || false}
            modal={true}
            title="Confirm Delete Note"
          >
            Are you sure you want to delete this note and all of it's child notes? If you delete the note it can
            not be recovered.
          </Dialog>


          <NiltfAppBar handleLogout={this.handleLogout}/>
          <Drawer open={true}
                  width={NiltfConstants.drawerWidth}
          >
            {/*<MenuItem>Menu Item</MenuItem>*/}
            {/*<MenuItem>Menu Item 2</MenuItem>*/}
            <NoteTreeContainer/>
          </Drawer>
          <NoteEditorContainer/>
        </div>
      )
  },
  componentDidMount: function() {
    // alert("it mounted");
    // load the default set of notes
    this.props.getNotes('');
    this.props.getRootNote()
  },

  getStyles : function() {
    // alert("get styles");
    const styles = {
    //   appBar: {
    //     position: 'fixed',
    //     // Needed to overlap the examples
    //     // zIndex: this.state.muiTheme.zIndex.appBar + 1,
    //     top: 0,
    //   },
    //   root: {
    //     paddingTop: spacing.desktopKeylineIncrement,
    //     minHeight: 400,
    //   },
    //   content: {
    //     margin: spacing.desktopGutter,
    //   },
    //   contentWhenMedium: {
    //     margin: `${spacing.desktopGutter * 2}px ${spacing.desktopGutter * 3}px`,
    //   },
    //   footer: {
    //     backgroundColor: grey900,
    //     textAlign: 'center',
    //   },
    //   a: {
    //     color: darkWhite,
    //   },
    //   p: {
    //     margin: '0 auto',
    //     padding: 0,
    //     color: lightWhite,
    //     maxWidth: 356,
    //   },
    //   browserstack: {
    //     display: 'flex',
    //     alignItems: 'flex-start',
    //     justifyContent: 'center',
    //     margin: '25px 15px 0',
    //     padding: 0,
    //     color: lightWhite,
    //     lineHeight: '25px',
    //     fontSize: 12,
    //   },
    //   browserstackLogo: {
    //     margin: '0 3px',
    //   },
    //   iconButton: {
    //     color: darkWhite,
    //   },
    // };
    //
    // if (this.props.width === MEDIUM || this.props.width === LARGE) {
    //   styles.content = Object.assign(styles.content, styles.contentWhenMedium);
    }

    return styles;
  }

});


function mapStateToProps(state) {
  return {
    "ui" : state.get("ui"),
  }
}

const DndNotesView = DragDropContext(HTML5Backend)(NotesView);
export const NotesViewContainer = connect(
  mapStateToProps,
  actionCreators
)(DndNotesView);
