import React, { Component, PropTypes } from 'react';
import Square from './Square';
import { DndItemTypes } from '../../constants/niltfConstants'
import { DropTarget } from 'react-dnd';
import { DragSource } from 'react-dnd';

const squareDropSpec = {
  canDrop(props) {
    // return canMoveKnight(props.x, props.y);
    return true
  },

  drop(props, monitor, component) {
    // alert("component is " + component)
    // alert("result is  " + monitor.getDropResult())
    const item = monitor.getItem()
    alert(`dropped [${item.x},${item.y}] on [${props.x},${props.y}]`)
  }
};

function collectDrop(connect, monitor) {
  // console.log("boardSquare.collect")
  // alert("boardSquare.collect");
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  };
}

const squareDragSpec = {
  beginDrag(props, monitor, component) {
    // alert("begin drag  x=" + props.x + ", y=" + props.y)
    return {
      x : props.x,
      y : props.y
    };
  }
};

function collectDrag(connect, monitor) {
  // console.log("knight.collect")
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  }
}

@DropTarget(DndItemTypes.SQUARE, squareDropSpec, collectDrop)
@DragSource(DndItemTypes.SQUARE, squareDragSpec, collectDrag)
export default class BoardSquare extends Component {

  // static propTypes = {
  //   x: PropTypes.number.isRequired,
  //   y: PropTypes.number.isRequired,
  //   isOver: PropTypes.bool.isRequired,
  //   canDrop: PropTypes.bool.isRequired,
  //   isDragging: PropTypes.bool.isRequired
  // };


  renderOverlay(color) {
    return (
      <div style={{
        position: 'absolute',
        top: 0,
        left: 0,
        height: '100%',
        width: '100%',
        zIndex: 1,
        opacity: 0.5,
        backgroundColor: color,
      }} />
    );
  }

  render() {
    const { x, y, connectDropTarget, isOver, canDrop, connectDragSource, isDragging } = this.props;
    // const { connectDragSource, isDragging } = this.props
    const black = (x + y) % 2 === 1;

    return connectDragSource(connectDropTarget(
      <div className="darylAddedClassForDebug" style={{ position: 'relative', height: "100%", width: "100%"}}>
        <Square black={black}>
          {this.props.children}
        </Square>
        {isOver && canDrop && this.renderOverlay('red')}
      </div>
    ));
  }
}

