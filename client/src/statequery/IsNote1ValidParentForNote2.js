

export function isNote1AncestorOfNote2 (notes, note1Id, note2Id) {
  // alert(notes)
  // const note2 = notes.get(note2Id)
  const note2ParentId = notes.get(note2Id).get("parentId")
  console.log(`isNote1AncestorOfNote2 note2Id=${note2Id} note2ParentId=${note2ParentId},  note1Id= ${note1Id}`)
  if (note2ParentId === note1Id) {
    console.log("isNote1AncestorOfNote2 it's equal to note1Id returning true")
    return true
  } else if (note2ParentId === note2Id) {
    console.log("isNote1AncestorOfNote2 we're at the top level note returning false")
    return false
  } else {
    console.log("isNote1AncestorOfNote2 descending deeper")
    return isNote1AncestorOfNote2(notes, note1Id, note2ParentId)
  }
}