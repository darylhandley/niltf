import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route} from 'react-router';
import {browserHistory} from 'react-router'
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import io from 'socket.io-client';
import reducer from './reducer';
import {seState, selectSubreddit, getNotes } from './actionCreators';
import App from './App';
import {NotesViewContainer} from './views/Notes/NotesView';
import {SplashContainer} from './views/Splash/Splash';
import Board from './views/Board/Board';
import {Editor, EditorState, ContentState, RichUtils, convertToRaw} from 'draft-js';
// uncomment these lines to add back bootstrap
// require('expose?$!expose?jQuery!jquery');
// require("bootstrap-webpack");
// require('./css/starter-template.css');
// require('./css/themes/bootswatch-slate.css');
// require('./css/themes/bootswatch-slate-custom.css');
import './css/niltf.css';
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import Cookies from 'js-cookie'

import TextField from 'material-ui/TextField'

import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {lime50,lime500, lime700, lightBlack, black, limeA700, grey100, grey500, darkBlack, white, grey300} from 'material-ui/styles/colors'
import ColorManipulator from 'material-ui/utils/colorManipulator'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap, can eventually be removed when react supports it
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();


const loggerMiddleware = createLogger()

console.log("Creating store")
const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(
    thunkMiddleware, // lets us dispatch() functions
    loggerMiddleware // neat middleware that logs actions
  )
);

console.log("Creating mui theme")
// const muiTheme = getMuiTheme({
  // palette: {
  //   primary1Color: lime500,
  //   primary2Color: lime700,
  //   primary3Color: lightBlack,
  //   accent1Color: limeA700,
  //   accent2Color: grey100,
  //   accent3Color: grey500,
  //   textColor: white,
  //   alternateTextColor: white,
  //   canvasColor: black,
  //   borderColor: grey300,
  //   // disabledColor: ColorManipulator.fade(darkBlack, 0.3),
  //   pickerHeaderColor: lime500,
  // },
// });
// const muiTheme = getMuiTheme({
//   baseTheme: darkBaseTheme,
//   palette: {
//     // primary1Color: lime500,
//     // primary2Color: lime700,
//     // primary3Color: lightBlack,
//     // accent1Color: limeA700,
//     // accent2Color: grey100,
//     // accent3Color: grey500,
//     // textColor: white,
//     // alternateTextColor: white,
//     // canvasColor: black,
//     // borderColor: grey300,
//     // disabledColor: ColorManipulator.fade(darkBlack, 0.3),
//     // pickerHeaderColor: lime500,
//   },
// })

const muiTheme = getMuiTheme()

// todo, need some kind of initial state
console.log("Setting initial state")
// const defaultEditorState = EditorState.createWithContent(ContentState.createEmpty())
store.dispatch({
  type: 'SET_STATE',
  state: {
    activeNote : {
        id: '',
        title: "Default Note",
        body: "",
        editorState: EditorState.createEmpty(),
    },
    ui : {
      Login : {
        componentLevelError : ""
      }
    }
  }
});
// store.dispatch({
//   type: 'UPDATE_ACTIVE_NOTE_ID',
//   activeNoteId: '1111-1111-1111-1111'
// })
// console.log("getting notes")
// store.dispatch(getNotes(''))
// store.dispatch({
//   type: 'UPDATE_ACTIVE_NOTE_ID',
//   activeNoteId: '11541a50-abcd-4545-a55b-db9396f567fb'
// })

// this requireUauth and requireNoAuth is not really working at the moment
// it doesn't always redirect as expected. Leave for now and revisit later
function requireAuth () {
  // alert("requireAuth")
  if (!Cookies.get("authToken")) {
    // alert("push /");
    browserHistory.push('/')
  }
}

function requireNotAuth () {
  // alert("requireNotAuth")
  if (Cookies.get("authToken")) {
    // alert("push notes");
    browserHistory.push('/notes')
  }
}


console.log("Creating routes")
const routes =
<Route component={App}>
  <Route path="/notes" component={NotesViewContainer} onEnter={requireAuth} />
  <Route path="/" component={SplashContainer} onEnter={requireNotAuth} />
  <Route path="/board" component={Board} onEnter={requireAuth} />
  {/*<Route path="/editorTest" component={EditorTest} />*/}
  {/*<Route path="/editorTest2" component={EditorTest2} />*/}
</Route>

console.log("Rendering dom")
ReactDOM.render(
  <MuiThemeProvider muiTheme={muiTheme} >
    <Provider store={store}>
      <Router history={browserHistory}>{routes}</Router>
    </Provider>
  </MuiThemeProvider>
  ,document.getElementById('app')
)
