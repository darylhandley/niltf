
import React from 'react';
import withWidth, {MEDIUM, LARGE} from 'material-ui/utils/withWidth';

const app = React.createClass({
  render: function() {
    // alert("render app");
    return this.props.children;
  }
});

export default withWidth()(app);
