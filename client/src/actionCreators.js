import fetch from 'isomorphic-fetch'
import Cookies from 'js-cookie';
import {browserHistory} from 'react-router'
import {convertToRaw} from 'draft-js'
import {NiltfConstants} from './constants/niltfConstants'

const apiBasePath = NiltfConstants.ApiBasePath

export function setState(state) {
  return {
    type: 'SET_STATE',
    state
  };
}

export function updateActiveNoteId(activeNoteId) {
  // alert("updateActiveNoteId");
  return {
    type: 'UPDATE_ACTIVE_NOTE_ID',
    activeNoteId
  };
}

export function updateActiveNoteTitle(activeNoteTitle) {
  // alert("reducer updateActiveNoteId");
  return {
    type: 'UPDATE_ACTIVE_NOTE_TITLE',
    activeNoteTitle
  };
}

export function updateActiveNoteBody(activeNoteBody) {
  // alert("action creator updateActiveNoteBody " + activeNoteBody);
  return {
    type: 'UPDATE_ACTIVE_NOTE_BODY',
    activeNoteBody
  };
}

export function updateActiveNoteEditorState(editorState) {
  // alert("action creator updateActiveNoteBody " + activeNoteBody);
  return {
    type: 'UPDATE_ACTIVE_NOTE_EDITOR_STATE',
    editorState
  };
}

export const GET_NOTES_REQUEST = 'GET_NOTES_REQUEST'
function getNotesRequest(parentNoteId) {
  return {
    type: GET_NOTES_REQUEST,
    parentNoteId
  }
}

export const GET_NOTES_RECEIVE = 'GET_NOTES_RECEIVE'
function getNotesReceive(parentNoteId, json) {
  return {
    type: GET_NOTES_RECEIVE,
    parentNoteId,
    json: json,
    // posts: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}

export const GET_ROOT_NOTE_RECEIVE = 'GET_ROOT_NOTE_RECEIVE'
function getRootNoteReceive(json) {
  return {
    type: GET_ROOT_NOTE_RECEIVE,
    json: json,
    receivedAt: Date.now()
  }
}

// Meet our first thunk action creator!
// Though its insides are different, you would use it just like any other action creator:
// This is a thnnk action creator for doing the getNotes endpoint
// We'll use this same pattern for all of our client server interaction
// it uses thunk middleware
export function getNotes(parentNoteId) {

  // Thunk passes the dispatch method as an argument to the function,
  // this makes the method able to dispatch actions itself.
  return function (dispatch) {

    // dispatch the request
    // - state is updated here to inform the API call is starting.
    // - this is a noop for our case but we'll leave it gere for now
    dispatch(getNotesRequest(parentNoteId))

    const myHeaders = new Headers({
      "auth-token": Cookies.get("authToken")
    });

    // The function called by the thunk middleware can return a value,
    // that is passed on as the return value of the dispatch method.
    // In this case, we return a promise to wait for.
    // This is not required by thunk middleware, but it is convenient for us.
    return fetch(`${apiBasePath}/notes`, {headers: myHeaders})
      .then(response => response.json())
      .then(json =>
        // We can dispatch many times!
        // Here, we update the app state with the results of the API call.
        dispatch(getNotesReceive(parentNoteId, json))
      )

      // In a real world app, you also want to
      // catch any error in the network call.
  }
}

export function getRootNote() {

  return function (dispatch) {


    const myHeaders = new Headers({
      "auth-token": Cookies.get("authToken")
    });

    return fetch(`${apiBasePath}/notes/root`, {headers: myHeaders})
      .then(response => response.json())
      .then(json =>
        dispatch(getRootNoteReceive(json))
      )

    // In a real world app, you also want to
    // catch any error in the network call.
  }
}

export const CREATE_NOTE_RECEIVE = 'CREATE_NOTE_RECEIVE'
function createNoteReceive(json) {
  return {
    type: CREATE_NOTE_RECEIVE,
    json: json
    // posts: json.data.children.map(child => child.data),
    // receivedAt: Date.now()
  }
}

export function createNewNote(newNoteTitle) {

  return function (dispatch) {
    // dispatch(creawteNotesRequest(parentNoteId))
    const body = JSON.stringify({"title": newNoteTitle, "body" : ""})
    const authToken = Cookies.get("authToken");

    const myHeaders = new Headers({
      "Content-Type": "application/json",
      "auth-token": Cookies.get("authToken")
    });

    return fetch(`${apiBasePath}/notes`, {method:"post", body: body,
      headers: myHeaders})
      .then(response => response.json())
      .then(json =>
        // update the app state with the results of the API call.
        // need to add the id back to the note
        dispatch(createNoteReceive(json))
      ).catch(function(error) {
        // console.log(response.json());
        console.log(error);
      });

      // TODO catch errors
  }
}

export const DELETE_NOTE_RECEIVE = 'DELETE_NOTE_RECEIVE'
function deleteNoteReceive(noteId) {
  // alert(noteId)
  return {
    type: DELETE_NOTE_RECEIVE,
    noteId : noteId
    // posts: json.data.children.map(child => child.data),
    // receivedAt: Date.now()
  }
}

export function deleteNoteId(noteId) {

  return function (dispatch) {

    const myHeaders = new Headers({
      // "Content-Type": "application/json",
      "auth-token": Cookies.get("authToken")
    });

    return fetch(`${apiBasePath}/notes/` + noteId,
      {method:"delete", headers : myHeaders})
      .then(response => {
        // response.json()
        dispatch(deleteNoteReceive(noteId))
      })
      .catch(function(error) {
        console.log(error);
      });

      // TODO catch errors
  }
}

export const DELETE_NOTE_INIT = 'DELETE_NOTE_INIT'
export function deleteNoteInit(noteId) {
  return {
    type: DELETE_NOTE_INIT,
    noteId : noteId
  }
}

export const DELETE_NOTE_DIALOG_CLOSE = 'DELETE_NOTE_DIALOG_CLOSE'
export function deleteNoteDialogClose() {
  return {
    type: DELETE_NOTE_DIALOG_CLOSE,
  }
}

export function addChildNote(parentNoteId) {


  return function (dispatch, getState) {
    let state = getState()
    // alert("actionCreator.addChildNote " + parentNoteId)
    // alert(state)

    const parentNote = state.getIn(["notes", parentNoteId])
    // alert(parentNote)

    // dispatch(creawteNotesRequest(parentNoteId))
    const body = JSON.stringify({"title": "Untitled", "body" : "", "parentId" : parentNoteId})
    const authToken = Cookies.get("authToken");

    const myHeaders = new Headers({
      "Content-Type": "application/json",
      "auth-token": Cookies.get("authToken")
    });

    return fetch(`${apiBasePath}/notes`, {method:"post", body: body,
      headers: myHeaders})
      .then(response => response.json())
      .then(json =>
        // update the app state with the results of the API call.
        dispatch(createNoteReceive(json))
      ).catch(function(error) {
        // console.log(response.json());
        console.log(error);
      });

    // TODO catch errors
  }



}

export function moveNoteToNewParent(noteId, parentNoteId) {
  return function (dispatch, getState) {
    // alert(`dropping ${noteId} on ${parentNoteId}`)
    let state = getState()
    if (noteId === parentNoteId) {
      return
    }
    const note = state.getIn(["notes", noteId])
    const parentNote = state.getIn(["notes", parentNoteId])

    // if current note parent is the same as the new parent then it is a no op and we just return
    const oldParentId = note.get("parentId")
    if (oldParentId === parentNoteId) {
      return
    }

    // update the parentNote by adding the child to it's children
    const newChildrenIds = parentNote.get("childrenIds").push(noteId)

    const body = JSON.stringify({"childrenIds": newChildrenIds})

    const myHeaders = new Headers({
      "Content-Type": "application/json",
      "auth-token": Cookies.get("authToken")
    });

    return fetch(`${apiBasePath}/notes/${parentNoteId}`, {method:"PATCH", body: body,
      headers: myHeaders})
      .then(response => response.json())
      .then(json =>
        // update the app state with the results of the API call.
        dispatch(moveNoteToParentSuccess(noteId, parentNoteId))
      ).catch(function(error) {
        console.log(error);
      });

    // TODO catch errors
  }

}

export function moveNoteToSibling(noteId, parentId, siblingId, dropType) {

  return function (dispatch, getState) {

    const state = getState()
    const parentNote = state.getIn(["notes", parentId])

    // update the parentNote by adding the child to it's children
    // in the location before or after the sibling
    const childrenIds = parentNote.get("childrenIds")
    let newChildrenIds = childrenIds.filterNot(id => id === noteId)
    const siblingIndex = newChildrenIds.indexOf(siblingId)
    // alert(`siblingIndex is ${siblingIndex}`)
    const insertIndex = (dropType === "before") ? siblingIndex : siblingIndex + 1;
    newChildrenIds = newChildrenIds.insert(insertIndex, noteId)
    // alert(`${childrenIds} modified to ${newChildrenIds}`)

    const body = JSON.stringify({"childrenIds": newChildrenIds})

    const myHeaders = new Headers({
      "Content-Type": "application/json",
      "auth-token": Cookies.get("authToken")
    });

    return fetch(`${apiBasePath}/notes/${parentId}`, {method:"PATCH", body: body,
      headers: myHeaders})
      .then(response => response.json())
      .then(json =>
        // update the app state with the results of the API call.
        dispatch(moveNoteSuccess(noteId, parentId, newChildrenIds))
      ).catch(function(error) {
        console.log(error);
      });

    // TODO catch errors
  }

}

export const MOVE_NOTE_TO_PARENT_SUCCESS = 'MOVE_NOTE_TO_PARENT_SUCCESS'
export function moveNoteToParentSuccess(noteId, parentNoteId) {
  return {
    type: MOVE_NOTE_TO_PARENT_SUCCESS,
    noteId : noteId,
    parentNoteId: parentNoteId,
  }
}

export const MOVE_NOTE_SUCCESS = 'MOVE_NOTE_SUCCESS'
export function moveNoteSuccess(noteId, parentNoteId, newChildrenIds) {
  return {
    type: MOVE_NOTE_SUCCESS,
    noteId : noteId,
    parentNoteId: parentNoteId,
    newChildrenIds: newChildrenIds,
  }
}



export function loadChildNotes(parentNoteId) {
  // alert("ActionCreator loading child notes for noteId " + parentNoteId)

  return function (dispatch) {

    // dispatch(getNotesRequest(parentNoteId))

    const myHeaders = new Headers({
      "auth-token": Cookies.get("authToken")
    });

    // return fetch(`${apiBasePath}/notes/${parentId}`, {method:"PATCH", body: body,
    return fetch(`${apiBasePath}/notes/${parentNoteId}/children`, {headers: myHeaders})
      .then(response => response.json())
      .then(json =>
        // update app states with results
        dispatch(getNotesReceive(parentNoteId, json))
      )

  }
}


// export function loadChildNotes(noteId) {
//
//   return function (dispatch) {
//     alert("ActionCreator loading child notes")
//     // dispatch(creawteNotesRequest(parentNoteId))
//     // const body = JSON.stringify({"title": newNoteTitle, "body" : ""})
//     //
//     // const myHeaders = new Headers({
//     //   "Content-Type": "application/json",
//     // });
//     // alert(noteId);
//
//     // const myHeaders = new Headers({
//     //   // "Content-Type": "application/json",
//     //   "auth-token": Cookies.get("authToken")
//     // });
//     //
//     // return fetch(`${apiBasePath}/notes/` + noteId,
//     //   {method:"delete", headers : myHeaders})
//     //   .then(response => {
//     //     // response.json()
//     //     dispatch(deleteNoteReceive(noteId))
//     //   })
//     //   .catch(function(error) {
//     //     console.log(error);
//     //   });
//
//     // TODO catch errors
//   }
// }

export const SAVE_NOTE_RECEIVE = 'SAVE_NOTE_RECEIVE'
function saveNoteReceive(noteId) {
  // alert(noteId)
  return {
    type: SAVE_NOTE_RECEIVE
    // posts: json.data.children.map(child => child.data),
    // receivedAt: Date.now()
  }
}

export function saveActiveNote() {

  return function (dispatch, getState) {
    // dispatch(creawteNotesRequest(parentNoteId))
    let state = getState()
    let noteId = state.get("activeNote").get("id");
    let title = state.get("activeNote").get("title");
    // let noteBody = state.get("activeNote").get("body");
    let editorState = state.get("activeNote").get("editorState");
    let noteBody = JSON.stringify(convertToRaw(editorState.getCurrentContent()))
    const body = JSON.stringify({"title": title, "body" : noteBody})
    const myHeaders = new Headers({
      "Content-Type": "application/json",
      "auth-token": Cookies.get("authToken")
    });
    // alert(noteId + " " + title + " " + noteBody);
    // alert("done here")
    //
    return fetch(`${apiBasePath}/notes/` + noteId,
      {
        method:"PATCH",
        body: body,
        headers: myHeaders
      })
      .then(response => response.json())
      .then(json =>
        // update the app state with the results of the API call.
        dispatch(saveNoteReceive(noteId))
      )
  }
}

export function login(email, password) {

  return function (dispatch) {

    // alert("in the login username=" + email + ", password" + password)
    const body = JSON.stringify({"email": email, "password" : password})
    const myHeaders = new Headers({
      "Content-Type": "application/json",
    });

    return fetch(`${apiBasePath}/auth/token`,
      {
        method:"POST",
        body: body,
        headers: myHeaders
      })
      .then(function(response) {
        if (response.status >= 200 && response.status < 300) {
          response.json().then(function(json) {
            console.log("logged in succsefully tokenId=" + json.authToken)
            dispatch(loginSuccess(json))
            browserHistory.push('/notes')
          })
        } else if (response.status === 401) {
          response.json().then(function(json) {
            console.log("login failed")
            dispatch(loginFailed(json))
          })
        } else {
          response.json().then(function(json) {
            console.log("Unexpected error : " + json.status + "," + json.message)
            dispatch(loginError(json.message))
          })
        }
      })
      .catch(function(error) {
        console.log("Error occured: " + error)
        dispatch(loginError(error))
      })

      // TODO catch errors
  }
}

export const LOGIN_FAILED = 'LOGIN_FAILED'
export function loginFailed(json) {
  console.log("actionCreator.loginFailed")
  return {
    type: LOGIN_FAILED,
    json
  }
}

export const LOGIN_ERROR = 'LOGIN_ERROR'
export function loginError(message) {
  console.log("actionCreator.loginError")
  return {
    type: LOGIN_ERROR,
    message
  }
}

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export function loginSuccess(json) {
  console.log("actionCreator.loginSuccess")
  return {
    type: LOGIN_SUCCESS,
    json
  }
}

export const CLEAR_LOGIN = 'CLEAR_LOGIN'
export function clearLogin() {
  console.log("actionCreator.clearLogin")
  return {
    type: CLEAR_LOGIN
  }
}



export function signup(email, password) {

  return function (dispatch) {
    // alert("in the signup username=" + email + ", password" + password)
    const body = JSON.stringify({"email": email, "password" : password})
    const myHeaders = new Headers({
      "Content-Type": "application/json",
    });

    // discussion on some fetch handling, need to figure out a nice
    // clean way in the future as we start to get more use cases
    // https://github.com/github/fetch/issues/203
    return fetch(`${apiBasePath}/users`,
      {
        method:"POST",
        body: body,
        headers: myHeaders
      })
      .then(function(response) {
        if (response.status >= 200 && response.status < 300) {
          response.json().then(function(json) {
            alert("success")
            // console.log(object.type, object.message)
          })
        } else if (response.status === 409 || response.status === 400) {
          response.json().then(function(json) {
            alert("invalid : " + json.message)
          })
        } else {
          response.json().then(function(json) {
            alert("Unexpected error :" + json.message)
          })
        }
      })
      .catch(function(error) {
        alert("Error occured: " + error)
      })
  }
}


export function logout() {
  return {
    type: 'LOGOUT'
  };
}

export const UPLOAD_FILE_SUCCESS = 'UPLOAD_FILE_SUCCESS'
function uploadFileSuccess(resultJson) {
  // alert(noteId)
  return {
    type: UPLOAD_FILE_SUCCESS,
    result: resultJson
  }
}


export function uploadFileToActiveNote(file) {

  return function (dispatch) {

    const myHeaders = new Headers({
      "enctype": "multipart/form-data",
      "auth-token": Cookies.get("authToken")
    });

    const formData = new FormData()
    formData.append('file', file)

    // alert("executing fetch")
    // upload the file and then dispatch the result
    return fetch(`${apiBasePath}/files/upload`,
      {
        method:"POST",
        body: formData,
        headers: myHeaders
      })
      .then(response => response.json())
      .then(json => {
          // right here, insert the image into the note
          // alert("success")
          dispatch(uploadFileSuccess(json))
        }
      )

  }
}
