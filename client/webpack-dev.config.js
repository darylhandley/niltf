var webpack = require('webpack');

module.exports = {
  entry: [
    'webpack-dev-server/client?http://localhost:7272',
    'webpack/hot/only-dev-server',
    './src/index.jsx'
  ],
  module: {
    loaders: [
      // {test: /\.jsx?$/, exclude: /node_modules/, loader: 'react-hot-loader/babel'},
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel', // 'babel-loader' is also a legal name to reference
        query: {
          // presets: ['es2015'] // commented this out to fix drag and drop, not sure waht it does really. DMH
        }
      },
      // the url-loader uses DataUrls.
      // the file-loader emits files.
      { test: /\.(woff|woff2)$/,  loader: "url-loader?limit=10000&mimetype=application/font-woff" },
      { test: /\.ttf$/,    loader: "file-loader" },
      { test: /\.eot$/,    loader: "file-loader" },
      { test: /\.svg$/,    loader: "file-loader" },
      { test: /\.css$/, loader: 'style!css!postcss'}

      // { test: require.resolve("jquery"), loader: "imports?jQuery=jquery" }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: './dist',
    hot: true,
    historyApiFallback: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // new webpack.ProvidePlugin({    // <added>
    //   jQuery: 'jquery',
    //   $: 'jquery',
    //   jquery: 'jquery'   // </added>
    // })
  ]
};
